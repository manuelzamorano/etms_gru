
var formInstitucion
, dataCadena
, dataTipoInstitucion
, dataBrick
, dataHorario
, dataInstitucion
, dataCategoriaAdopcion
, dataCategoriaCompania
, dataCategoriaPotencial;

$.when(promiseCultureLoad).done(function () {
    
    cargarBrick();
    cargarCadena();
    cargarHorario();
    cargarCategoriaAdopcion();
    cargarCategoriaCompania();
    cargarCategoriaPotencial();
    cargarTipoInstitucion();
    obtenerDataInstitucion();
    mostrarFormulario();
    mostrarBotonActualizar();
    detenerLoadingPanel();

});

function mostrarFormulario(){
    formInstitucion = $("#formInstitucion").dxForm({
        colCount: 2,
        colSpan: 2,
        formData: dataInstitucion,
        items: [
            {
                itemType: "group",
                caption: "Datos Institucion",
                colCount: 2,
                colSpan: 2,
                items: [
                    {
                        dataField: "codigo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Código'
                        },
                        editorOptions: {
                            placeholder: 'Sin puntos, ni guión',
                            disabled: true,
                            maxLength: 15
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar Rut"
                        }]
                    },
                    {
                        itemType: "empty",
                    },
                    {
                        dataField: "nombre",
                        colSpan: 2,
                        editorType: "dxTextBox",
                        label: {
                            text: 'Nombre'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nombre"
                        }]
                    },
                    {
                        dataField: "local",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Local'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 10 caracteres',
                            maxLength: 10
                        },
                    },
                    {
                        dataField: "tipo_institucion_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Tipo'
                        },
                        editorOptions: {
                            dataSource: dataTipoInstitucion
                            , displayExpr: 'tipo_institucion_descriptiva'
                            , valueExpr: 'tipo_institucion_id'
                            , showClearButton: true
                            , searchEnabled: true
                        }
                    },
                    {
                        dataField: "cadena_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Cadena'
                        },
                        editorOptions: {
                            dataSource: dataCadena
                            , displayExpr: 'cadena_descriptiva'
                            , valueExpr: 'cadena_id'
                            , showClearButton: true
                            , searchEnabled: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar cadena"
                        }]
                    },
                    {
                        dataField: "telefono",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Teléfono / celular'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 25 caracteres',
                            maxLength: 25
                        }
                    },
                    {
                        dataField: "calle",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Dirección'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 400
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar dirección"
                        }]
                    },
                    {
                        dataField: "comuna",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Comuna'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar comuna"
                        }]
                    },
                    {
                        dataField: "brick_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Brick'
                        },
                        editorOptions: {
                            dataSource: dataBrick
                            , displayExpr: 'brick_descriptiva'
                            , valueExpr: 'brick_id'
                            , showClearButton: true
                            , searchEnabled: true
                        }
                    },
                ],
            },
            {
                itemType: "group",
                caption: "Horario de atención",
                colSpan: 1,
                items: [
                    {
                        dataField: "lunes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Lunes'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "martes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Martes'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "miercoles_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Miércoles'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "jueves_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Jueves'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "viernes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Viernes'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                ]
            },
            {
                itemType: "group",
                caption: "Categoria",
                colSpan: 1,
                items: [
                    {
                        dataField: "categoria_compania_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Compañia'
                        },
                        editorOptions: {
                            dataSource: dataCategoriaCompania
                            , displayExpr: 'categoria_compania_descriptiva'
                            , valueExpr: 'categoria_compania_id'
                            , showClearButton: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar una categoria"
                        }]
                    },
                    {
                        dataField: "categoria_potencial_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Potencial'
                        },
                        editorOptions: {
                            dataSource: dataCategoriaPotencial
                            , displayExpr: 'categoria_potencial_descriptiva'
                            , valueExpr: 'categoria_potencial_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "categoria_adopcion_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Adopción'
                        },
                        editorOptions: {
                            dataSource: dataCategoriaAdopcion
                            , displayExpr: 'categoria_adopcion_descriptiva'
                            , valueExpr: 'categoria_adopcion_id'
                            , showClearButton: true
                        }
                    },
                ]
            }
        ]
    }).dxForm('instance');
}

function mostrarBotonActualizar(){
    $("#buttonActualizar").dxButton({
        text: "Actualizar",
        type: "success",
        width: 200,
        validationGroup: 'guardarDatos',
        onClick: function(e) {
            if(formInstitucion.validate().isValid) {
                swal.queue([{
                    title: 'Actualizar',
                    confirmButtonText: 'Guardar',
                    text: 'Desea guardar los cambios?',
                    type: 'question',
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            actualizarInstitucion();
                        })
                    }
                }])
            } else {
                DevExpress.ui.notify('Complete campos obligatorios.', 'error');
            }
        }
    });
}

function actualizarInstitucion(){

    var formData = formInstitucion.option('formData');

    $.ajax({
        dataType: "json",
        'async': false,
        data: formData,
        url: "../rest/crudInstitucion/actualizarDataInstitucion",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        swal({
            title: 'Actualización',
            text: 'Registro completo',
            type: 'success',
            confirmButtonText: 'OK'
        }).then(
            function () { 
                location.href = "../crudInstitucion";
            }
        );
    });
    
}

function cargarCadena(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataCadenaSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataCadena = data[0];
    });
}

function cargarBrick(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataBrickSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataBrick = data[0];
    });
}

function cargarHorario(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataHorarioSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataHorario = data[0];
    });
}

function cargarCategoriaAdopcion(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataCategoriaAdopcionSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataCategoriaAdopcion = data[0];
    });
}

function cargarCategoriaCompania(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataCategoriaCompaniaSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataCategoriaCompania = data[0];
    });
}

function cargarCategoriaPotencial(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataCategoriaPotencialSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataCategoriaPotencial = data[0];
    });
}

function cargarTipoInstitucion(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataTipoInstitucionSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataTipoInstitucion = data[0];
    });
}

function obtenerDataInstitucion(){
    var institucionId = getParameterByName('id');
    if(institucionId){
        $.ajax({
            dataType: "json",
            'async': false,
            data:{
                institucion_id : institucionId
            },
            url: "../rest/crudInstitucion/obtenerDataInstitucionPorID",
            error: function(xhr, status) {
                if (typeof this.statusCode[xhr.status] != 'undefined') {
                    return false;
                }
                console.log('ajax.error');
            },
            statusCode: {
                404: function(response) {
                    console.log('status_code: 404');
                },
                500: function(response) {
                    console.log('status_code: 500');
                }
            }
        }).done(function(data) {
            dataInstitucion = data[0][0];
        });
    }
    

}