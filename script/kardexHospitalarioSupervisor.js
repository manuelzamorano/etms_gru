
var dataDataGrid, dataGrid;
var columns1 = [
    { dataField: "cliente_id", dataType: "number", visible: false },
    { dataField: "estado", dataType: "number", visible: false },
    { caption: "Representante", dataField: "representante", width: 160 },
    { caption: "Frecuencia", dataField: "realizadas", width: 90, alignment: 'center' },
    { caption: "Agendadas", dataField: "agendadas", width: 100, alignment: 'center' },
    { caption: "Código", dataField: "codigo", width: 85 },
    { caption: "Cliente", dataField: "nombre", width: 330 },
    { caption: "Contacto", dataField: "contacto", width: 330 },
    { caption: "Teléfono", dataField: "telefono", width: 100 },
    { caption: "Dirección", dataField: "direccion", width: 320 },
    { caption: "Cat CIA", dataField: "categoria_compania_descriptiva", dataType: "string", width: 100 },
    { caption: "Última visita", dataField: "fecha_ultima_visita", dataType: "string", width: 100 },
];
var columns2 = [
    { dataField: "cliente_id", dataType: "number", visible: false },
    { dataField: "estado", dataType: "number", visible: false },
    { caption: "Representante", dataField: "representante", width: 160 },
    { caption: "Frecuencia", dataField: "realizadas", width: 90, alignment: 'center' },
    { caption: "Agendadas", dataField: "agendadas", width: 100, alignment: 'center' },
    { caption: "Código", dataField: "codigo", width: 85 },
    { caption: "Cliente", dataField: "nombre", width: 330 },
    { caption: "Contacto", dataField: "contacto", width: 330 },
    { caption: "Brick", dataField: "brick_descriptiva", width: 150 },
    { caption: "Teléfono", dataField: "telefono", width: 100 },
    { caption: "Formato", dataField: "formato", width: 100 },
    { caption: "Dirección", dataField: "direccion", width: 320 },
    { caption: "Calle Referencia", dataField: "calle_referencia", width: 150 },
    { caption: "Cat CIA", dataField: "categoria_compania_descriptiva", dataType: "string", width: 70 },
    { caption: "Cat Potencial", dataField: "categoria_potencial_descriptiva", width: 120 },
    { caption: "Última visita", dataField: "fecha_ultima_visita", dataType: "string", width: 100 },
];
var columns3 = [
    { dataField: "cliente_id", dataType: "number", visible: false },
    { dataField: "estado", dataType: "number", visible: false },
    { caption: "Representante", dataField: "representante", width: 160 },
    { caption: "Frecuencia", dataField: "realizadas", width: 90, alignment: 'center' },
    { caption: "Agendadas", dataField: "agendadas", width: 100, alignment: 'center' },
    { caption: "Código", dataField: "codigo", width: 85 },
    { caption: "Cliente", dataField: "nombre", width: 330 },
    { caption: "Contacto", dataField: "contacto", width: 330 },
    { caption: "Brick", dataField: "brick_descriptiva", width: 150 },
    { caption: "Teléfono", dataField: "telefono", width: 100 },
    { caption: "Dirección", dataField: "direccion", width: 320 },
    { caption: "Cat Potencial", dataField: "categoria_potencial_descriptiva", width: 100 },
    { caption: "Última visita", dataField: "fecha_ultima_visita", dataType: "string", width: 100 },
];
var columns4 = [
    { dataField: "cliente_id", dataType: "number", visible: false },
    { dataField: "estado", dataType: "number", visible: false },
    { caption: "Representante", dataField: "representante", width: 160 },
    { caption: "Frecuencia", dataField: "realizadas", width: 90, alignment: 'center' },
    { caption: "Agendadas", dataField: "agendadas", width: 100, alignment: 'center' },
    { caption: "Código", dataField: "codigo", width: 85 },
    { caption: "Cliente", dataField: "nombre", width: 330 },
    { caption: "Contacto", dataField: "contacto", width: 330 },
    { caption: "Teléfono", dataField: "telefono", width: 100 },
    { caption: "Formato", dataField: "formato", width: 100 },
    { caption: "Cat CIA", dataField: "categoria_compania_descriptiva", dataType: "string", width: 110 },
    { caption: "Última visita", dataField: "fecha_ultima_visita", dataType: "string", width: 150 },
    { caption: "Visita N -1", dataField: "fecha_visita_n1", width: 150 },
    { caption: "Visita N -2", dataField: "fecha_visita_n2", width: 150 },
    { caption: "Visita N -3", dataField: "fecha_visita_n3", width: 150 },
    { caption: "Visita N -4", dataField: "fecha_visita_n4", width: 150 }
];
$.when(promiseCultureLoad).done(function () {
    loadDataDataGrid();
    $( ".tabDatagrid1" ).click( function() { mostrarDataGridSeleccionada('tab1') });
    $( ".tabDatagrid2" ).click( function() { mostrarDataGridSeleccionada('tab2') });
    $( ".tabDatagrid3" ).click( function() { mostrarDataGridSeleccionada('tab3') });
    $( ".tabDatagrid4" ).click( function() { mostrarDataGridSeleccionada('tab4') });
    dataGrid = $("#dataGridContainer").dxDataGrid({
        dataSource: dataDataGrid,
        rowAlternationEnabled: true,
        showBorders: true,
        columnAutoWidth: true,
        columnFixing: { enabled: true },
        columns: columns1,
        "export": {
            enabled: true,
            fileName: "kardex_hospitalario_"+getFechaExportarExcel(),
            excelFilterEnabled: true
        },
        summary: {
            totalItems: [
                {
                    name: "visitasRealizadas",
                    showInColumn: "representante",
                    displayFormat: "Realizadas {0}",
                    valueFormat: "decimal",
                    summaryType: "custom"
                },
                {
                    name: "visitasFrecuencia",
                    showInColumn: "realizadas",
                    displayFormat: "de {0}",
                    valueFormat: "decimal",
                    summaryType: "custom"
                }
            ],
            calculateCustomSummary: function (options) {
                if (options.name === "visitasRealizadas") {
                    if (options.summaryProcess === "start") {
                        options.totalValue = 0;
                    }
                    if (options.summaryProcess === "calculate") {
                        options.totalValue = options.totalValue + options.value.real_visitas;
                    }
                    if (options.summaryProcess === "finalize") {
                        options.totalValue = options.totalValue;
                    }
                }
                if (options.name === "visitasFrecuencia") {
                    if (options.summaryProcess === "start") {
                        options.totalValue = 0;
                    }
                    if (options.summaryProcess === "calculate") {
                        options.totalValue = options.totalValue + options.value.frecuencia;
                    }
                    if (options.summaryProcess === "finalize") {
                        options.totalValue = options.totalValue;
                    }
                }
            }
        },
        paging: { enabled: true, pageSize: 10 },
        filterRow: { visible: true },
        paging: { pageSize: 10 },
        width: '100%',
    }).dxDataGrid('instance');
    detenerLoadingPanel();
});

function loadDataDataGrid() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/kardex/getDataKardexHospitalarioSupervisor",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataDataGrid = data[0];
    });
}

function mostrarDataGridSeleccionada(tab){
    var columns;
    dataGrid.option('columns', []);
    $('.tabDatagrid1').removeClass('active');
    $('.tabDatagrid2').removeClass('active');
    $('.tabDatagrid3').removeClass('active');
    $('.tabDatagrid4').removeClass('active');
    
    switch (tab) {
        case "tab1":
            dataGrid.option('columns', columns1);
            $(".tabDatagrid1").addClass('active');
            break;
        case "tab2":
            dataGrid.option('columns', columns2);
            $(".tabDatagrid2").addClass('active');
            break;
        case "tab3":
            dataGrid.option('columns', columns3);
            $(".tabDatagrid3").addClass('active');
            break;
        case "tab4":
            dataGrid.option('columns', columns4);
            $(".tabDatagrid4").addClass('active');
            break;
        }

}
