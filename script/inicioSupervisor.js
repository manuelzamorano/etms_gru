
var resultData, metaCiclo, metaAFecha
, realizadas, canceladas, cobertura_a_fecha
, dataGaugeCategoria, cobertura_total_institucion = []
, cobertura_total_profesional = [], cobertura_total_hospitalario = []
, categoria_total_institucion= [], categoria_total_profesional = []
, categoria_total_hospitalario = [], cobertura_total_cliente = []
, dataPieChartAgenda = [], dataChartAgenda = []
, dataPieChartAgendaInstitucion = []
, dataPieChartAgendaProfesional = []
, dataPieChartAgendaHospitalario= [];

$.when(promiseCultureLoad).done(function () {
    resultData = $.Deferred();
    loadDataConfig();
    loadDataInicio();
    $.when(resultData).done(function () {

        selectBoxCiclo = $("#selectBoxCiclo").dxSelectBox({
            dataSource: dataCiclo
            , displayExpr: 'ciclo_descriptiva'
            , valueExpr: 'ciclo_id'
            , value: dataCiclo[0].ciclo_id
            , onValueChanged: function(data) {
                loadDataSelectBox();
            }
        }).dxSelectBox("instance");

        selectBoxRepresenante = $("#selectBoxRepresenante").dxSelectBox({
            dataSource: dataRepresentante
            , displayExpr: 'representante_descriptiva'
            , valueExpr: 'representante_id'
            , showClearButton: true
            , onValueChanged: function(data) {
                loadDataSelectBox();
            }
        }).dxSelectBox("instance");
        
        detenerLoadingPanel();
    });
});

function controles(){

    metaCiclo >- 1 ? 
        $("#h2MetaCiclo").text(metaCiclo) 
        : $("#h2MetaCiclo").text('0');
    metaAFecha >- 1 ?
        $("#h2MetaFecha").text(metaAFecha) 
        : $("#h2MetaFecha").text('0');
    realizadas >- 1 ?
        $("#h2VisitaRealizadas").text(realizadas + ' ('+ Math.round((realizadas * 100) / metaAFecha) +'%)') 
        : $("#h2VisitaRealizadas").text('0');
    canceladas >- 1 ?
        $("#h2VisitasCanceladas").text(canceladas) 
        : $("#h2VisitasCanceladas").text('0');    

    //RELOJES
    if (!isEmpty(dataGaugeCliente[0])) {
        $("#circularGaugeInstitucion").dxCircularGauge({
            value: dataGaugeCliente[0].porcentaje_visita,
            valueIndicator: {
                type: 'rectangleNeedle',
                color: 'white',
                offset: 2
            },
            scale: {
                startValue: 0,
                endValue: 120,
                tickInterval: 30,
                tick: {
                    visible: false,
                    color: 'black'
                },
                minorTick: {
                    visible: true,
                    color: '#rgba(0,0,0,0.2)'
                },
                label: {
                    font: {
                        color: 'white',
                        size: 11
                    },
                    customizeText: function (arg) {
                        return arg.valueText + " %";
                    }
                }
            },
            rangeContainer: {
                ranges: [
                    { startValue: 0, endValue: 60, color: '#FF5652' },
                    { startValue: 60, endValue: 90, color: '#FFC107' },
                    { startValue: 90, endValue: 120, color: '#319b6d' }
                ]
            },
            size: {
                height: 100
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: Math.round(arg.valueText) + ' %'
                    };
                },
                font: {
                    color: '#39BBB0',
                    size: 40
                }
            },
        });
    }   

    if (!isEmpty(dataGaugeCliente[1])) {
        $("#circularGaugeProfesional").dxCircularGauge({
            value: dataGaugeCliente[1].porcentaje_visita,
            valueIndicator: {
                type: 'rectangleNeedle',
                color: 'white',
                offset: 2
            },
            scale: {
                startValue: 0,
                endValue: 120,
                tickInterval: 30,
                tick: {
                    visible: false,
                    color: 'black'
                },
                minorTick: {
                    visible: true,
                    color: '#rgba(0,0,0,0.2)'
                },
                label: {
                    font: {
                        color: 'white',
                        size: 11
                    },
                    customizeText: function (arg) {
                        return arg.valueText + " %";
                    }
                }
            },
            rangeContainer: {
                ranges: [
                    { startValue: 0, endValue: 60, color: '#FF5652' },
                    { startValue: 60, endValue: 90, color: '#FFC107' },
                    { startValue: 90, endValue: 120, color: '#319b6d' }
                ]
            },
            size: {
                height: 100
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: Math.round(arg.valueText) + ' %'
                    };
                },
                font: {
                    color: '#39BBB0',
                    size: 40
                }
            },
        });    
    }
    
    if(!isEmpty(dataGaugeCliente[2])){
        $("#circularGaugeHospitalario").dxCircularGauge({
            value: dataGaugeCliente[2].porcentaje_visita,
            valueIndicator: {
                type: 'rectangleNeedle',
                color: 'white',
                offset: 2
            },
            scale: {
                startValue: 0,
                endValue: 120,
                tickInterval: 30,
                tick: {
                    visible: false,
                    color: 'black'
                },
                minorTick: {
                    visible: true,
                    color: '#rgba(0,0,0,0.2)'
                },
                label: {
                    font: {
                        color: 'white',
                        size: 11
                    },
                    customizeText: function (arg) {
                        return arg.valueText + " %";
                    }
                }
            },
            rangeContainer: {
                ranges: [
                    { startValue: 0, endValue: 60, color: '#FF5652' },
                    { startValue: 60, endValue: 90, color: '#FFC107' },
                    { startValue: 90, endValue: 120, color: '#319b6d' }
                ]
            },
            size: {
                height: 100
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: Math.round(arg.valueText) + ' %'
                    };
                },
                font: {
                    color: '#39BBB0',
                    size: 40
                }
            },
        });
    }


    $("#gaugeCategoriaA").dxCircularGauge({
        value: dataGaugeCategoria[0].porcentaje_visita,
        valueIndicator: {
            type: 'rectangleNeedle',
            color: 'white',
            offset: 2
        },
        scale: {
            startValue: 0,
            endValue: 120,
            tickInterval: 30,
            tick: {
                visible: false,
                color: 'black'
            },
            minorTick: {
                visible: true,
                color: '#rgba(0,0,0,0.2)'
            },
            label: {
                font: {
                    color: 'white',
                    size: 11
                },
                customizeText: function (arg) {
                    return arg.valueText + " %";
                }
            }
        },
        rangeContainer: {
            ranges: [
                { startValue: 0, endValue: 60, color: '#FF5652' },
                { startValue: 60, endValue: 90, color: '#FFC107' },
                { startValue: 90, endValue: 120, color: '#319b6d' }
            ]
        },
        size: {
            height: 105
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (arg) {
                return {
                    text: Math.round(arg.valueText) + ' %'
                };
            },
            font: {
                color: '#39BBB0',
                size: 40
            }
        },
    });

    $("#gaugeCategoriaB").dxCircularGauge({
        value: dataGaugeCategoria[1].porcentaje_visita,
        valueIndicator: {
            type: 'rectangleNeedle',
            color: 'white',
            offset: 2
        },
        scale: {
            startValue: 0,
            endValue: 120,
            tickInterval: 30,
            tick: {
                visible: false,
                color: 'black'
            },
            minorTick: {
                visible: true,
                color: '#rgba(0,0,0,0.2)'
            },
            label: {
                font: {
                    color: 'white',
                    size: 11
                },
                customizeText: function (arg) {
                    return arg.valueText + " %";
                }
            }
        },
        rangeContainer: {
            ranges: [
                { startValue: 0, endValue: 60, color: '#FF5652' },
                { startValue: 60, endValue: 90, color: '#FFC107' },
                { startValue: 90, endValue: 120, color: '#319b6d' }
            ]
        },
        size: {
            height: 100
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (arg) {
                return {
                    text: Math.round(arg.valueText) + ' %'
                };
            },
            font: {
                color: '#39BBB0',
                size: 40
            }
        },
    });

    $("#gaugeCategoriaC").dxCircularGauge({
        value: dataGaugeCategoria[2].porcentaje_visita,
        valueIndicator: {
            type: 'rectangleNeedle',
            color: 'white',
            offset: 2
        },
        scale: {
            startValue: 0,
            endValue: 120,
            tickInterval: 30,
            tick: {
                visible: false,
                color: 'black'
            },
            minorTick: {
                visible: true,
                color: '#rgba(0,0,0,0.2)'
            },
            label: {
                font: {
                    color: 'white',
                    size: 11
                },
                customizeText: function (arg) {
                    return arg.valueText + " %";
                }
            }
        },
        rangeContainer: {
            ranges: [
                { startValue: 0, endValue: 60, color: '#FF5652' },
                { startValue: 60, endValue: 90, color: '#FFC107' },
                { startValue: 90, endValue: 120, color: '#319b6d' }
            ]
        },
        size: {
            height: 100
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (arg) {
                return {
                    text: Math.round(arg.valueText) + ' %'
                };
            },
            font: {
                color: '#39BBB0',
                size: 40
            }
        },
    });
    
    //TORTA
    

    //COBERTURA CLIENTES
    if(!isEmpty(cobertura_total_cliente)){
        $("#coberturaTotalCliente").css('display', '');
        $("#chartCoberturaTotalCliente").dxChart({
            dataSource: cobertura_total_cliente,
            commonSeriesSettings: {
                argumentField: "dia",
                type: "stackedBar"
            },
            size: {
                height: 220,
            },
            series: [
                { valueField: "visita_institucion", name: "Puntos de ventas", color: '#03A9F4' },
                { valueField: "visita_profesional", name: "Profesionales", color: '#FF6B68' },
                { valueField: "visita_hospitalario", name: "Hospitalarios", color: '#D559EA' }
            ],
            legend: {
                verticalAlignment: "top",
                horizontalAlignment: "center",
                itemTextPosition: "right"
            },
            valueAxis: {
                visible: true,
                min: 1
            },
            tooltip: {
                enabled: true,
                location: "edge",
                customizeTooltip: function (arg) {
                    return {
                        text: arg.seriesName + " : " + arg.valueText
                    };
                },
                font: {
                    color: '#39BBB0',
                    size: 16
                }
            }
        });
    } else { $("#coberturaTotalCliente").css('display', 'none'); }

    //COBERTURA POR TIPO CLIENTE

    if(!isEmpty(cobertura_total_institucion)){
        $("#coberturaTotalInstitucion").css('display', '');
        $("#chartCoberturaTotalInstitucion").dxChart({
            dataSource: cobertura_total_institucion,
            size: {
                height: 220,
            },
            barWidth: 0.5,
            series: [
                { valueField: "real_visita", name: 'Cantidad', type: "bar", color: '#8de8c0', argumentField: "dia" }
            ],
            legend: {
                horizontalAlignment: "right",
                position: "inside",
                border: { visible: true }
            },
            valueAxis: {
                visible: true,
                min: 1
            },
            customizeLabel: function() {
                if (this.value > 0) {
                    return {
                        visible: true,
                        backgroundColor: "#39BBB0",
                        customizeText: function () {
                            return this.valueText;
                        }
                    };
                }
            },
        });
    } else { $("#coberturaTotalInstitucion").css('display', 'none'); }
    
    if(!isEmpty(cobertura_total_profesional)){
        $("#coberturaTotalProfesional").css('display', '');
        $("#chartCoberturaTotalProfesional").dxChart({
            dataSource: cobertura_total_profesional,
            size: {
                height: 220,
            },
            valueAxis: {
                visible: true,
                min: 1
            },
            barWidth: 0.5,
            series: [
                { valueField: "real_visita", name: 'Cantidad', type: "bar", color: '#8de8c0', argumentField: "dia" }
            ],
            legend: {
                horizontalAlignment: "right",
                position: "inside",
                border: { visible: true }
            },
            customizeLabel: function() {
                if (this.value > 0) {
                    return {
                        visible: true,
                        backgroundColor: "#39BBB0",
                        customizeText: function () {
                            return this.valueText;
                        }
                    };
                }
            },
        });
    } else { $("#coberturaTotalProfesional").css('display', 'none'); }

    if(!isEmpty(cobertura_total_hospitalario)){
        $("#coberturaTotalHospitalario").css('display', '');
        $("#chartCoberturaTotalHospitalario").dxChart({
            dataSource: cobertura_total_hospitalario,
            size: {
                height: 220,
            },
            valueAxis: {
                visible: true,
                min: 1
            },
            barWidth: 0.5,
            series: [
                { valueField: "real_visita", name: 'Cantidad', type: "bar", color: '#8de8c0', argumentField: "dia" }
            ],
            legend: {
                horizontalAlignment: "right",
                position: "inside",
                border: { visible: true }
            },
            customizeLabel: function() {
                if (this.value > 0) {
                    return {
                        visible: true,
                        backgroundColor: "#39BBB0",
                        customizeText: function () {
                            return this.valueText;
                        }
                    };
                }
            },
        });
    } else { $("#coberturaTotalHospitalario").css('display', 'none'); }
    //CATEGORIA POR TIPO CLIENTE

    if(!isEmpty(categoria_total_institucion)){
        $("#categoriaInstitucion").css('display', '');
        $("#chartCategoriaInstitucion").dxChart({
            dataSource: categoria_total_institucion,
            commonSeriesSettings: {
                argumentField: "dia"
            },
            size: {
                height: 220,
            },
            series: [
                { valueField: "categoria_a", name: 'A', type: "line", color: '#65D5A5' }
                , { valueField: "categoria_b", name: 'B', type: "line", color: '#FFC107' }
                , { valueField: "categoria_c", name: 'C', type: "line", color: '#D559EA' }
                // , { valueField: "categoria_d", name: 'D', type: "line", color: '#FF5652' }
            ],
            legend: {
                horizontalAlignment: "right",
                position: "inside",
                border: { visible: true }
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: Math.round(arg.valueText)
                    };
                },
                font: {
                    color: '#39BBB0',
                    size: 40
                }
            },
            valueAxis: {
                visible: true,
                min: 0
            }
        });
    } else { $("#categoriaInstitucion").css('display', 'none'); }

    if(!isEmpty(categoria_total_profesional)){
        $("#categoriaProfesional").css('display', '');
        $("#chartCategoriaProfesional").dxChart({
            dataSource: categoria_total_profesional,
            size: {
                height: 220,
            },
            valueAxis: {
                visible: true,
                min: 1
            },
            series: [
                { valueField: "categoria_a", name: 'A', type: "line", color: '#65D5A5', argumentField: "dia" }
                , { valueField: "categoria_b", name: 'B', type: "line", color: '#FFC107', argumentField: "dia" }
                , { valueField: "categoria_c", name: 'C', type: "line", color: '#D559EA', argumentField: "dia"}
                // , { valueField: "categoria_d", name: 'D', type: "line", color: '#FF5652' }
            ],
            legend: {
                horizontalAlignment: "right",
                position: "inside",
                border: { visible: true }
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: Math.round(arg.valueText)
                    };
                },
                font: {
                    color: '#39BBB0',
                    size: 40
                }
            },
            valueAxis: {
                visible: true,
                min: 0
            }
        });
    } else { $("#categoriaProfesional").css('display', 'none'); }

    if(!isEmpty(categoria_total_hospitalario)){
        $("#categoriaHospitalario").css('display', '');
        $("#chartCategoriaHospitalario").dxChart({
            dataSource: categoria_total_hospitalario,
            commonSeriesSettings: {
                argumentField: "dia"
            },
            size: {
                height: 220,
            },
            series: [
                { valueField: "categoria_a", name: 'A', type: "line", color: '#65D5A5' }
                , { valueField: "categoria_b", name: 'B', type: "line", color: '#FFC107' }
                , { valueField: "categoria_c", name: 'C', type: "line", color: '#D559EA' }
                // , { valueField: "categoria_d", name: 'D', type: "line", color: '#FF5652' }
            ],
            legend: {
                horizontalAlignment: "right",
                position: "inside",
                border: { visible: true }
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: Math.round(arg.valueText)
                    };
                },
                font: {
                    color: '#39BBB0',
                    size: 40
                }
            },
            valueAxis: {
                visible: true,
                min: 0
            }
        });
    } else { $("#categoriaHospitalario").css('display', 'none'); }
    
    if(!isEmpty(dataChartAgenda)){
        $("#agendaChart").css('display', '');
        $("#chartAgenda").dxChart({
            dataSource: dataChartAgenda,
            commonSeriesSettings: {
                argumentField: "dia"
            },
            size: {
                height: 220,
            },
            series: [
                { valueField: "planificado_no_visitado", name: 'Planificado no visitado', type: "line", color: '#65D5A5' }
                , { valueField: "razon_no_visita", name: 'Razón no visitado', type: "line", color: '#FFC107' }
                , { valueField: "visitado", name: 'Visitado', type: "line", color: '#D559EA' }
                // , { valueField: "categoria_d", name: 'D', type: "line", color: '#FF5652' }
            ],
            legend: {
                horizontalAlignment: "right",
                position: "inside",
                border: { visible: true }
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: Math.round(arg.valueText)
                    };
                },
                font: {
                    color: '#39BBB0',
                    size: 40
                }
            },
            valueAxis: {
                visible: true,
                min: 0
            }
        });
    } else { $("#agendaChart").css('display', 'none'); }

    //TORTA AGENDA POR TIPO CLIENTE
    if(!isEmpty(dataPieChartAgendaInstitucion)){
        $("#agendaPieInstitucion").css('display', '');
        $("#pieChartAgendaInstitucion").dxPieChart({
            dataSource: dataPieChartAgendaInstitucion,
            // sizeGroup: "tortas",
            diameter: 0.55,
            palette: ['#ff6b68','#d066e2','#00BCD4','#f5c942','#8BC34A','#39bbb0','#CDDC39','#ff85af','#FF5722'],
            series: [
                {
                    argumentField: "estado",
                    valueField: "cantidad",
                    label: {
                        visible: true,
                        font: {
                            size: 12
                        },
                        connector: {
                            visible: true,
                            width: 0.5
                        },
                        position: "columns",
                        customizeText: function(arg) {
                            return arg.valueText + " (" + arg.percentText + ")";
                        }
                    }
                }
            ],
            resolveLabelOverlapping: "shift",
            legend: {
                orientation: "horizontal",
                itemTextPosition: "right",
                horizontalAlignment: "left",
                verticalAlignment: "bottom",
                columnCount: 2,
                font:{
                    size: 11
                },
            },
            onPointClick: function (e) {
                var point = e.target;
                toggleVisibility(point);
            },
            onLegendClick: function (e) {
                var arg = e.target;
                toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
            }
        }).dxPieChart("instance");
    } else { $("#agendaPieInstitucion").css('display', 'none'); }

    if(!isEmpty(dataPieChartAgendaProfesional)){
        $("#agendaPieProfesional").css('display', '');
        $("#pieChartAgendaProfesional").dxPieChart({
            dataSource: dataPieChartAgendaProfesional,
            // sizeGroup: "tortas",
            diameter: 0.55,
            palette: ['#ff6b68','#d066e2','#00BCD4','#f5c942','#8BC34A','#39bbb0','#CDDC39','#ff85af','#FF5722'],
            series: [
                {
                    argumentField: "estado",
                    valueField: "cantidad",
                    label: {
                        visible: true,
                        font: {
                            size: 12
                        },
                        connector: {
                            visible: true,
                            width: 0.5
                        },
                        position: "columns",
                        customizeText: function(arg) {
                            return arg.valueText + " (" + arg.percentText + ")";
                        }
                    }
                }
            ],
            resolveLabelOverlapping: "shift",
            legend: {
                orientation: "horizontal",
                itemTextPosition: "right",
                horizontalAlignment: "left",
                verticalAlignment: "bottom",
                columnCount: 2,
                font:{
                    size: 11
                },
            },
            onPointClick: function (e) {
                var point = e.target;
                toggleVisibility(point);
            },
            onLegendClick: function (e) {
                var arg = e.target;
                toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
            }
        });
    } else { $("#agendaPieProfesional").css('display', 'none'); }

    if(!isEmpty(dataPieChartAgendaHospitalario)){
        $("#agendaPieHospitalario").css('display', '');
        $("#pieChartAgendaHospitalario").dxPieChart({
            dataSource: dataPieChartAgendaHospitalario,
            // sizeGroup: "tortas",
            diameter: 0.55,
            palette: ['#ff6b68','#d066e2','#00BCD4','#f5c942','#8BC34A','#39bbb0','#CDDC39','#ff85af','#FF5722'],
            series: [
                {
                    argumentField: "estado",
                    valueField: "cantidad",
                    label: {
                        visible: true,
                        font: {
                            size: 12
                        },
                        connector: {
                            visible: true,
                            width: 0.5
                        },
                        position: "columns",
                        customizeText: function(arg) {
                            return arg.valueText + " (" + arg.percentText + ")";
                        }
                    }
                }
            ],
            resolveLabelOverlapping: "shift",
            legend: {
                orientation: "horizontal",
                itemTextPosition: "right",
                horizontalAlignment: "left",
                verticalAlignment: "bottom",
                columnCount: 2,
                font:{
                    size: 11
                },
            },
            onPointClick: function (e) {
                var point = e.target;
                toggleVisibility(point);
            },
            onLegendClick: function (e) {
                var arg = e.target;
                toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
            }
        });
    } else { $("#agendaPieHospitalario").css('display', 'none'); }

}

function loadDataConfig(){
    $.ajax({
        url: "../rest/resource/getCiclo",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    })
    .done(function (data) {
        dataCiclo = data[0];
    });

    $.ajax({
        url: "../rest/resource/getRepresentante",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    })
    .done(function (data) {
        dataRepresentante = data[0];
    });
}

function loadDataInicio() {

    $.ajax({
        url: "../rest/inicioSupervisor/getDataInicioSupervisor",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    })
    .done(function (data) {
        metaCiclo = data[0][0].meta_ciclo;
        metaAFecha = data[1][0].meta_a_fecha;
        realizadas = data[2][0].visitas_realizadas;
        canceladas = data[3][0].visitas_canceladas;
        dataGaugeCategoria = data[4];
        dataGaugeCliente = data[5];
        if(typeof data[6][0].vacio != "number"){ cobertura_total_institucion = data[6]; }
        if(typeof data[7][0].vacio != "number"){ cobertura_total_profesional = data[7]; }
        if(typeof data[8][0].vacio != "number"){ cobertura_total_hospitalario = data[8]; }
        if(typeof data[9][0].vacio != "number"){ categoria_total_institucion = data[9]; }
        if(typeof data[10][0].vacio != "number"){ categoria_total_profesional = data[10]; }
        if(typeof data[11][0].vacio != "number"){ categoria_total_hospitalario = data[11]; }
        if(typeof data[12][0].vacio != "number"){ cobertura_total_cliente = data[12]; }
        if(typeof data[13][0].vacio != "number"){ dataPieChartAgendaInstitucion = data[13]; }
        if(typeof data[14][0].vacio != "number"){ dataPieChartAgendaProfesional = data[14]; }
        if(typeof data[15][0].vacio != "number"){ dataPieChartAgendaHospitalario = data[15]; }
        if(typeof data[16][0].vacio != "number"){ dataChartAgenda = data[16]; }
        controles();
        resultData.resolve();
    });
}

function resetData(){
    metaCiclo = 0;
    metaAFecha = 0;
    realizadas = 0;
    canceladas = 0;
    dataGaugeCategoria = 0;
    dataGaugeCliente = 0;
    cobertura_total_institucion = [];
    cobertura_total_profesional = [];
    cobertura_total_hospitalario = [];
    categoria_total_institucion = [];
    categoria_total_profesional = [];
    categoria_total_hospitalario = [];
    cobertura_total_cliente = [];
    dataPieChartAgendaInstitucion = [];
    dataPieChartAgendaProfesional = [];
    dataPieChartAgendaHospitalario = [];
    dataChartAgenda = [];
}

function loadDataSelectBox(){
    resetData();
    var cicloId, representanteId;
    if(selectBoxCiclo.option().value > 0){
        cicloId = selectBoxCiclo.option().value;
    } else {
        cicloId = '';
    }
    if(selectBoxRepresenante.option().value > 0){
        representanteId = selectBoxRepresenante.option().value;
    }else{
        representanteId = '';
    }
    $.ajax({
        url: "../rest/inicioSupervisor/getDataSelectBoxSupervisor",
        dataType: "json",
        data: {
            representante_id : representanteId
            , ciclo_id : cicloId
        },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    })
    .done(function (data) {
        metaCiclo = data[0][0].meta_ciclo;
        metaAFecha = data[1][0].meta_a_fecha;
        realizadas = data[2][0].visitas_realizadas;
        canceladas = data[3][0].visitas_canceladas;
        dataGaugeCategoria = data[4];
        dataGaugeCliente = data[5];
        if(typeof data[6][0].vacio != "number"){ cobertura_total_institucion = data[6]; }
        if(typeof data[7][0].vacio != "number"){ cobertura_total_profesional = data[7]; }
        if(typeof data[8][0].vacio != "number"){ cobertura_total_hospitalario = data[8]; }
        if(typeof data[9][0].vacio != "number"){ categoria_total_institucion = data[9]; }
        if(typeof data[10][0].vacio != "number"){ categoria_total_profesional = data[10]; }
        if(typeof data[11][0].vacio != "number"){ categoria_total_hospitalario = data[11]; }
        if(typeof data[12][0].vacio != "number"){ cobertura_total_cliente = data[12]; }
        if(typeof data[13][0].vacio != "number"){ dataPieChartAgendaInstitucion = data[13]; }
        if(typeof data[14][0].vacio != "number"){ dataPieChartAgendaProfesional = data[14]; }
        if(typeof data[15][0].vacio != "number"){ dataPieChartAgendaHospitalario = data[15]; }
        if(typeof data[16][0].vacio != "number"){ dataChartAgenda = data[16]; }
        controles();
    });
}