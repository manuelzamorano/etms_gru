var promiseCultureLoad = $.Deferred();

$.when(
    $.getJSON('../js/cldr/supplemental/likelySubtags.json'),
    $.getJSON('../js/cldr/main/es/numbers.json'),
    $.getJSON('../js/cldr/supplemental/numberingSystems.json'),
    $.getJSON('../js/cldr/main/es/ca-gregorian.json'),
    $.getJSON('../js/cldr/main/es/timeZoneNames.json'),
    $.getJSON('../js/cldr/supplemental/timeData.json'),
    $.getJSON('../js/cldr/supplemental/weekData.json')
).then(function() {
  // Normalize $.get results, we only need the JSON, not the request statuses.
  return [].slice.apply( arguments, [0]).map(function(result) {
      return result[0];
  });
}).then(Globalize.load).then(function() {
    Globalize.locale('es');
    DevExpress.localization.locale('es');
    promiseCultureLoad.resolve();
});

function detenerLoadingPanel(){
    setTimeout(function() {
        $(".page-loader").fadeOut()
    }, 500)
}

$(document).ready(function(){
    setNombrePerfilUsuarioMenu();
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function Uint8ToString(u8a){
  var CHUNK_SZ = 0x8000;
  var c = [];
  for (var i=0; i < u8a.length; i+=CHUNK_SZ) {
    c.push(String.fromCharCode.apply(null, u8a.subarray(i, i+CHUNK_SZ)));
  }
  return c.join("");
}

var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

function setNombrePerfilUsuarioMenu(){
    $.ajax({
        dataType: "json",
        'async': true,
        url: "../rest/datosUsuario",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        $(".usuario_nombre").text(data.nombre_completo);
        $(".usuario_rol").text(data.rol_descriptiva);
    });
}

function toggleVisibility(item) {
    if(item.isVisible()) {
        item.hide();
    } else { 
        item.show();
    }
}

Date.prototype.sameDay = function(d) {
    return this.getFullYear() === d.getFullYear()
        && this.getDate() === d.getDate()
        && this.getMonth() === d.getMonth();
}

function textBox_onKeyDown_rut(e){
    var event = e.jQueryEvent;
    var keyCode = event.keyCode; 
    // Allow: BackSpace, enter, suprimir, tab
    if ($.inArray(keyCode, [8,13,46,9]) !== -1 ||
        // Allow: Ctrl+A
        (keyCode == 65 && event.ctrlKey === true) ||
        // Allow: Ctrl+C
        (keyCode == 67 && event.ctrlKey === true) ||
        // Allow: Ctrl+X
        (keyCode == 88 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
        (keyCode >= 35 && keyCode <= 39) ||
        //numeros teclado extendido
        (keyCode >= 96 && keyCode <= 105)
        ) {
        // Let it happen, don't do anything
        return;
    }
    //	Letra K
    else if ($.inArray(keyCode, [75]) !== -1){
        return
    }
    // Es numero
    else if(keyCode<48 ||keyCode>57){
        event.preventDefault();
    }			
}

function textBox_onKeyPress_numeroEntero(e){
    var event = e.jQueryEvent;			
    
    if(event.keyCode !== 0)
        str = String.fromCharCode(event.keyCode);			
    else 
        str = String.fromCharCode(event.charCode);			
    
    if(!/[0-9]/.test(str))
        event.preventDefault();
}

function textBox_onKeyPress_numeroDecimal(e){
    var event = e.jQueryEvent,
    str = String.fromCharCode(event.keyCode);
    if(!/[0-9+.]/.test(str))
        event.preventDefault();
}

function getFechaExportarExcel(){
    var fechaActual = new Date();
    return fechaActual.getDate().toString()+(fechaActual.getUTCMonth()+1).toString()+fechaActual.getFullYear().toString();
}