var loadPanel;
var resultData;
var dataGridCoberturaInstitucion;
var dataGridCoberturaProfesional;
var dataGridCoberturaHospitalario;
var cicloId = null;

$.when(promiseCultureLoad).done(function () {

    
    $("#selectBoxCiclo").dxSelectBox({  
        valueExpr: 'ciclo_id',
        displayExpr: 'ciclo_descriptiva',
        width: 300,
        onSelectionChanged: function (e) {
            var cicloId = $("#selectBoxCiclo").dxSelectBox('instance').option().value;
            loadDataDataGrid(cicloId);
        }
    });

    cargarCiclo();

    dataGridCoberturaInstitucion = $("#dataGridCoberturaInstitucion").dxDataGrid({
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "VisitasLocales",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total Visitas: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            { caption: "Local", dataField: "institucion_local", width: 80 },
            { caption: "Código Local", dataField: "institucion_codigo", width: 120 },
            { caption: "Cadena", dataField: "cadena_descriptiva", width: 120 },
            { caption: "Fecha visita", dataField: "fecha_visita", width: 120 },
            { caption: "Fecha ingreso", dataField: "fecha_ingreso", width: 120 },
            { caption: "Representante", dataField: "representante_nombre", width: 180 },
            { caption: "Tipo visita", dataField: "tipo_visita_descriptiva", width: 120 },
            { 
                allowReordering:true
                , caption: "Información"
                , dataField: "visita_informacion"
                , width: 300
                , cellTemplate: function (container, options) { 
                    $("<div />").dxTextArea({
                        value: options.data.visita_informacion,
                        readOnly: true,
                        height: 70
                    }).appendTo(container);
                },
            },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');

    dataGridCoberturaProfesional = $("#dataGridCoberturaProfesional").dxDataGrid({
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "VisitasLocales",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total Visitas: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            { caption: "Rut", dataField: "profesional_codigo", width: 90 },
            { caption: "Nombre", dataField: "profesional_nombre", width: 330 },
            { caption: "Especialidad", dataField: "especialidad_descriptiva", width: 120 },
            { caption: "Especialidad 2", dataField: "especialidad_2", width: 120 },
            { caption: "Fecha visita", dataField: "fecha_visita", width: 120 },
            { caption: "Fecha ingreso", dataField: "fecha_ingreso", width: 120 },
            { caption: "Representante", dataField: "representante_nombre", width: 180 },
            { caption: "Tipo visita", dataField: "tipo_visita_descriptiva", width: 120 },
            { 
                caption: "Información"
                , dataField: "visita_informacion"
                , width: 300
                , cellTemplate: function (container, options) { 
                    $("<div />").dxTextArea({
                        value: options.data.visita_informacion,
                        readOnly: true,
                        height: 70
                    }).appendTo(container);
                },
            },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');

    dataGridCoberturaHospitalario = $("#dataGridCoberturaHospitalario").dxDataGrid({
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "VisitasLocales",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total Visitas: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            { caption: "Código", dataField: "hospitalario_codigo", width: 100 },
            { caption: "Nombre", dataField: "hospitalario_nombre", width: 330 },
            { caption: "Contacto", dataField: "hospitalario_contact", width: 150 },
            { caption: "Fecha visita", dataField: "fecha_visita", width: 120 },
            { caption: "Fecha ingreso", dataField: "fecha_ingreso", width: 120 },
            { caption: "Representante", dataField: "representante_nombre", width: 180 },
            { caption: "Tipo visita", dataField: "tipo_visita_descriptiva", width: 120 },
            { 
                caption: "Información"
                , dataField: "visita_informacion"
                , width: 300
                , cellTemplate: function (container, options) { 
                    $("<div />").dxTextArea({
                        value: options.data.visita_informacion,
                        readOnly: true,
                        height: 70
                    }).appendTo(container);
                },
            },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');
    
    loadDataDataGrid(cicloId);

    detenerLoadingPanel();

});

function loadDataDataGrid(cicloId) {
    $.ajax({
        dataType: "json",
        'async': false,
        data : { ciclo_id : cicloId },
        url: '../rest/coberturaSupervisor/getDataCoberturaSupervisor',
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataGridCoberturaInstitucion.option('dataSource', data[0]);
        dataGridCoberturaProfesional.option('dataSource', data[1]);
        dataGridCoberturaHospitalario.option('dataSource', data[2]);
    });
}

function cargarCiclo(){
    $.ajax({
        url: "../rest/resource/getCiclo",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        $("#selectBoxCiclo").dxSelectBox('instance').option('dataSource', data[0]);
    });
}