var loadPanel, resultData, dataRealizadas,dataRepresentante;
$.when(promiseCultureLoad).done(function () {
    resultData = $.Deferred();
    LoadDataRepresentante();
    LoadDataDataGrid();
    $.when(resultData).done(function () {
        $("#dataGridVisita").dxDataGrid({
            dataSource: dataDataGrid,
            rowAlternationEnabled: true,
            showBorders: true,
            "export": {
                enabled: true,
                fileName: "VisitasLocales",
                excelFilterEnabled: true
            },
            columnAutoWidth: true,
            columnFixing: {
                enabled: true
            },
            columns: [
                { 
                    allowReordering: true
                    , caption: "Representante"
                    , dataField: "representante_id"
                    , width: 160
                    , fixed: true
                    , lookup: { dataSource: dataRepresentante, valueExpr: 'representante_id', displayExpr: 'representante_descriptiva' } 
                },
                { caption: "Rut", dataField: "rut", width: 85, fixed: true },
                { caption: "Cliente", dataField: "nombre", width: 330, fixed: true },
                { caption: "Contacto", dataField: "contacto", width: 330 },
                { caption: "Dirección", dataField: "calle", width: 250 },
                { caption: "Brick", dataField: "brick", width: 200 },
                { caption: "Cat. Cia", dataField: "categoria_cia", width: 110 },
                { caption: "Real", dataField: "real_visitas", width: 90 },
                { caption: "Plan", dataField: "frecuencia", width: 90 },
                { caption: "%", dataField: "cumplimiento", width: 50 },
                { caption: "Última Visita", dataField: "fecha_ultima_visita", width: 100 },
                { 
                    allowReordering: true
                    , caption: "Última Información"
                    , dataField: "informacion_ultima_visita"
                    , width: 200
                    , cellTemplate: function (container, options) { 
                        $("<div />").dxTextArea({
                            value: options.data.informacion_ultima_visita,
                            readOnly: true,
                            height: 70
                        }).appendTo(container);
                    },
                },
                { allowReordering: true, caption: "Visita N -2", dataField: "fecha_visita_n1", width: 100 },
                { 
                    allowReordering:true
                    , caption: "Información N -2"
                    , dataField: "informacion_visita_n1"
                    , width: 200
                    , cellTemplate: function (container, options) { 
                        $("<div />").dxTextArea({
                            value: options.data.informacion_visita_n1,
                            readOnly: true,
                            height: 70
                        }).appendTo(container);
                    },
                },
                { allowReordering: true, caption: "Visita N -3", dataField: "fecha_visita_n2", width: 100 },
                { 
                    allowReordering: true
                    , caption: "Información N -3"
                    , dataField: "informacion_visita_n2"
                    , width: 200
                    , cellTemplate: function (container, options) { 
                        $("<div />").dxTextArea({
                            value: options.data.informacion_visita_n2,
                            readOnly: true,
                            height: 70
                        }).appendTo(container);
                    },
                },
                { allowReordering: true, caption: "Visita N -4", dataField: "fecha_visita_n3", width: 100 },
                { 
                    allowReordering: true
                    , caption: "Información N -4"
                    , dataField: "informacion_visita_n3"
                    , width: 200
                    , cellTemplate: function (container, options) { 
                        $("<div />").dxTextArea({
                            value: options.data.fecha_visita_n3,
                            readOnly: true,
                            height: 70
                        }).appendTo(container);
                    },
                },
            ],
            summary: {
                totalItems: [
                    {
                        column: 'representante_id',
                        summaryType: 'count',
                        displayFormat: "Total: {0}",
                    },
                    {
                        column: 'frecuencia',
                        displayFormat: "Plan: {0}",
                        summaryType: 'sum',
                    },
                    {
                        column: 'real_visitas',
                        displayFormat: "Real: {0}",
                        summaryType: 'sum',
                    },
                    {
                        name: "cumplimiento",
                        showInColumn: "cumplimiento",
                        displayFormat: "{0} %",
                        valueFormat: "decimal",
                        precision: 1,
                        summaryType: "custom"
                    },
                    {
                        name: "cumplimiento_meta",
                        showInColumn: "categoria_cia",
                        displayFormat: "Meta: {0}",
                        valueFormat: "decimal",
                        summaryType: "custom"
                    }
                ],
                calculateCustomSummary: function (options) {
                    if (options.name === "cumplimiento") {
                        if (options.summaryProcess === "start") {
                            options.totalValue = 0;
                            options.sumaFrecuencia = 0;
                            options.sumaReal = 0;
                            options.contador = 0;
                            options.totalCumplimiento = 0;
                        }
                        if (options.summaryProcess === "calculate") {
                            options.sumaReal = options.sumaReal + options.value.real_visitas;
                            options.sumaFrecuencia = options.sumaFrecuencia + options.value.frecuencia;
                            options.contador = options.contador + 1;
                            options.totalCumplimiento = options.value.cumplimiento_franquicia_a_fecha;
                        }
                        if (options.summaryProcess === "finalize") {
                            var totalReal = options.sumaReal;
                            var totalFrecuencia = options.totalCumplimiento;
                            options.totalValue = (totalReal / totalFrecuencia) * 100;
                        }
                    }
                    if (options.name === "cumplimiento_meta") {
                        if (options.summaryProcess === "start") {
                            options.totalValue = 0;
                        }
                        if (options.summaryProcess === "calculate") {
                            options.totalValue = options.value.cumplimiento_franquicia;
                        }
                        if (options.summaryProcess === "finalize") {
                            options.totalValue = options.totalValue;
                        }
                    }
                }
            },
            paging: {
                enabled: true, pageSize: 5
            },
            filterRow: {
                visible: true
            },
            paging: {
                pageSize: 5
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20]
            },
            width: '100%'
        });
        detenerLoadingPanel();
    });
});

function LoadDataDataGrid() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: '../rest/visitasSupervisor/getDataVisitasHospitalario',
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataDataGrid = data[0];
        resultData.resolve();
    });
}

function LoadDataRepresentante() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: '../rest/resource/getRepresentante',
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataRepresentante = data[0];
    });
}