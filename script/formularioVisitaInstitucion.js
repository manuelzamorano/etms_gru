
var cliente, eventoId, dataCliente, dataGridProducto, dataPregunta;
var dataConfiguracionCiclo, dataSelectBoxTipoVisita;
var currentLatitud = '';
var currentLongitud = '';
var dataPromocionalReal = [
    { value : 0, descriptiva : 'No' }
    , { value : 1, descriptiva : 'Si' }
];

$.when(promiseCultureLoad).done(function () {
    detenerLoadingPanel();
    //Data Loaders
    cliente = getParameterByName('id');
    eventoId = getParameterByName('eventId');
    loadDataCliente(cliente);
    //Data Labels
    if (dataCliente[0]) {
        $("#nombres").text(dataCliente[0].nombre);
        $("#cadena").text(dataCliente[0].cadena_descriptiva);
        $("#codigo").text(dataCliente[0].codigo);
        $("#categoria_compania").text(dataCliente[0].categoria_compania_descriptiva);
        $("#frecuencia").text(dataCliente[0].frecuencia);
        $("#ciclo").text(dataCliente[0].ciclo);
        $("#cicloFechaInicio").text(dataCliente[0].ciclo_fecha_inicio);
        $("#cicloFechaTermino").text(dataCliente[0].ciclo_fecha_termino);
    } else {
        console.log('Error al mostrar los datos');
    }

    // //Grilla Producto
    $("#gridProducto").dxDataGrid({
        dataSource: dataGridProducto,
        rowAlternationEnabled: true,
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        columns: [
            { caption: "Producto_id", dataField: "producto_id", dataType: "number", visible: false },
            {
                caption: "Real promocional", dataField: "real_promocion", width: 130, fixed: true,
                cellTemplate: function(container, options){
                    $("<div id='realPromocionProductoId_" + options.data.producto_id +  "' class='select_box_real_promocion'></div>").appendTo(container);
                }
            },
            { caption: "Plan promocional", dataField: "producto_descriptiva", width: 150 },
            { caption: "Peso", dataField: "peso", width: 60 },
            { caption: "Plan MM", dataField: "cantidad_plan", width: 120 },
            {
                caption: "Real MM", dataField: "real", width: 120,
                cellTemplate: function(container, options){
                    $("<div id='realProductoId_" + options.data.producto_id +  "' class='number_box_real'></div>").appendTo(container);
                }
            },
            // { caption: "Potencial", dataField: "categoria_potencial_descriptiva", width: 90 },
            // { caption: "Adopción", dataField: "categoria_adopcion_descriptiva", width: 90 },
            // { caption: "Guia", dataField: "categoria_guia_descriptiva", width: 90 },
            { caption: "Mensaje Promocional", dataField: "mensaje_promocional", width: 700 },
        ],
        width: '100%',
        onContentReady: function(e){
            var numberBoxReal = $('.number_box_real');
            for (var i = 0; i < numberBoxReal.length; i++) {
                $(numberBoxReal[i]).dxNumberBox({
                    min: 0,
                    max: 99,
                    showSpinButtons: true,
                    step: 1,
                    value: 0,
                    width: '100%'
                }).dxValidator({
                    validationRules: [{
                        type: "required"
                        , message: ""
                    }]
                    , validationGroup: 'saveChanges'
                });
            }
            var selectBoxPromocionalReal = $('.select_box_real_promocion');
            for (var i3 = 0; i3 < selectBoxPromocionalReal.length; i3++) {
                $(selectBoxPromocionalReal[i3]).dxSelectBox({
                    dataSource: dataPromocionalReal,
                    displayExpr: 'descriptiva',
                    valueExpr: 'value',
                    value: 1
                }).dxValidator({
                    validationRules: [{
                        type: "required"
                        , message: ""
                    }]
                    , validationGroup: 'saveChanges'
                });
            }
            
        },
    }).dxDataGrid('instance');

    cargarControles();

    obtenerCoordenadas();

});

function loadDataCliente(cliente_id) {
    $.ajax({
        data: { cliente_id: cliente_id },
        url: "../rest/formularioVisitaInstitucion/getDataVisita",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataCliente         = data[0];
        dataGridProducto    = data[1];
        dataSelectBoxTipoVisita = data[2];
        dataConfiguracionCiclo = data[3];
        dataPregunta = data[4];
        dataAlternativa = data[5];
        cargarEncuesta(dataPregunta, dataAlternativa);
    });
}

function saveDataVisita() {

    obtenerCoordenadas();

    var data = {};

    data.cliente_id = cliente;
    data.ciclo_id = dataCliente[0].ciclo_id;
    data.tipo_visita_id =  $("#selectBoxTipoVisita").dxSelectBox("instance").option('value');
    data.fecha =  $("#dateFechaVisita").dxDateBox("instance").option('value');
    data.informacion = $("#textAreaVisitaActual").dxTextArea("instance").option('value');
    data.es_acompañada = $("#checkBoxVisitaAcompanada").dxCheckBox("instance").option('value');
    data.latitud = currentLatitud;
    data.longitud = currentLongitud;

    $.ajax({
        data: data,
        url: "../rest/formularioVisitaInstitucion/insertVisita",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                if(response.responseJSON.number == 53627)
                    swal('Registro duplicado!', 'Esta visita ya fue anteriormente guardada con este fecha!', 'warning')
                        .then(function () {
                        //location.href = "../KardexInstitucion";
                        });
                else
                    swal('Error al guardar!', 'Reporte problema al administrador', 'warning').then(function () {
                        //location.href = "../KardexInstitucion";
                    });
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        var visitaId = data[0][0].visita_id;
        if (!visitaId == 0) {
            guardarDataVisitaProducto(visitaId);
            guardarDataVisitaEncuesta(visitaId);
            swal('Guardada!', 'Su registro esta completo!', 'success').then(function () {
                location.href = "../KardexInstitucion";
            });
        }        
    });
}

function guardarDataVisitaProducto(visitaId) {
    var numberBoxReal = $('.number_box_real');
    // var selectBoxObjetivoVisita = $('.select_box_objetivo_visita');
    var selectBoxRealPromocion = $('.select_box_real_promocion')
    for (var i = 0; i < numberBoxReal.length; i++) {
        var productoId = dataGridProducto[i].producto_id;
        var real = $('#' + numberBoxReal[i].id).dxNumberBox("instance").option('value');
        // var objetivoVisitaId = $('#' + selectBoxObjetivoVisita[i].id).dxSelectBox('instance').option().value;
        var realPromocion = $('#' + selectBoxRealPromocion[i].id).dxSelectBox('instance').option().value;
        $.ajax({
            data: {
                visita_id: visitaId
                , producto_id: productoId
                // , objetivo_visita_id: objetivoVisitaId
                , real_promocional: realPromocion
                , real: real
            },
            url: "../rest/formularioVisitaInstitucion/insertVisitaProducto",
            dataType: "json",
            'async': false,
            error: function () { console.log("No se ha podido guardado los productos"); }
        }).done(function (data) {});
    }
}

function guardarDataVisitaEncuesta(visitaId){
    for (var i = 0; i < dataPregunta.length; i++) {
        var elementoHtml = '#pregunta'+(i+1)
        if ($(elementoHtml).dxSelectBox("instance").option('value') != undefined){
            var alternativaId = $(elementoHtml).dxSelectBox("instance").option('value');
            $.ajax({
                data: {
                    visita_id: visitaId
                    , alternativa_id: alternativaId
                },
                url: "../rest/formularioVisitaInstitucion/insertVisitaEncuesta",
                dataType: "json",
                'async': false,
                error: function () { console.log("No se ha podido guardar encuesta"); }
            }).done(function (data) {});
        }
    }
}

function cargarControles(){
    //Controles devextreme
    $("#buttonGuardarContainer").dxButton({
        validationGroup: 'saveChanges',
        text: 'Guardar Cambios',
        type: 'default',
        width: '100%',
        height: 43,
        onClick: function (go) {
            var result = go.validationGroup.validate();
            if (result.isValid) {
                swal({
                    title: 'Desea guardar la visita?',
                    text: "Esta correcta esta fecha? "+$("#dateFechaVisita").dxDateBox("instance").option("text"),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Si, ingresar!'
                }).then(function () {
                    swal.enableLoading();
                    saveDataVisita();
                });
            } else{
                swal('Faltan campos por completar','','error');
            }
        }
    });

    $("#buttonCancelarContainer").dxButton({
        text: 'Cancelar Visita',
        type: 'danger',
        width: '100%',
        height: 43,
        onClick: function (info) {
            location.href = "../KardexInstitucion";
        }
    });

    $("#checkBoxVisitaAcompanada").dxCheckBox({});

    $("#dateFechaVisita").dxDateBox({
        value: null
        , min: dataConfiguracionCiclo[0].ciclo_fecha_inicio
        , max: dataConfiguracionCiclo[0].ciclo_fecha_termino
        }).dxValidator({
            validationRules: [{
                type: "required"
                , message: "Ingrese la fecha de la visita"
            }]
            , validationGroup: 'saveChanges'
    });

    $("#selectBoxTipoVisita").dxSelectBox({
        dataSource: dataSelectBoxTipoVisita,
        displayExpr: 'descriptiva',
        valueExpr: 'id',
        value: dataSelectBoxTipoVisita[0].id,
        placeholder: 'Seleccione tipo de visita'
        }).dxValidator({
            validationRules: [{
                type: "required"
                , message: "Selecciona un tipo de visita"
            }]
            , validationGroup: 'saveChanges'
    });

    $("#textAreaVisitaActual").dxTextArea({
        placeholder: "Agregue información de visita...",
        maxLength: 2000,
        height: 170,
        width: "100%"
        }).dxValidator({
            validationRules: [{
                type: "required"
                , message: "Debes ingresar la información de la visita"
            }]
            , validationGroup: 'saveChanges'
    });

    $("#textAreaOtroRepresentante").dxTextArea({
        placeholder: "Seleccione un representante...",
        maxLength: 300,
        height: 170,
        width: "100%"
    });

    $("#textBoxComentario").dxTextBox({
        placeholder: '...',
        maxLength: 40,
        mode: 'text',
        width: '100%'
    });
}

function cargarEncuesta(dataPregunta, dataAlternativa){
    var dataTempAlternativa = new Array();
    
    for (var i = 0; i < dataPregunta.length; i++) {
        
        $('#container-encuesta').append(""+
            "<div class='col-md-6 col-sm-6' style='padding-top: 10px;'>"+
                "<div class='m-b-15'>"+
                "<label>"+dataPregunta[i].pregunta_descriptiva+"</label>"+
                    "<div class='form-control input-mask' id='pregunta"+(i+1)+"'></div>"+
                "</div>"+
            "</div>"+
        "");

        for (var e = 0; e < dataAlternativa.length; e++) {
            if(dataPregunta[i].pregunta_id == dataAlternativa[e].pregunta_id){
                dataTempAlternativa.push(dataAlternativa[e]);
            }
        }
        
        $("#pregunta"+(i+1)).dxSelectBox({
            dataSource: dataTempAlternativa,
            displayExpr: 'alternativa_descriptiva',
            valueExpr: 'alternativa_id',
            placeholder: 'Seleccione',
            showClearButton: true,
            width: '100%'
        }).dxSelectBox('instance');

        dataTempAlternativa = [];

    }
}

function obtenerCoordenadas(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            currentLatitud = position.coords.latitude;
            currentLongitud = position.coords.longitude;
        });
    }
}