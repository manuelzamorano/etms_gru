
$( document ).ready(function() {
    var value = 0
    // $("#img").rotate({
    //     bind: {
    //         click: function(){
    //             value +=90;
    //             $(this).rotate({ animateTo:value})
    //         }
    //     }
    // });
    var incidenciaId = getParameterByName('incidencia_id');
    $('#tituloIncidenciaImagen').text(incidenciaId+' Incidencia');
    $.ajax({
        dataType: "json",
        data:{
            incidencia_id : incidenciaId
        },
        'async': false,
        url: '../rest/incidenciaImagen',
        error: function () {
            console.log('No se pudo cargar la data de DataDataGrid');
        }
    }).done(function (data) {
        var u8 = new Uint8Array(data[0][0].incidencia_imagen.data);
        var imagenbase64 = 'data:image/png;base64,'+btoa(Uint8ToString(u8));
    
        $('#divIncidenciaImagen').append("<img"
            + " style='height: -webkit-fill-available;'"
            + " src='"
            + imagenbase64
            +"' />"
        // ).rotate({
        //     bind: {
        //         click: function(){
        //             value +=90;
        //             $(this).rotate({ 
        //                 angle:value
        //                 //, center: ["50%", "100%"],
        //             })
        //         }
        //     }
        // });
        );
    });    

});

function Uint8ToString(u8a){
    var CHUNK_SZ = 0x8000;
    var c = [];
    for (var i=0; i < u8a.length; i+=CHUNK_SZ) {
        c.push(String.fromCharCode.apply(null, u8a.subarray(i, i+CHUNK_SZ)));
    }
    return c.join("");
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}