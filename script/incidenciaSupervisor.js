
var dataGridIncidencia;

$.when(promiseCultureLoad).done(function () {
    LoadDataDataGrid();
    $("#dataGridIncidencia").dxDataGrid({
        dataSource: dataGridIncidencia,
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "Incidencias",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        onRowPrepared: function(e) {
            e.rowElement.css({ height: 70});
        },
        columns: [
            { 
                caption: "Imagen"
                , dataField: "incidencia_imagen"
                , width: '70px'
                , cellTemplate: function (container, options) {
                    if(options.value){
                        var buttonIncidenciaId = "buttonIncidencia"+options.data.incidencia_id;
                        var incidenciaId = options.data.incidencia_id;
                        $("<div />")
                            .attr("id", buttonIncidenciaId)
                            .appendTo(container);
                        $("#"+buttonIncidenciaId).dxButton({
                            icon: 'image'
                            , onClick: function(){
                                window.open("../incidenciaImagen?incidencia_id="+incidenciaId);
                            }
                        });
                    }
                }
            },
            { caption: "Local", dataField: "local_codigo", width: 130 },
            { caption: "Dirección", dataField: "local_direccion", width: 200 },
            { caption: "Fecha", dataField: "incidencia_fecha", width: 200 },
            { caption: "Representante", dataField: "representante", width: 180 },
            { caption: "Producto", dataField: "incidencia_producto", width: 150 },
            { caption: "Quiebre stock", dataField: "incidencia_quiebre_stock", width: 140 },
            { caption: "Stock bajo sugerido", dataField: "incidencia_stock_bajo_sugerido", width: 140 },
            { caption: "No funciona programa paciente", dataField: "incidencia_no_funciona_programa_paciente", width: 140 },
            { caption: "Planograma", dataField: "incidencia_planograma", width: 140 },
            { caption: "Acción competencia", dataField: "incidencia_accion_competencia", width: 140 },
            { caption: "Capacitación realizada", dataField: "incidencia_capacitacion_realizada", width: 140 },
            { caption: "Información especial", dataField: "incidencia_informacion_especial", width: 140 },
            { 
                    allowReordering:true
                    , caption: "Comentario"
                    , dataField: "incidencia_comentario"
                    , width: 300
                    , cellTemplate: function (container, options) { 
                        $("<div />").dxTextArea({
                            value: options.data.incidencia_comentario,
                            readOnly: true,
                            height: 70
                        }).appendTo(container);
                    },
                },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        width: '100%'
    });
    detenerLoadingPanel();
});

function LoadDataDataGrid() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: '../rest/incidenciaSupervisor/getDataIncidencia',
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataGridIncidencia = data[0];
    });
}