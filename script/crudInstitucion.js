
var dataDataGrid;

$.when(promiseCultureLoad).done(function () {
    
    obtenerData();
    crearDataGrid();
    detenerLoadingPanel();

    $("#buttonNuevoInstitucion").click(function() {
        location.href = "../crudInstitucionNuevo";
    });

});


function crearDataGrid(){

    $("#dataGridInstitucion").dxDataGrid({
        dataSource: dataDataGrid,
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "Instituciones",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            {
               caption: "Editar", dataField: "", width: 70, alignment: 'center', fixed: true
               , cellTemplate: function (container, options) {
                   if (options.data) {
                       var dataId = options.data.id;
                       var html
                       html =
                           "<button class='btn btn-primary btn--icon' onclick='onClickEditar("
                           + dataId
                           + ")' > <i class='zmdi zmdi-edit'></i></button>";
                       $(html)
                           .appendTo(container);
                   }
               }
            },
            {
                caption: "Eliminar", dataField: "", width: 70, alignment: 'center', fixed: true
                , cellTemplate: function (container, options) {
                    if (options.data) {
                        var dataId = options.data.id;
                        var html
                        html =
                            "<button class='btn btn-danger btn--icon m-l-5' onclick='onClickEliminar("
                            + dataId
                            + ")' > <i class='zmdi zmdi-delete'></i></button>";
                        $(html)
                            .appendTo(container);
                    }
                }
            },
            { caption:'ID', dataField: 'id', width: 60, fixed: true },
			{ caption:'Código', dataField: 'codigo', width: 100 },
			{ caption:'Cadena', dataField: 'cadena', width: 140 },
			{ caption:'Local', dataField: 'local', width: 100 },
            { caption:'Nombre', dataField: 'nombre', width: 200 },
            { caption:'Dirección', dataField: 'direccion', width: 400 },
			{ caption:'Comuna', dataField: 'comuna', width: 200 },
			{ caption:'Brick', dataField: 'brick', width: 200 },
			{ caption:'Tipo Punto Venta', dataField: 'tipo_institucion', width: 200 },
			{ caption:'Teléfono', dataField: 'telefono', width: 100 },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');
}

function onClickEditar(e){
    var sendData = "?id=" + e;
    location.href = "../crudInstitucionEditar" + sendData;
}

function onClickEliminar(e){
    swal({
        title: 'Desea eliminar el registro'+e+' ?',
        text: "Se eliminará de la base de datos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, eliminar!'
    }).then(function () {
        // swal.enableLoading();
        swal('No Eliminado');
    });
}

function obtenerData() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudInstitucion/obtenerDataInstitucion",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataDataGrid = data[0];
    });
}