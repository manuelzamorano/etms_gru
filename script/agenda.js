
var resultData;
var dataRecord;
var dataClient;
var dataStatus;
var timeout;
var dataEstadoAgenda;
var dataAgregarCliente;
var selectBoxCliente;
var selectBoxEstadoAgenda;
var popup = null;
var optionsScheduler;
var eventoNuevoFecha;
var cancelarEventoAgendaId;
var fechaEventoCancelado;
var currentViewScheduler = "workWeek";
var formaterHour = Globalize( "en" ).dateFormatter( { skeleton: "Hm" } );
var dateDefault = new Date();

$.when(promiseCultureLoad).done(function () {

    loadScheduler(dateDefault);
    //AGREGAR NUEVO EVENTO
    selectBoxCliente = $("#selectBoxCliente").dxSelectBox({
        displayExpr: 'cliente_codigo',
        displayValue: 'cliente_id', 
        searchEnabled: true,
        showClearButton: true,
    }).dxSelectBox('instance');

    $('#modalAgregarEvento').on('hidden.bs.modal', function(){
        selectBoxCliente.option('value', null);
    });

    $("#buttonGuardarEvento").dxButton({
        text: 'Guardar',
        onClick: function() {
            if(selectBoxCliente.option("value")){
                var clienteId = selectBoxCliente.option('value').cliente_id;
                addDataScheduler(clienteId, eventoNuevoFecha);
            } else {
                swal('Error de ingreso','Debe seleccionar un cliente','error');
            }
        }
    });

    //CANCELAR EVENTO
    selectBoxEstadoAgenda = $("#selectBoxEstadoAgenda").dxSelectBox({
        displayExpr: 'descriptiva',
        displayValue: 'id'
    }).dxSelectBox('instance');

    $("#buttonCancelarEvento").dxButton({
        text: 'Guardar',
        onClick: function() {
            if(selectBoxEstadoAgenda.option('value')){
                var estadoAgendaId = selectBoxEstadoAgenda.option('value').id;
                updateStateScheduler(cancelarEventoAgendaId, estadoAgendaId, fechaEventoCancelado);
            }
        }
    });

    //CLONACIÓN AGENDA
    $("#buttonCopiarDia").click(function() {
        $('#modalClonarDia').modal('show');
        $("#dateFechaOrigen").dxDateBox({
            value: null
        });
        $("#dateFechaDestino").dxDateBox({
            value: null
        });
    });

    $("#buttonClonarAgenda").dxButton({
        text: 'Guardar',
        onClick: function() {
            var fechaOrigen = $("#dateFechaOrigen").dxDateBox('instance').option('value');
            var fechaDestino = $("#dateFechaDestino").dxDateBox('instance').option('value');
            if(fechaOrigen && fechaDestino){
                if(!fechaOrigen.sameDay(fechaDestino)){
                    $.when(verificarCantidadEvento(fechaOrigen))
                        .then(function(response){
                            //String para label fecha en swal 2
                            var month = fechaDestino.getUTCMonth() + 1; //months from 1-12
                            var day = fechaDestino.getUTCDate();
                            var year = fechaDestino.getUTCFullYear();
                            var textFecha = day + "/" + month + "/" + year;
                            //Mensaje swal para confirmar copia de agenda
                            swal({
                                title: 'Copiar Agenda',
                                text: 'Deseas copiar '+ response[0][0].cantidad +' evento(s) al día '+ textFecha +'?',
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Si, copiar!',
                            }).then(function(e){
                                if(e) {
                                    $(".page-loader").fadeIn();
                                    currentViewScheduler = $("#scheduler").dxScheduler('instance').option("currentView");
                                    $.when(insertarClonacionAgenda(fechaOrigen, fechaDestino), loadScheduler(fechaDestino))
                                    .then(function(){
                                        $('#modalClonarDia').modal('hide');
                                        $(".page-loader").fadeOut();
                                        swal('Copiados!','Se actualizó la agenda en el día '+textFecha+'.','success');
                                    })
                                }
                            });
                        });
                }else{
                    swal('Error de ingreso','Las fechas deben ser distintas','error');
                }
            } else {
                swal('Error de ingreso','Debe seleccionar ambos dias','error');
            }
        }
    });

    $('#modalClonarDia').on('hidden.bs.modal', function(){
        $("#dateFechaOrigen").dxDateBox('instance').option('value', null);
        $("#dateFechaDestino").dxDateBox('instance').option('value', null);
    });

});

function verificarCantidadEvento(fechaOrigen){
    var $deferred = $.ajax({
        url: "../rest/agenda/getCantidadAgenda",
        dataType: "json",
        data: {fecha: fechaOrigen},
        async: false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    });
    return $deferred.promise();
}

function insertarClonacionAgenda(fechaOrigen, fechaDestino){
    var $deferred = $.ajax({
        url: "../rest/agenda/insertarClonacionAgenda",
        dataType: "json",
        data: {fechaOrigen: fechaOrigen, fechaDestino: fechaDestino},
        async: false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    });
    return $deferred.promise();
}

//CONTROL AGENDA

function loadScheduler(dateDefault){
    resultData = $.Deferred();
    getDataCliente();
    getDataEstadoAgenda();
    loadDataScheduler();
    $.when(resultData).done(function () {
        optionsScheduler = {
            views: ["workWeek", "day"],
            currentView: currentViewScheduler,
            currentDate: dateDefault,
            firstDayOfWeek: 1,
            startDayHour: 8,
            endDayHour: 19,
            showAllDayPanel: false,
            width: "100%",
            height: "500px",
            dataSource: dataRecord,
            resources: [
                {
                    field: 'cliente_id',
                    dataSource: dataClient,
                    label: "Cliente"
                }
            ],
            editing: { 
                allowAdding: false
                , allowResizing: false
                , allowDragging: true
            },
            appointmentTemplate: appointmentTemplate,
            onAppointmentDblClick: onAppointmentDblClick,
            onAppointmentUpdating: onAppointmentUpdating,
            appointmentTooltipTemplate: appointmentTooltipTemplate,
            onContentReady: onContentReady,
            onCellClick: onCellClick,
            onAppointmentAdded: function(e){
                addDataScheduler(e.appointmentData.cliente.cliente_id, e.appointmentData.startDate)
            },
            onAppointmentDeleted: function(e) {
                deleteDataScheduler(e.appointmentData.id)
            },
            onAppointmentUpdated: function (e) {
                updateDataScheduler(e.appointmentData.id, e.appointmentData.startDate, e.appointmentData.endDate);
            },
        };
        var scheduler = $("#scheduler").dxScheduler(optionsScheduler).dxScheduler('instance');
        detenerLoadingPanel();
    });
}

//CONFIGURACION SCHEDULER

function onCellClick(e){
    if (!timeout) {
        timeout = setTimeout(function() {
           timeout = null;             
        }, 300);
    }
    else {
        $('#modalAgregarEvento').modal('show');
        eventoNuevoFecha = e.cellData.startDate;
        return;
    }            
    e.cancel = true;
}

function onAppointmentDblClick(e){
    e.cancel = true;
}

function appointmentTemplate(itemData){
    var $details = $("<div>").addClass("dx-scheduler-appointment-content-details");
    var startDate = formaterHour(itemData.startDate);
    var endDate = formaterHour(itemData.endDate);
    if(itemData.estado_codigo !== 'vigente')
    {
        $("<div>").append(
            '<i class=\'zmdi zmdi-minus-circle\' style=\'color: #ff6b68;\'></i> ' + itemData.text + '</div><div style="float:right">CANCELADA</div>'
            ).appendTo($details);
        $("<div style='clear: both;'></div>").appendTo($details);
    }
    else 
    {
        if(itemData.visitado == 1){
            $("<div>").append(
                '<i class=\'zmdi zmdi-check-circle\' style=\'color: #3F51B5;\'></i> ' + itemData.text).appendTo($details);
            $("<div style='clear: both;'></div>").appendTo($details);
        }else{
            $("<div>").text(itemData.text).appendTo($details); 
        }
    }
    if (itemData.categoria_compañia_descriptiva) {
        // $("<div>").text(startDate + ' - ' + endDate + " | Visitas (" + itemData.contacto.toString() + "/" + itemData.frecuencia.toString() + ")").addClass("dx-scheduler-appointment-content-date").appendTo($details);
        $("<div>").text(itemData.especialidad+"VISITAS (" + itemData.contacto.toString() + "/" + itemData.frecuencia.toString() + ")").addClass("dx-scheduler-appointment-content-date").appendTo($details);
        $("<br>").appendTo($details);
        $("<div>").text("AGENDADAS (" + itemData.cantidad_agenda.toString() + "/" + itemData.frecuencia.toString() + ")").addClass("dx-scheduler-appointment-content-date").appendTo($details);
        $("<br>").appendTo($details);
        // $("<br><div>").text("Categoría " + itemData.categoria_compañia_descriptiva).appendTo($details);
        $("<div>").text(itemData.calle).addClass("dx-scheduler-appointment-content-date").appendTo($details);
    }
    
    return $details;
}

function onAppointmentUpdating(e){
    if(e.newData.startDate.getTime() === e.oldData.startDate.getTime() && e.newData.endDate.getTime() === e.oldData.endDate.getTime())
    {
        e.newData.startDate = combineDateAndTime(e.newData.startOnlyDate, e.newData.startOnlyTime);
        e.newData.endDate = combineDateAndTime(e.newData.endOnlyDate, e.newData.endOnlyTime);
    }
}

function appointmentTooltipTemplate(data, container){
    var markup = tooltipAgenda(data);
    if(data.estado_codigo == 'vigente'){
        markup.find(".visitShow").dxButton({
            text: "Visitar",
            width: 120,
            height: 30,
            onClick: function() {
                if (data.frecuencia === data.contacto) {
                    if (data.frecuencia === 0) {
                        DevExpress.ui.dialog.alert('No puedes visitar a un médico que no está en tu kardex del ciclo actual.');
                    } else {
                        DevExpress.ui.dialog.alert('No tienes visitas pendientes para este cliente en el ciclo actual.');
                    }
                } else {
                    var page = "";
                    if (data.tipo_cliente_codigo === "institucion")
                        page = "formularioVisitaInstitucion";
                    else if (data.tipo_cliente_codigo === "profesional") {
                        page = "formularioVisitaProfesional";
                    }
                    else if (data.tipo_cliente_codigo === "hospitalario") {
                        page = "formularioVisitaHospitalario";
                    }
                    location.href = "/" + page + "?id=" + Math.abs(data.cliente_id).toString() + "&eventId=" + data.id.toString();
                }
            }
        });
    }
    
    markup.find(".visitDelete").dxButton({
        onClick: function() {
            cancelarEventoAgendaId = data.id;
            fechaEventoCancelado = data.startDate;
            $("#scheduler").dxScheduler('instance').hideAppointmentTooltip();
            $('#modalCancelarEvento').modal('show');
        },
        text: "Cancelar",
        width: 120,
        height: 30
    });
    return markup;
}

function onContentReady(e){
    var popupConfig = e.component._popupConfig;
    e.component._popupConfig = function() {
        var popupOptions = popupConfig.apply(e.component, arguments)

        return $.extend(popupOptions, {
            width: 350,
            height: 250,
            fullScreen: false
        });
    }
}

//VISTAS

function tooltipAgenda(data) {
    var startDate = formaterHour(data.startDate);
    var endDate = formaterHour(data.endDate);

    var tooltipTitle = '';
    var tooltipDate = '';
    var restoreOrDeleteButton = '';

    //TOOLTIP MODAL VIGENTE
    if(data.estado_codigo === 'vigente') {
        tooltipTitle = "<div>"+data.text+"</div>"+
        "<div>CATEGORIA "+data.categoria_compañia_descriptiva+"</div>"+
        "<div>BRICK "+data.brick+"</div>"+
        "<div>"+data.calle+"</div>";
        tooltipDate = startDate + ' - ' + endDate;
        restoreOrDeleteButton = "<div class='visit-tooltip-buttons'>" +
        "<div class='visitDelete'></div>" +
        "</div>";
    }
    else {
        //TOOLTIP MODAL CANCELADO
        tooltipTitle = "<div style='float:left'>" + data.text + "</div>" +
        "<div style='float:right'>CANCELADA</div>" +
        "<div style='clear: both;'></div>";
        tooltipDate = "<div style='float:left'>" + startDate + ' - ' + endDate + "</div>" +
        "<br>"+
        "<div style='float:left'>Motivo: " + getStatusById(data.estado_id).descriptiva + "</div>" +
        "<div style='clear: both;'></div>";
        restoreOrDeleteButton = "<div class='visit-tooltip-buttons'>" +
        "<div class='visitRestore'></div>" +
        "</div>";
    }

    return $("<div class='visit-tooltip'>" +
                "<div class='visit-tooltip-title'>" + tooltipTitle + "</div>" +
                "<div class='visit-tooltip-date'>" + tooltipDate + "</div>" +
                "<div class='visit-tooltip-group-buttons' style='padding-top:5px'>" +
                    restoreOrDeleteButton +
                    "<div class='visit-tooltip-buttons'>" +
                        "<div class='visitShow'></div>" +
                    "</div>" +
                    "<div class='visit-tooltip-buttons'>" +
                        "<div class='visitEditDate'></div>" +
                    "</div>" +
                "</div>" +
            "</div>");
}

//FUNCIONES DATA

function getDataCliente(){
    $.ajax({
        url: "../rest/agenda/getDataCliente",
        dataType: "json",
        async: false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).then(function (data) {
        selectBoxCliente.option('dataSource', data[0]);
    });
}

function getDataEstadoAgenda(){
    $.ajax({
        url: "../rest/agenda/getDataEstadoAgenda",
        dataType: "json",
        async: false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).then(function (data) {
        dataEstadoAgenda = data[0];
        selectBoxEstadoAgenda.option('dataSource', dataEstadoAgenda);
    });
}

function updateDataScheduler(id, startDate, endDate) {
    $.ajax({
        url: "../rest/agenda/updateDataScheduler",
        data: { id: id, startDate: startDate, endDate: endDate },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function () {
        var scheduler = $("#scheduler").dxScheduler('instance');
        scheduler._hideTooltip();
    });
}

function addDataScheduler(clienteId, fecha) {
    $.ajax({
        url: "../rest/agenda/insertDataScheduler",
        data: { clienteId: clienteId, fecha: fecha },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function () {
        var dateDefault = fecha;
        currentViewScheduler = $("#scheduler").dxScheduler('instance').option("currentView");
        loadScheduler(dateDefault);
        $('#modalAgregarEvento').modal('hide');
        getDataCliente();
        selectBoxCliente.option('value', null);
        eventoNuevoFecha = null;
    });
}

function updateStateScheduler(id, estado_id, fecha) {
    $.ajax({
        url: "../rest/agenda/updateStateScheduler",
        data: { id: id, estado_id: estado_id },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function () {
        var dateDefault = fecha;
        currentViewScheduler = $("#scheduler").dxScheduler('instance').option("currentView");
        loadScheduler(dateDefault);
        $('#modalCancelarEvento').modal('hide');
        selectBoxEstadoAgenda.option('value', null);
        fechaEventoCancelado = null;
    });
}

function deleteDataScheduler(id) {
    $.ajax({
        url: "../rest/agenda/deleteDataScheduler",
        data: { id: id },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function () {
        DevExpress.ui.notify("El evento ha sido eliminado.", "success", 1000);
    });
}

function loadDataScheduler() {
    $.ajax({
        url: "../rest/agenda/getDataScheduler",
        dataType: "json",
        async: false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).then(function (data) {
        dataRecord = $.map(data[1], function (item) {
            item['startDate'] = new Date(item['startDate']);
            item['endDate'] = new Date(item['endDate']);
            return item;
        });
        dataClient = data[0];
        resultData.resolve();
    });
}

//FUNCIONES COMUNES

function getHour(date, hour) {
    var d = new Date(date);
    d.setHours(hour);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d;
}

function getStatusById(id) {
    for(var i = 0; i < dataEstadoAgenda.length; i++) {
        if (dataEstadoAgenda[i].id === id)
            return dataEstadoAgenda[i];
    }
    return null;
}

function combineDateAndTime(date, time) {
    timeString = time.getHours() + ':' + time.getMinutes() + ':00';

    var year = date.getFullYear();
    var month = date.getMonth() + 1; // Jan is 0, dec is 11
    var day = date.getDate();
    var dateString = '' + year + '-' + month + '-' + day;
    var combined = new Date(dateString + ' ' + timeString);

    return combined;
};

