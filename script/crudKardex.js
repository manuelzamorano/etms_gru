
var dataDataGrid;

$.when(promiseCultureLoad).done(function () {
    
    obtenerData();
    crearDataGrid();
    detenerLoadingPanel();

    $("#buttonNuevoKardex").click(function() {
        location.href = "../crudKardexNuevo";
    });

});


function crearDataGrid(){

    $("#dataGridKardex").dxDataGrid({
        dataSource: dataDataGrid,
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "Kardex",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            {
               caption: "Editar", dataField: "", width: 70, alignment: 'center', fixed: true
               , cellTemplate: function (container, options) {
                   if (options.data) {
                       var dataId = options.data.representante_cliente_id;
                       var html
                       html =
                           "<button class='btn btn-primary btn--icon' onclick='onClickEditar("
                           + dataId
                           + ")' > <i class='zmdi zmdi-edit'></i></button>";
                       $(html)
                           .appendTo(container);
                   }
               }
            },
            {
                caption: "Eliminar", dataField: "", width: 70, alignment: 'center', fixed: true
                , cellTemplate: function (container, options) {
                    if (options.data) {
                        var dataId = options.data.representante_cliente_id;
                        var html
                        html =
                            "<button class='btn btn-danger btn--icon m-l-5' onclick='onClickEliminar("
                            + dataId
                            + ")' > <i class='zmdi zmdi-delete'></i></button>";
                        $(html)
                            .appendTo(container);
                    }
                }
            },
            { dataField: "cliente_id", dataType: "number", visible: false },
            { dataField: "estado", dataType: "number", visible: false },
            { caption: "Supervisor", dataField: "supervisor", width: 160 },
            { caption: "Representante", dataField: "representante", width: 160 },
            { caption: "Frecuencia", dataField: "realizadas", width: 90, alignment: 'center' },
            { caption: "Agendadas", dataField: "agendadas", width: 100, alignment: 'center' },
            { caption: "Código", dataField: "codigo", width: 85 },
            { caption: "Brick", dataField: "brick", width: 150 },
            { caption: "Comuna", dataField: "comuna", width: 150 },
            { caption: "Dirección", dataField: "direccion", width: 220 },
            { caption: "Cat Compañia", dataField: "categoria_compania", width: 120 },
            { caption: "Cat Adopción", dataField: "categoria_adopcion", width: 120 },
            { caption: "Cat Potencial", dataField: "categoria_potencial", width: 120 },
            { caption: "Cat Estratégica", dataField: "categoria_estrategica", width: 120 },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');
}

function onClickEditar(e){
    var sendData = "?id=" + e;
    location.href = "../crudKardexEditar" + sendData;
}

function onClickEliminar(e){
    swal({
        title: 'Desea eliminar el registro'+e+' ?',
        text: "Se eliminará de la base de datos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, eliminar!'
    }).then(function () {
        // swal.enableLoading();
        swal('No Eliminado');
    });
}

function obtenerData() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudKardex/obtenerDataKardex",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataDataGrid = data[0];
    });
}