
var dataDataGrid;

$.when(promiseCultureLoad).done(function () {
    
    obtenerData();
    crearDataGrid();
    detenerLoadingPanel();

    $("#buttonNuevoProfesional").click(function() {
        location.href = "../crudProfesionalNuevo";
    });

});


function crearDataGrid(){

    $("#dataGridProfesional").dxDataGrid({
        dataSource: dataDataGrid,
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "Profesionales",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            {
               caption: "Editar", dataField: "", width: 70, alignment: 'center', fixed: true
               , cellTemplate: function (container, options) {
                   if (options.data) {
                       var dataId = options.data.id;
                       var html
                       html =
                           "<button class='btn btn-primary btn--icon' onclick='onClickEditar("
                           + dataId
                           + ")' > <i class='zmdi zmdi-edit'></i></button>";
                       $(html)
                           .appendTo(container);
                   }
               }
            },
            {
                caption: "Eliminar", dataField: "", width: 70, alignment: 'center', fixed: true
                , cellTemplate: function (container, options) {
                    if (options.data) {
                        var dataId = options.data.id;
                        var html
                        html =
                            "<button class='btn btn-danger btn--icon m-l-5' onclick='onClickEliminar("
                            + dataId
                            + ")' > <i class='zmdi zmdi-delete'></i></button>";
                        $(html)
                            .appendTo(container);
                    }
                }
            },
            { caption:'ID', dataField: 'id', width: 60, fixed: true },
			{ caption:'Código', dataField: 'codigo', width: 100 },
			{ caption:'Nombre', dataField: 'nombre', width: 400 },
			{ caption:'Especialidad', dataField: 'especialidad', width: 200 },
			{ caption:'Especialidad 2', dataField: 'especialidad_2', width: 200 },
			{ caption:'Dirección', dataField: 'direccion', width: 400 },
			{ caption:'Comuna', dataField: 'comuna', width: 200 },
			{ caption:'Brick', dataField: 'brick', width: 200 },
			{ caption:'Tipo profesional', dataField: 'tipo_profesional', width: 200 },
			{ caption:'Categoría estratégica', dataField: 'categoria_estrategica', width: 200 },
			{ caption:'Código 2', dataField: 'codigo2', width: 100 },
			{ caption:'Apellido Paterno', dataField: 'apellido_paterno', width: 100 },
			{ caption:'Apellido Materno', dataField: 'apellido_materno', width: 100 },
			{ caption:'Género', dataField: 'genero', width: 100 },
			{ caption:'Correo', dataField: 'correo', width: 100 },
			{ caption:'Correo 2', dataField: 'correo2', width: 100 },
			{ caption:'Teléfono', dataField: 'telefono', width: 100 },
			{ caption:'Teléfono 2', dataField: 'telefono2', width: 100 },
			{ caption:'Nacionalidad', dataField: 'nacionalidad', width: 100 },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');
}

function onClickEditar(e){
    var sendData = "?id=" + e;
    location.href = "../crudProfesionalEditar" + sendData;
}

function onClickEliminar(e){
    swal({
        title: 'Desea eliminar el registro'+e+' ?',
        text: "Se eliminará de la base de datos",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, eliminar!'
    }).then(function () {
        // swal.enableLoading();
        swal('No Eliminado');
    });
}

function obtenerData() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudProfesional/obtenerDataProfesional",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataDataGrid = data[0];
    });
}