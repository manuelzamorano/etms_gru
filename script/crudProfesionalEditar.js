
var formProfesional
, dataTipoProfesional
, dataCadena
, dataBrick
, dataEspecialidad
, dataGenero
, dataNacionalidad
, dataHorario
, dataProfesional
, dataCategoriaAdopcion
, dataCategoriaCompania
, dataCategoriaPotencial;

$.when(promiseCultureLoad).done(function () {
    
    cargarBrick();
    cargarCadena();
    cargarEspecialidad();
    cargarGenero();
    cargarNacionalidad();
    cargarHorario();
    cargarCategoriaAdopcion();
    cargarCategoriaCompania();
    cargarCategoriaPotencial();
    cargarTipoProfesional();
    obtenerDataProfesional();
    mostrarFormulario();
    mostrarBotonActualizar();
    detenerLoadingPanel();

});

function mostrarFormulario(){
    formProfesional = $("#formProfesional").dxForm({
        colCount: 2,
        colSpan: 2,
        formData: dataProfesional,
        items: [
            {
                itemType: "group",
                caption: "Datos Profesional",
                colCount: 2,
                colSpan: 2,
                items: [
                    {
                        dataField: "codigo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Rut'
                        },
                        editorOptions: {
                            placeholder: 'Sin puntos, ni guión',
                            disabled: true,
                            maxLength: 15
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar Rut"
                        }]
                    },
                    {
                        itemType: "empty",
                    },
                    {
                        dataField: "nombre",
                        colSpan: 2,
                        editorType: "dxTextBox",
                        label: {
                            text: 'Nombre'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nombre"
                        }]
                    },
                    {
                        dataField: "tipo_profesional_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Tipo'
                        },
                        editorOptions: {
                            dataSource: dataTipoProfesional
                            , displayExpr: 'tipo_profesional_descriptiva'
                            , valueExpr: 'tipo_profesional_id'
                            , showClearButton: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar tipo"
                        }]
                    },
                    {
                        dataField: "genero_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Género'
                        },
                        editorOptions: {
                            dataSource: dataGenero
                            , displayExpr: 'genero_descriptiva'
                            , valueExpr: 'genero_id'
                            , showClearButton: true
                        }
                    }, 
                    {
                        dataField: "correo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Correo Electrónico'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        }
                    }, 
                    {
                        dataField: "telefono",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Teléfono / celular'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 25 caracteres',
                            maxLength: 25
                        }
                    }, 
                    {
                        dataField: "especialidad_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Especialidad'
                        },
                        editorOptions: {
                            dataSource: dataEspecialidad
                            , displayExpr: 'especialidad_descriptiva'
                            , valueExpr: 'especialidad_id'
                            , showClearButton: true
                            , searchEnabled: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar especialidad"
                        }]
                    },
                    {
                        dataField: "especialidad_2",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Especialidad 2'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 200 caracteres',
                            maxLength: 200
                        }
                    },
                    {
                        dataField: "nacionalidad_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Nacionalidad'
                        },
                        editorOptions: {
                            dataSource: dataNacionalidad
                            , displayExpr: 'nacionalidad_descriptiva'
                            , valueExpr: 'nacionalidad_id'
                            , showClearButton: true
                            , searchEnabled: true
                        }
                    },
                    {
                        dataField: "calle",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Dirección'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 400
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar dirección"
                        }]
                    },
                    {
                        dataField: "comuna",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Comuna'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar comuna"
                        }]
                    },
                    {
                        dataField: "brick_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Brick'
                        },
                        editorOptions: {
                            dataSource: dataBrick
                            , displayExpr: 'brick_descriptiva'
                            , valueExpr: 'brick_id'
                            , showClearButton: true
                            , searchEnabled: true
                        }
                    },
                ],
            },
            {
                itemType: "group",
                caption: "Horario de atención",
                colSpan: 1,
                items: [
                    {
                        dataField: "lunes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Lunes'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "martes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Martes'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "miercoles_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Miércoles'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "jueves_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Jueves'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "viernes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Viernes'
                        },
                        editorOptions: {
                            dataSource: dataHorario
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                ]
            },
            {
                itemType: "group",
                caption: "Categoria",
                colSpan: 1,
                items: [
                    {
                        dataField: "categoria_compania_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Compañia'
                        },
                        editorOptions: {
                            dataSource: dataCategoriaCompania
                            , displayExpr: 'categoria_compania_descriptiva'
                            , valueExpr: 'categoria_compania_id'
                            , showClearButton: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar una categoria"
                        }]
                    },
                    {
                        dataField: "categoria_potencial_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Potencial'
                        },
                        editorOptions: {
                            dataSource: dataCategoriaPotencial
                            , displayExpr: 'categoria_potencial_descriptiva'
                            , valueExpr: 'categoria_potencial_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "categoria_adopcion_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Adopción'
                        },
                        editorOptions: {
                            dataSource: dataCategoriaAdopcion
                            , displayExpr: 'categoria_adopcion_descriptiva'
                            , valueExpr: 'categoria_adopcion_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "categoria_estrategica",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Estratégica'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 50 caracteres',
                            maxLength: 50
                        }
                    },
                ]
            }
        ]
    }).dxForm('instance');
}

function mostrarBotonActualizar(){
    $("#buttonActualizar").dxButton({
        text: "Actualizar",
        type: "success",
        width: 200,
        validationGroup: 'guardarDatos',
        onClick: function(e) {
            if(formProfesional.validate().isValid) {
                swal.queue([{
                    title: 'Actualizar',
                    confirmButtonText: 'Guardar',
                    text: 'Desea guardar los cambios?',
                    type: 'question',
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            actualizarProfesional();
                        })
                    }
                }])
            } else {
                DevExpress.ui.notify('Complete campos obligatorios.', 'error');
            }
        }
    });
}

function actualizarProfesional(){

    var formData = formProfesional.option('formData');

    $.ajax({
        dataType: "json",
        'async': false,
        data: formData,
        url: "../rest/crudProfesional/actualizarDataProfesional",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        swal({
            title: 'Actualización',
            text: 'Registro completo',
            type: 'success',
            confirmButtonText: 'OK'
        }).then(
            function () { 
                location.href = "../crudProfesional";
            }
        );
    });
    
}

function cargarCadena(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataCadenaSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataCadena = data[0];
    });
}

function cargarBrick(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataBrickSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataBrick = data[0];
    });
}

function cargarEspecialidad(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataEspecialidadSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataEspecialidad = data[0];
    });
}

function cargarGenero(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataGeneroSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataGenero = data[0];
    });
}

function cargarNacionalidad(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataNacionalidadSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataNacionalidad = data[0];
    });
}

function cargarHorario(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataHorarioSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataHorario = data[0];
    });
}

function cargarCategoriaAdopcion(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataCategoriaAdopcionSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataCategoriaAdopcion = data[0];
    });
}

function cargarCategoriaCompania(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataCategoriaCompaniaSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataCategoriaCompania = data[0];
    });
}

function cargarCategoriaPotencial(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataCategoriaPotencialSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataCategoriaPotencial = data[0];
    });
}

function obtenerDataProfesional(){
    var profesionalId = getParameterByName('id');
    if(profesionalId){
        $.ajax({
            dataType: "json",
            'async': false,
            data:{
                profesional_id : profesionalId
            },
            url: "../rest/crudProfesional/obtenerDataProfesionalPorID",
            error: function(xhr, status) {
                if (typeof this.statusCode[xhr.status] != 'undefined') {
                    return false;
                }
                console.log('ajax.error');
            },
            statusCode: {
                404: function(response) {
                    console.log('status_code: 404');
                },
                500: function(response) {
                    console.log('status_code: 500');
                }
            }
        }).done(function(data) {
            dataProfesional = data[0][0];
        });
    }
}

function cargarTipoProfesional(){
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/crudData/obtenerCrudDataTipoProfesionalSelectBox",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function(data) {
        dataTipoProfesional = data[0];
    });
}