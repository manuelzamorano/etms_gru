
var formActivo, dataEdicion, dataCliente;

$.when(promiseCultureLoad).done(function () {
    obtenerDataEdicion();
    configuracionBotones();
});

// FORMULARIOS PROFESIONAL
function mostrarAltaProfesional(){
    //==========OCULTAR MENU BOTONES==========//
    $("#card_alta_profesional").css({'display':''});
    //========CONFIGURACION BOTONES ALTA======//
    $("#button_guardar_alta_profesional").click(function() {
        obtenerDataFormularioAltaProfesional();
    });
    $("#button_cancelar_alta_profesional").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_alta_profesional").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO ALTA=============//
    formActivo = $("#form_alta_profesional").dxForm({
        colCount: 2,
        //formData: data,
        items: [
            {
                itemType: "group",
                caption: "Datos Profesional",
                colCount: 2,
                colSpan: 2,
                items: [
                    {
                        dataField: "codigo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Rut'
                        },
                        editorOptions: {
                            placeholder: 'Sin puntos, ni guión',
                            maxLength: 15
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar Rut"
                        }]
                    }, 
                    {
                        dataField: "nombre",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Nombre'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nombre"
                        }]
                    },
                    {
                        dataField: "genero_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Género'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[3]
                            , displayExpr: 'genero_descriptiva'
                            , valueExpr: 'genero_id'
                            , showClearButton: true
                        }
                    }, 
                    {
                        dataField: "correo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Correo Electrónico'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        }
                    }, 
                    {
                        dataField: "telefono",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Teléfono / celular'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 25 caracteres',
                            maxLength: 25
                        }
                    }, 
                    {
                        dataField: "especialidad_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Especialidad'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[1]
                            , displayExpr: 'especialidad_descriptiva'
                            , valueExpr: 'especialidad_id'
                            , showClearButton: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar especialidad"
                        }]
                    },
                    {
                        dataField: "especialidad_2",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Especialidad 2'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 200 caracteres',
                            maxLength: 200
                        }
                    },
                    {
                        dataField: "nacionalidad_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Nacionalidad'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[2]
                            , displayExpr: 'nacionalidad_descriptiva'
                            , valueExpr: 'nacionalidad_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "calle",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Dirección'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar dirección"
                        }]
                    },
                    {
                        dataField: "comuna",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Comuna'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar comuna"
                        }]
                    },
                ],
            },
            {
                itemType: "group",
                caption: "Horario de atención",
                colSpan: 1,
                items: [
                    {
                        dataField: "lunes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Lunes'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "martes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Martes'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "miercoles_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Miércoles'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "jueves_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Jueves'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "viernes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Viernes'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                ]
            }
        ]
    }).dxForm('instance');

}

function mostrarBajaProfesional(){
    //==========DATA CLIENTE==========//
    obtenerDataClienteProfesional();
    //==========OCULTAR MENU BOTONES==========//
    $("#card_baja_profesional").css({'display':''});
    //========CONFIGURACION BOTONES BAJA======//
    $("#button_guardar_baja_profesional").click(function() {
        obtenerDataFormularioBajaProfesional();
    });
    $("#button_cancelar_baja_profesional").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_baja_profesional").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO BAJA=============//
    formActivo = $("#form_baja_profesional").dxForm({
        colCount: 2,
        //formData: data,
        items:
            [
                {
                    itemType: "group",
                    caption: "Datos para baja",
                    items: [
                        {
                            dataField: "cliente_id",
                            editorType: "dxSelectBox",
                            label: {
                                text: 'Profesional'
                            },
                            editorOptions: {
                                dataSource: dataCliente[0]
                                , displayExpr: 'cliente_descriptiva'
                                , valueExpr: 'cliente_id'
                                , searchEnabled: true
                                , showClearButton: true
                            },
                            validationRules: [{
                                type: "required",
                                message: "Debe seleccionar un profesional"
                            }]
                        }, 
                        {
                            dataField: "motivo_baja_id",
                            editorType: "dxSelectBox",
                            label: {
                                text: 'Motivo baja'
                            },
                            editorOptions: {
                                dataSource: dataEdicion[5]
                                , displayExpr: 'motivo_baja_descriptiva'
                                , valueExpr: 'motivo_baja_id'
                                , showClearButton: true
                            },
                            validationRules: [{
                                type: "required",
                                message: "Debe seleccionar un motivo de baja"
                            }]
                        }, 
                        {
                            dataField: "comentario",
                            editorType: "dxTextArea",
                            label: {
                                text: 'Comentario'
                            },
                            editorOptions: {
                                placeholder: 'Máximo 300 caracteres',
                                maxLength: 300,
                                height: 150
                            },
                            validationRules: [{
                                type: "required",
                                message: "Ingrese comentario"
                            }]
                        }
                    ]
                }
            ]
    }).dxForm('instance');
}

function mostrarEdicionProfesional(){
    //==========DATA CLIENTE==========//
    obtenerDataClienteProfesional();
    //==========OCULTAR MENU BOTONES==========//
    $("#card_edicion_profesional").css({'display':''});
    //========CONFIGURACION BOTONES ALTA======//
    $("#button_guardar_edicion_profesional").click(function() {
        obtenerDataFormularioEdicionProfesional();
    });
    $("#button_cancelar_edicion_profesional").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_edicion_profesional").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO EDICION=============//
    formActivo = $("#form_edicion_profesional").dxForm({
        colCount: 2,
        //formData: data,
        items: [
            {
                itemType: "group",
                colSpan: 1,
                items: [
                    {
                        dataField: "cliente_id",
                        name: "cliente_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Seleccione profesional'
                        },
                        editorOptions: {
                            dataSource: dataCliente[0]
                            , displayExpr: 'cliente_descriptiva'
                            , valueExpr: 'cliente_id'
                            , searchEnabled: true
                            , showClearButton: true
                            , onValueChanged: cargarDataEdicion
                        }
                    },
                ],
            },
            {
                itemType: "empty",
            },
            {
                itemType: "group",
                colCount: 2,
                colSpan: 2,
                caption: "Datos Profesional",
                items: [
                    {
                        dataField: "codigo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Rut'
                        },
                        editorOptions: {
                            placeholder: 'Sin puntos, ni guión',
                            disabled: true,
                            maxLength: 15
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar Rut"
                        }]
                    }, 
                    {
                        dataField: "nombre",
                        name: "nombre",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Nombre'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nombre"
                        }]
                    },
                    {
                        dataField: "genero_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Género'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[3]
                            , displayExpr: 'genero_descriptiva'
                            , valueExpr: 'genero_id'
                            , showClearButton: true
                        }
                    }, 
                    {
                        dataField: "correo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Correo Electrónico'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        }
                    }, 
                    {
                        dataField: "telefono",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Teléfono / celular'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 25 caracteres',
                            maxLength: 25
                        }
                    }, 
                    {
                        dataField: "especialidad_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Especialidad'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[1]
                            , displayExpr: 'especialidad_descriptiva'
                            , valueExpr: 'especialidad_id'
                            , showClearButton: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar especialidad"
                        }]
                    },
                    {
                        dataField: "especialidad_2",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Especialidad 2'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 200 caracteres',
                            maxLength: 200
                        }
                    },
                    {
                        dataField: "nacionalidad_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Nacionalidad'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[2]
                            , displayExpr: 'nacionalidad_descriptiva'
                            , valueExpr: 'nacionalidad_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "direccion",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Dirección'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar dirección"
                        }]
                    },
                    {
                        dataField: "comuna",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Comuna'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar comuna"
                        }]
                    },
                ],
            },
            {
                itemType: "group",
                caption: "Horario de atención",
                items: [
                    {
                        dataField: "lunes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Lunes'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "martes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Martes'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "miercoles_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Miércoles'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "jueves_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Jueves'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                    {
                        dataField: "viernes_horario_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Viernes'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[4]
                            , displayExpr: 'horario_descriptiva'
                            , valueExpr: 'horario_id'
                            , showClearButton: true
                        }
                    },
                ]
            }
        ]
    }).dxForm('instance');

}

// FORMULARIOS INSTITUCION
function mostrarAltaInstitucion(){
    //==========OCULTAR MENU BOTONES==========//
    $("#card_alta_institucion").css({'display':''});
    //========CONFIGURACION BOTONES ALTA======//
    $("#button_guardar_alta_institucion").click(function() {
        obtenerDataFormularioAltaInstitucion();
    });
    $("#button_cancelar_alta_institucion").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_alta_institucion").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO ALTA=============//
    formActivo = $("#form_alta_institucion").dxForm({
        colCount: 2,
        //formData: data,
        items: [
            {
                itemType: "group",
                caption: "Datos Punto de Venta",
                items: [
                    {
                        dataField: "local",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Local'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 20 caracteres',
                            maxLength: 20
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nro de local"
                        }]
                    }, 
                    {
                        dataField: "telefono",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Teléfono / celular'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 25 caracteres',
                            maxLength: 25
                        }
                    }, 
                    {
                        dataField: "cadena_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Cadena'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[0]
                            , displayExpr: 'cadena_descriptiva'
                            , valueExpr: 'cadena_id'
                            , showClearButton: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe seleccionar una cadena"
                        }]
                    },
                    {
                        dataField: "calle",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Dirección'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar dirección"
                        }]
                    },
                    {
                        dataField: "comuna",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Comuna'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar comuna"
                        }]
                    },
                ],
            },
        ]
    }).dxForm('instance');
}

function mostrarBajaInstitucion(){
    //==========DATA CLIENTE==========//
    obtenerDataClienteInstitucion();
    //==========OCULTAR MENU BOTONES==========//
    $("#card_baja_institucion").css({'display':''});
    //========CONFIGURACION BOTONES BAJA======//
    $("#button_guardar_baja_institucion").click(function() {
        obtenerDataFormularioBajaInstitucion();
    });
    $("#button_cancelar_baja_institucion").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_baja_institucion").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO BAJA=============//
    formActivo = $("#form_baja_institucion").dxForm({
        colCount: 2,
        //formData: data,
        items:
            [
                {
                    itemType: "group",
                    caption: "Datos para baja",
                    items: [
                        {
                            dataField: "cliente_id",
                            editorType: "dxSelectBox",
                            label: {
                                text: 'Punto de venta'
                            },
                            editorOptions: {
                                dataSource: dataCliente[0]
                                , displayExpr: 'cliente_descriptiva'
                                , valueExpr: 'cliente_id'
                                , showClearButton: true
                            },
                            validationRules: [{
                                type: "required",
                                message: "Debe seleccionar un profesional"
                            }]
                        }, 
                        {
                            dataField: "motivo_baja_id",
                            editorType: "dxSelectBox",
                            label: {
                                text: 'Motivo baja'
                            },
                            editorOptions: {
                                dataSource: dataEdicion[5]
                                , displayExpr: 'motivo_baja_descriptiva'
                                , valueExpr: 'motivo_baja_id'
                                , showClearButton: true
                            },
                            validationRules: [{
                                type: "required",
                                message: "Debe seleccionar un motivo de baja"
                            }]
                        }, 
                        {
                            dataField: "comentario",
                            editorType: "dxTextArea",
                            label: {
                                text: 'Comentario'
                            },
                            editorOptions: {
                                placeholder: 'Máximo 300 caracteres',
                                maxLength: 300,
                                height: 150
                            },
                            validationRules: [{
                                type: "required",
                                message: "Ingrese comentario"
                            }]
                        }
                    ]
                }
            ]
    }).dxForm('instance');
}

function mostrarEdicionInstitucion(){
    //==========DATA CLIENTE==========//
    obtenerDataClienteInstitucion();
    //==========OCULTAR MENU BOTONES==========//
    $("#card_edicion_institucion").css({'display':''});
    //========CONFIGURACION BOTONES ALTA======//
    $("#button_guardar_edicion_institucion").click(function() {
        obtenerDataFormularioEdicionInstitucion();
    });
    $("#button_cancelar_edicion_institucion").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_edicion_institucion").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO EDICION=============//
    formActivo = $("#form_edicion_institucion").dxForm({
        colCount: 2,
        //formData: data,
        items: [
            {
                itemType: "group",
                colSpan: 1,
                items: [
                    {
                        dataField: "cliente_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Seleccione Punto de venta'
                        },
                        editorOptions: {
                            dataSource: dataCliente[0]
                            , displayExpr: 'cliente_descriptiva'
                            , valueExpr: 'cliente_id'
                            , showClearButton: true
                            , onValueChanged: cargarDataEdicion
                        }
                    },
                ],
            },
            {
                itemType: "empty",
            },
            {
                itemType: "group",
                caption: "Datos Punto de Venta",
                items: [
                    {
                        dataField: "codigo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Código'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 15 caracteres',
                            disabled: true,
                            maxLength: 15
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar codigo de farmacia"
                        }]
                    }, 
                    {
                        dataField: "local",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Local'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 20 caracteres',
                            maxLength: 20
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nro de local"
                        }]
                    }, 
                    {
                        dataField: "telefono",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Teléfono / celular'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 25 caracteres',
                            maxLength: 25
                        }
                    }, 
                    {
                        dataField: "cadena_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Cadena'
                        },
                        editorOptions: {
                            dataSource: dataEdicion[0]
                            , displayExpr: 'cadena_descriptiva'
                            , valueExpr: 'cadena_id'
                            , showClearButton: true
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe seleccionar una cadena"
                        }]
                    },
                    {
                        dataField: "direccion",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Dirección'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar dirección"
                        }]
                    },
                    {
                        dataField: "comuna",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Comuna'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar comuna"
                        }]
                    },
                ],
            }
        ]
    }).dxForm('instance');
}

// FORMULARIOS HOSPITALARIO
function mostrarAltaHospitalario(){
    //==========OCULTAR MENU BOTONES==========//
    $("#card_alta_hospitalario").css({'display':''});
    //========CONFIGURACION BOTONES ALTA======//
    $("#button_guardar_alta_hospitalario").click(function() {
        obtenerDataFormularioAltaHospitalario();
    });
    $("#button_cancelar_alta_hospitalario").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_alta_hospitalario").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO ALTA=============//
    formActivo = $("#form_alta_hospitalario").dxForm({
        colCount: 2,
        //formData: data,
        items: [
            {
                itemType: "group",
                caption: "Datos Hospitalario",
                items: [
                    {
                        dataField: "nombre",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Nombre'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nombre"
                        }]
                    },
                    {
                        dataField: "contacto",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Contacto'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nombre"
                        }]
                    }, 
                    {
                        dataField: "telefono",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Teléfono / celular'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        }
                    },
                    {
                        dataField: "correo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Correo'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        }
                    },
                    {
                        dataField: "calle",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Dirección'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar dirección"
                        }]
                    },
                    {
                        dataField: "comuna",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Comuna'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar comuna"
                        }]
                    },
                ],
            },
        ]
    }).dxForm('instance');
}

function mostrarBajaHospitalario(){
    //==========DATA CLIENTE==========//
    obtenerDataClienteHospitalario();
    //==========OCULTAR MENU BOTONES==========//
    $("#card_baja_hospitalario").css({'display':''});
    //========CONFIGURACION BOTONES BAJA======//
    $("#button_guardar_baja_hospitalario").click(function() {
        obtenerDataFormularioBajaHospitalario();
    });
    $("#button_cancelar_baja_hospitalario").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_baja_hospitalario").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO BAJA=============//
    formActivo = $("#form_baja_hospitalario").dxForm({
        colCount: 2,
        //formData: data,
        items:
            [
                {
                    itemType: "group",
                    caption: "Datos para baja",
                    items: [
                        {
                            dataField: "cliente_id",
                            editorType: "dxSelectBox",
                            label: {
                                text: 'Punto de venta'
                            },
                            editorOptions: {
                                dataSource: dataCliente[0]
                                , displayExpr: 'cliente_descriptiva'
                                , valueExpr: 'cliente_id'
                                , showClearButton: true
                            },
                            validationRules: [{
                                type: "required",
                                message: "Debe seleccionar un profesional"
                            }]
                        }, 
                        {
                            dataField: "motivo_baja_id",
                            editorType: "dxSelectBox",
                            label: {
                                text: 'Motivo baja'
                            },
                            editorOptions: {
                                dataSource: dataEdicion[5]
                                , displayExpr: 'motivo_baja_descriptiva'
                                , valueExpr: 'motivo_baja_id'
                                , showClearButton: true
                            },
                            validationRules: [{
                                type: "required",
                                message: "Debe seleccionar un motivo de baja"
                            }]
                        }, 
                        {
                            dataField: "comentario",
                            editorType: "dxTextArea",
                            label: {
                                text: 'Comentario'
                            },
                            editorOptions: {
                                placeholder: 'Máximo 300 caracteres',
                                maxLength: 300,
                                height: 150
                            },
                            validationRules: [{
                                type: "required",
                                message: "Ingrese comentario"
                            }]
                        }
                    ]
                }
            ]
    }).dxForm('instance');
}

function mostrarEdicionHospitalario(){
    //==========DATA CLIENTE==========//
    obtenerDataClienteHospitalario();
    //==========OCULTAR MENU BOTONES==========//
    $("#card_edicion_hospitalario").css({'display':''});
    //========CONFIGURACION BOTONES ALTA======//
    $("#button_guardar_edicion_hospitalario").click(function() {
        obtenerDataFormularioEdicionHospitalario();
    });
    $("#button_cancelar_edicion_hospitalario").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_edicion_hospitalario").css({'display':'none'});
        formActivo.option('formData', []);
    });
    //============FORMULARIO EDICION=============//
    formActivo = $("#form_edicion_hospitalario").dxForm({
        colCount: 2,
        items: [
            {
                itemType: "group",
                items: [
                    {
                        dataField: "cliente_id",
                        editorType: "dxSelectBox",
                        label: {
                            text: 'Seleccione hospitalario'
                        },
                        editorOptions: {
                            dataSource: dataCliente[0]
                            , displayExpr: 'cliente_descriptiva'
                            , valueExpr: 'cliente_id'
                            , showClearButton: true
                            , onValueChanged: cargarDataEdicion
                        }
                    },
                ],
            },
            {
                itemType: "empty",
            },
            {
                itemType: "group",
                items: [
                    {
                        dataField: "codigo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Código'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 200 caracteres',
                            disabled: true,
                            maxLength: 200
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar codigo de farmacia"
                        }]
                    }, 
                    {
                        dataField: "nombre",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Nombre'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar nombre"
                        }]
                    },
                    {
                        dataField: "contacto",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Contacto'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        }
                    }, 
                    {
                        dataField: "telefono",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Teléfono / celular'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        }
                    },
                    {
                        dataField: "correo",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Correo'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        }
                    },
                    {
                        dataField: "calle",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Dirección'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 300 caracteres',
                            maxLength: 300
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar dirección"
                        }]
                    },
                    {
                        dataField: "comuna",
                        editorType: "dxTextBox",
                        label: {
                            text: 'Comuna'
                        },
                        editorOptions: {
                            placeholder: 'Máximo 100 caracteres',
                            maxLength: 100
                        },
                        validationRules: [{
                            type: "required",
                            message: "Debe ingresar comuna"
                        }]
                    },
                ],
            }
        ],
    }).dxForm('instance');
}

//=======CONFIGURACION========//
function configuracionBotones(){
    //=========BOTONERA INICIAL========//
    $("#button_alta").click(function() {
        $("#card_buttons").css({'display':'none'});
        $("#card_buttons_alta").css({'display':''});
    });

    $("#button_baja").click(function() {
        $("#card_buttons").css({'display':'none'});
        $("#card_buttons_baja").css({'display':''});
    });

    $("#button_edicion").click(function() {
        $("#card_buttons").css({'display':'none'});
        $("#card_buttons_edicion").css({'display':''});
    });

    //===========BOTONERA ALTA==========//
    $("#button_profesional_alta").click(function() {
        $("#card_buttons_alta").css({'display':'none'});
        mostrarAltaProfesional();
    });

    $("#button_institucion_alta").click(function() {
        $("#card_buttons_alta").css({'display':'none'});
        mostrarAltaInstitucion();
    });

    $("#button_hospitalario_alta").click(function() {
        $("#card_buttons_alta").css({'display':'none'});
        mostrarAltaHospitalario();
    });

    //===========BOTONERA BAJA==========//
    $("#button_profesional_baja").click(function() {
        $("#card_buttons_baja").css({'display':'none'});
        mostrarBajaProfesional();
    });

    $("#button_institucion_baja").click(function() {
        $("#card_buttons_baja").css({'display':'none'});
        mostrarBajaInstitucion();
    });

    $("#button_hospitalario_baja").click(function() {
        $("#card_buttons_baja").css({'display':'none'});
        mostrarBajaHospitalario();
    });

    //===========BOTONERA EDICION==========//
    $("#button_profesional_edicion").click(function() {
        $("#card_buttons_edicion").css({'display':'none'});
        mostrarEdicionProfesional();
    });

    $("#button_institucion_edicion").click(function() {
        $("#card_buttons_edicion").css({'display':'none'});
        mostrarEdicionInstitucion();
    });

    $("#button_hospitalario_edicion").click(function() {
        $("#card_buttons_edicion").css({'display':'none'});
        mostrarEdicionHospitalario();
    });

    //===========BOTONERA EDICION==========//
    $(".button_volver").click(function() {
        $("#card_buttons").css({'display':''});
        $("#card_buttons_alta").css({'display':'none'});
        $("#card_buttons_baja").css({'display':'none'});
        $("#card_buttons_edicion").css({'display':'none'});
    });
}

//============DATA GET==============//
function obtenerDataEdicion(){
    $.ajax({
        url: "../rest/edicion/getDataEdicion",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataEdicion = data;
        detenerLoadingPanel();
    });
}

function obtenerDataClienteProfesional(){
    $.ajax({
        url: "../rest/edicion/getDataProfesional",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataCliente = data;
    });
}

function obtenerDataClienteInstitucion(){
    $.ajax({
        url: "../rest/edicion/getDataInstitucion",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataCliente = data;
    });
}

function obtenerDataClienteHospitalario(){
    $.ajax({
        url: "../rest/edicion/getDataHospitalario",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataCliente = data;
    });
}

function cargarDataEdicion(e){
    if(e.value > 0){
        $.ajax({
            url: "../rest/edicion/getDataFormEdicion",
            data: {
                cliente_id : e.value
            },
            dataType: "json",
            'async': false,
            error: function(xhr, status) {
                if (typeof this.statusCode[xhr.status] != 'undefined') {
                    return false;
                }
                console.log('ajax.error');
            },
            statusCode: {
                404: function(response) {
                    console.log('status_code: 404');
                },
                500: function(response) {
                    console.log('status_code: 500');
                }
            }
        }).done(function (data) {
            formActivo.option('formData', data[0][0]);
        });
    } 
    // else {
    //     formActivo.option('formData', []);
    // }
}

//=========DATA FORMULARIOS========//
function obtenerDataFormularioAltaProfesional(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Guardar nuevo cliente',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataAltaProfesional(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

function obtenerDataFormularioAltaInstitucion(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Guardar nuevo cliente',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataAltaInstitucion(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

function obtenerDataFormularioAltaHospitalario(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Guardar nuevo cliente',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataAltaHospitalario(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

function obtenerDataFormularioBajaProfesional(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Dar de alta a este médico',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataBajaProfesional(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

function obtenerDataFormularioBajaInstitucion(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Dar de alta a este punto de venta',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataBajaInstitucion(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

function obtenerDataFormularioBajaHospitalario(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Dar de alta a este hospitalario',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataBajaHospitalario(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

function obtenerDataFormularioEdicionProfesional(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Editar cliente',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataEdicionProfesional(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

function obtenerDataFormularioEdicionInstitucion(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Editar cliente',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataEdicionInstitucion(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

function obtenerDataFormularioEdicionHospitalario(){
    formActivo.validate();
    if (formActivo.validate().isValid) {
        swal({
            title: 'Editar cliente',
            text: "Confirme antes de continuar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar!'
        }).then(function () {
            var data = formActivo.option('formData');
            guardarDataEdicionHospitalario(data);
        });
    } else {
        swal('Faltan campos por completar','','error');
    }
}

//===========ENVIAR DATA===========//
function guardarDataAltaProfesional(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataAltaProfesional",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}

function guardarDataAltaInstitucion(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataAltaInstitucion",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}

function guardarDataAltaHospitalario(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataAltaHospitalario",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}

function guardarDataBajaProfesional(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataBajaProfesional",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}

function guardarDataBajaInstitucion(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataBajaInstitucion",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}

function guardarDataBajaHospitalario(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataBajaHospitalario",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}

function guardarDataEdicionProfesional(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataEdicionProfesional",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}

function guardarDataEdicionInstitucion(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataEdicionInstitucion",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}

function guardarDataEdicionHospitalario(dataForm){
    $.ajax({
        url: "../rest/edicion/insertDataEdicionHospitalario",
        dataType: "json",
        data: { data : dataForm },
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        swal('Registro terminado!','Los cambios de guardaron correctamente','success');
    });
}