var loadPanel, resultData, cicloId = null, dataGridInstitucion, dataGridProfesional, data;

$.when(promiseCultureLoad).done(function () {
    
    dataGridInstitucion = $("#dataGridCoberturaInstitucion").dxDataGrid({
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "VisitasLocales",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total Visitas: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            { allowReordering:true, caption: "Local", dataField: "institucion_local", width: 80 },
            { allowReordering:true, caption: "Código Local", dataField: "institucion_codigo", width: 120 },
            { allowReordering:true, caption: "Cadena", dataField: "cadena_descriptiva", width: 120 },
            { allowReordering:true, caption: "Fecha visita", dataField: "fecha_visita", width: 120 },
            { allowReordering:true, caption: "Tipo visita", dataField: "tipo_visita_descriptiva", width: 120 },
            { 
                allowReordering:true
                , caption: "Información"
                , dataField: "visita_informacion"
                , width: 300
                , cellTemplate: function (container, options) { 
                    $("<div />").dxTextArea({
                        value: options.data.visita_informacion,
                        readOnly: true,
                        height: 70
                    }).appendTo(container);
                },
            },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');

    dataGridProfesional = $("#dataGridCoberturaProfesional").dxDataGrid({
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "VisitasLocales",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total Visitas: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            { allowReordering:true, caption: "Rut", dataField: "profesional_codigo", width: 90 },
            { allowReordering:true, caption: "Nombre", dataField: "profesional_nombre", width: 330 },
            { allowReordering:true, caption: "Especialidad", dataField: "especialidad_descriptiva", width: 120 },
            { allowReordering:true, caption: "Fecha visita", dataField: "fecha_visita", width: 120 },
            { allowReordering:true, caption: "Tipo visita", dataField: "tipo_visita_descriptiva", width: 120 },
            { 
                allowReordering:true
                , caption: "Información"
                , dataField: "visita_informacion"
                , width: 300
                , cellTemplate: function (container, options) { 
                    $("<div />").dxTextArea({
                        value: options.data.visita_informacion,
                        readOnly: true,
                        height: 70
                    }).appendTo(container);
                },
            },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');

    dataGridHospitalario = $("#dataGridCoberturaHospitalario").dxDataGrid({
        rowAlternationEnabled: true,
        showBorders: true,
        "export": {
            enabled: true,
            fileName: "VisitasLocales",
            excelFilterEnabled: true
        },
        columnAutoWidth: true,
        columnFixing: {
            enabled: true
        },
        pager: {
            infoText: "Total Visitas: {2}",
            showInfo: true,
            visible: true,
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10, 20]
        },
        columns: [
            { allowReordering:true, caption: "Código", dataField: "hospitalario_codigo", width: 100 },
            { allowReordering:true, caption: "Nombre", dataField: "hospitalario_nombre", width: 330 },
            { allowReordering:true, caption: "Contacto", dataField: "hospitalario_contact", width: 150 },
            { allowReordering:true, caption: "Fecha visita", dataField: "fecha_visita", width: 120 },
            { allowReordering:true, caption: "Tipo visita", dataField: "tipo_visita_descriptiva", width: 120 },
            { 
                allowReordering:true
                , caption: "Información"
                , dataField: "visita_informacion"
                , width: 300
                , cellTemplate: function (container, options) { 
                    $("<div />").dxTextArea({
                        value: options.data.visita_informacion,
                        readOnly: true,
                        height: 70
                    }).appendTo(container);
                },
            },
        ],
        paging: {
            enabled: true, pageSize: 9
        },
        filterRow: {
            visible: true
        },
        paging: {
            pageSize: 10
        },
        width: '100%'
    }).dxDataGrid('instance');

    loadDataDataGrid();
    detenerLoadingPanel();

});

function loadDataDataGrid() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: '../rest/cobertura/getDataCobertura',
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataGridInstitucion.option('dataSource', data[0]);
        dataGridProfesional.option('dataSource', data[1]);
        dataGridHospitalario.option('dataSource', data[2]);
    });
}