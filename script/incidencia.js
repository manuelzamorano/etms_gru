
var dataCadena, dataLocalDireccion, dataProducto;
var selectBoxCadena, selectBoxLocalDireccion, selectBoxProducto;
var checkBoxQuiebreStock;
var checkBoxStockBajoSugerido; 
var checkBoxNoFuncionaProgramaPaciente;
var checkBoxPlanograma;
var checkBoxAccionCompetencia;
var checkBoxCapacitacionRealizada;
var checkBoxInformacionEspecial;
var checkBoxExhibicion;
var fileUploadImagen;
var textAreaComentario;
var buttonEnviar;

$.when(promiseCultureLoad).done(function () {

    obtenerDataCadena();
    obtenerDataProducto();

    selectBoxCadena = $("#selectBoxCadena").dxSelectBox({
        dataSource: dataCadena
        , displayExpr: 'cadena_descriptiva'
        , valueExpr: 'cadena_id'
        , onValueChanged: function(data) {
            obtenerDataLocalDireccion(data.value);
            selectBoxLocalDireccion.option('dataSource', dataLocalDireccion);
        }
    }).dxValidator({
            validationRules: [{
                type: 'required'
                , message: ''
            }]
            , validationGroup: 'guardarDatos'
    }).dxSelectBox("instance");

    selectBoxLocalDireccion = $("#selectBoxLocalDireccion").dxSelectBox({
        dataSource: dataLocalDireccion
         , displayExpr: 'local_descriptiva'
         , valueExpr: 'institucion_id'
         , searchEnabled: true
    }).dxValidator({
            validationRules: [{
                type: 'required'
                , message: ''
            }]
            , validationGroup: 'guardarDatos'
    }).dxSelectBox("instance");

    selectBoxProducto = $("#selectBoxProducto").dxSelectBox({
        dataSource: dataProducto
         , displayExpr: 'producto_descriptiva'
         , valueExpr: 'producto_id'
         , searchEnabled: true
    // }).dxValidator({
    //         validationRules: [{
    //             type: 'required'
    //             , message: ''
    //         }]
    //         , validationGroup: 'guardarDatos'
    }).dxSelectBox("instance");

    checkBoxQuiebreStock = $("#checkBoxQuiebreStock").dxCheckBox({
        value: false
        , text: "Quiebre Stock"
    }).dxCheckBox("instance");
    
    checkBoxStockBajoSugerido = $("#checkBoxStockBajoSugerido").dxCheckBox({
        value: false
        , text: "Stock bajo sugerido para el local"
    }).dxCheckBox("instance");
    
    checkBoxNoFuncionaProgramaPaciente = $("#checkBoxNoFuncionaProgramaPaciente").dxCheckBox({
        value: false
        , text: "No funciona programa paciente"
    }).dxCheckBox("instance");
    
    checkBoxPlanograma = $("#checkBoxPlanograma").dxCheckBox({
        value: false
        , text: "Planograma"
    }).dxCheckBox("instance");
    
    checkBoxAccionCompetencia = $("#checkBoxAccionCompetencia").dxCheckBox({
        value: false
        , text: "Acción de la competencia"
    }).dxCheckBox("instance");
    
    checkBoxCapacitacionRealizada = $("#checkBoxCapacitacionRealizada").dxCheckBox({
        value: false
        //, text: "Capacitación realizada"
        , text: "Entrenamiento al interior de farmacia"
    }).dxCheckBox("instance");

    checkBoxInformacionEspecial = $("#checkBoxInformacionEspecial").dxCheckBox({
        value: false
        , text: "Solicitud de información especial"
    }).dxCheckBox("instance");

    checkBoxExhibicion = $("#checkBoxInformacionEspecial").dxCheckBox({
        value: false
        , text: "Exhibicion"
    }).dxCheckBox("instance");
    
    fileUploadImagen = $("#fileUploadImagen").dxFileUploader({
        uploadMode: 'useForm',
        accept: 'image/jpg', //.csv
        labelText: '',
        name: 'uploadFile',
        multiple: false,
        selectButtonText: 'Seleccione imagen',
        readyToUploadMessage: 'Listo para cargar',
        onValueChanged: function (e) {
            if (e.value != null) {
                if (e.value[0].size > 4000000) {
                    console.log('Imagen demasiado pesada') ;
                    e.element.find(".dx-fileuploader-file-container").eq(0).find(".dx-fileuploader-cancel-button").trigger("dxclick");
                }
            }
        }
    }).dxFileUploader("instance");

    textAreaComentario = $("#textAreaComentario").dxTextArea({
        height: 100
    }).dxValidator({
            validationRules: [{
                type: 'required'
                , message: ''
            }]
            , validationGroup: 'guardarDatos'
    }).dxTextArea("instance");

    buttonEnviar = $("#buttonEnviar").dxButton({
        text: "Enviar",
        type: "success",
        width: 200,
        validationGroup: 'guardarDatos',
        onClick: function(e) {
            var result = e.validationGroup.validate();
            if(result.isValid) {
                swal.queue([{
                    title: 'Guardar incidencia',
                    confirmButtonText: 'Guardar',
                    text: 'Desea guardar la incidencia?',
                    type: 'question',
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            var institucion_id = selectBoxLocalDireccion.option('value');
                            var producto_id = selectBoxProducto.option('value');
                            var quiebre_stock = checkBoxQuiebreStock.option('value');
                            var stock_bajo_sugerido = checkBoxStockBajoSugerido.option('value');
                            var no_funciona_programa_paciente = checkBoxNoFuncionaProgramaPaciente.option('value');
                            var planograma = checkBoxPlanograma.option('value');
                            var exhibicion = checkBoxExhibicion.option('value');
                            var accion_competencia = checkBoxAccionCompetencia.option('value');
                            var capacitacion_realizada = checkBoxCapacitacionRealizada.option('value');
                            var informacion_especial = checkBoxInformacionEspecial.option('value');
                            var comentario = textAreaComentario.option('value');
                            var imagen = fileUploadImagen.option('value')[0];
                            var formData = new FormData();
                            formData.append('institucion_id', institucion_id);
                            formData.append('producto_id', producto_id);
                            formData.append('quiebre_stock', quiebre_stock);
                            formData.append('stock_bajo_sugerido', stock_bajo_sugerido);
                            formData.append('no_funciona_programa_paciente', no_funciona_programa_paciente);
                            formData.append('planograma', planograma);
                            formData.append('exhibicion', exhibicion);
                            formData.append('accion_competencia', accion_competencia);
                            formData.append('capacitacion_realizada', capacitacion_realizada);
                            formData.append('informacion_especial', informacion_especial);
                            formData.append('comentario', comentario);
                            formData.append('imagen', imagen);
                            $.ajax({
                                url: "/rest/incidencia/insertarDataIncidencia",
                                type: "POST",
                                data: formData,
                                contentType: false,
                                processData: false,
                                error: function(xhr, status) {
                                    if (typeof this.statusCode[xhr.status] != 'undefined') {
                                        return false;
                                    }
                                    console.log('ajax.error');
                                },
                                statusCode: {
                                    404: function(response) {
                                        console.log('status_code: 404');
                                    },
                                    500: function(response) {
                                        console.log('status_code: 500');
                                    }
                                }
                            }).done(function (data) {
                                swal({
                                    title: 'Incidencia guardada',
                                    text: 'Registro completo',
                                    type: 'success',
                                    confirmButtonText: 'OK'
                                }).then(
                                    function () { 
                                        location.reload(); 
                                        resolve();
                                    }
                                )
                            });
                        })
                    }
                }])
            } else {
                DevExpress.ui.notify('Complete campos obligatorios.', 'error');
            }
        }
    });

    detenerLoadingPanel();
});

function resetearFormulario(){
    selectBoxLocalDireccion.option('value', 'null');
    selectBoxProducto.option('value', 'null');
    checkBoxQuiebreStock.option('value', 'false');
    checkBoxStockBajoSugerido.option('value', 'false');
    checkBoxNoFuncionaProgramaPaciente.option('value', 'false');
    checkBoxExhibicion.option('value', 'false');
    checkBoxPlanograma.option('value', 'false');
    checkBoxAccionCompetencia.option('value', 'false');
    checkBoxCapacitacionRealizada.option('value', 'false');
    checkBoxInformacionEspecial.option('value', 'false');
    textAreaComentario.option('value', 'false');
    fileUploadImagen.option('value', null);
}

function insertarDataIncidencia(){

    var institucion_id = selectBoxLocalDireccion.option('value');
    var producto_id = selectBoxProducto.option('value');
    var quiebre_stock = checkBoxQuiebreStock.option('value');
    var stock_bajo_sugerido = checkBoxStockBajoSugerido.option('value');
    var no_funciona_programa_paciente = checkBoxNoFuncionaProgramaPaciente.option('value');
    var exhibicion = checkBoxExhibicion.option('value');
    var planograma = checkBoxPlanograma.option('value');
    var accion_competencia = checkBoxAccionCompetencia.option('value');
    var capacitacion_realizada = checkBoxCapacitacionRealizada.option('value');
    var informacion_especial = checkBoxInformacionEspecial.option('value');
    var comentario = textAreaComentario.option('value');
    var imagen = fileUploadImagen.option('value')[0];

    var formData = new FormData();
    formData.append('institucion_id', institucion_id);
    formData.append('producto_id', producto_id);
    formData.append('quiebre_stock', quiebre_stock);
    formData.append('stock_bajo_sugerido', stock_bajo_sugerido);
    formData.append('no_funciona_programa_paciente', no_funciona_programa_paciente);
    formData.append('exhibicion', exhibicion);
    formData.append('planograma', planograma);
    formData.append('accion_competencia', accion_competencia);
    formData.append('capacitacion_realizada', capacitacion_realizada);
    formData.append('informacion_especial', informacion_especial);
    formData.append('comentario', comentario);
    formData.append('imagen', imagen);

    $.ajax({
        url: "/rest/incidencia/insertarDataIncidencia",
        type: "POST",
        data: formData,
        contentType: false,
		processData: false,
        done: function(data){
            resolve();
            swal({
                title: 'Incidencia guardada',
                text: 'Registro completo',
                type: 'success',
                confirmButtonText: 'OK'
                }).then(function () { location.reload(); })
            },
        error: function (error) {
            console.log(error);
            console.log("error");
        }
    });
}

function obtenerDataCadena(){
    $.ajax({
        url: "../rest/incidencia/getDataCadena",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataCadena = data[0];
    });
}

function obtenerDataLocalDireccion(cadenaId){
    $.ajax({
        url: "../rest/incidencia/getDataLocalDireccion",
        dataType: "json",
        'async': false,
        data: {
            cadena_id : cadenaId
        },
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataLocalDireccion = data[0];
    });
}

function obtenerDataProducto(){
    $.ajax({
        url: "../rest/getDataProducto",
        dataType: "json",
        'async': false,
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataProducto = data[0];
    });
}