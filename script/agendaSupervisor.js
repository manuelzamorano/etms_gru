//var loadPanel;
var resultData;
var dataRecord;
var dataClient;
var dataStatus;
var dataRepresentante;
var currentRepresentante;
var popup = null;
var optionsScheduler;
var formaterHour = Globalize('en').dateFormatter({ skeleton: "Hm" })
var dateDefault = new Date();

$.when(promiseCultureLoad).done(function () {
    loadDataRepresentante();
});

function loadDataRepresentante(){
    $.ajax({
        url: "../rest/resource/getRepresentante",
        dataType: "json",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataRepresentante = data[0];

        if(dataRepresentante.length > 0)
        {
            currentRepresentante = dataRepresentante[0];
            loadSelectBoxRepresentante();
            loadScheduler();
        }
        else {
            DevExpress.ui.notify({
                message: 'Se necesita registrar representantes para ver la agenda'
            }, "error", 2000);
        }
    });
}

function loadDataScheduler() {
    $.ajax({
        url: "../rest/agendaSupervisor/getDataScheduler",
        dataType: "json",
        data: { representante_id: currentRepresentante.representante_id },
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataRecord = $.map(data[1], function (item) {
            item['startDate'] = new Date(item['startDate']);
            item['endDate'] = new Date(item['endDate']);
            return item;
        });
        dataClient = data[0];
        dataStatus = data[2];
        resultData.resolve();
    });
}

function loadSelectBoxRepresentante(){
    $("#selectBoxRepresentante").dxSelectBox({
        items: dataRepresentante,
        value: currentRepresentante.representante_id,
        displayExpr: "representante_descriptiva",
        valueExpr: "representante_id",
        onValueChanged: function(data) {
            currentRepresentante = getRepresentanteById(data.value);
            loadScheduler();
        }
    });
}

function loadScheduler(){
    resultData = $.Deferred();

    loadDataScheduler();

    $.when(resultData).done(function () {
        $('.page-loader').fadeOut();

        optionsScheduler = {
            views: ["workWeek"],
            currentView: "workWeek",
            currentDate: dateDefault,
            firstDayOfWeek: 1,
            startDayHour: 8,
            useDropDownViewSwitcher: true,
            endDayHour: 19,
            showAllDayPanel: false,
            "editing": {
                allowAdding: false,
                allowDeleting: false,
                allowDragging: false,
                allowResizing: false,
                allowUpdating: false
            },
            appointmentTemplate: function (itemData) {
                var $details = $("<div>").addClass("dx-scheduler-appointment-content-details");
                var startDate = formaterHour(itemData.startDate);
                var endDate = formaterHour(itemData.endDate);
                if(itemData.estado_codigo !== 'vigente')
                {
                    $("<div>").append(
                        '<i class=\'zmdi zmdi-minus-circle\' style=\'color: #ff6b68;\'></i> ' + itemData.text + '</div><div style="float:right">Cancelada</div>'
                        ).appendTo($details);
                    $("<div style='clear: both;'></div>").appendTo($details);
                }
                else
                {
                    if(itemData.visitado == 1){
                        $("<div>").append(
                            '<i class=\'zmdi zmdi-check-circle\' style=\'color: #3F51B5;\'></i> ' + itemData.text).appendTo($details);
                        $("<div style='clear: both;'></div>").appendTo($details);
                    }else{
                        $("<div>").text(itemData.text).appendTo($details); 
                    }
                }
                if (itemData.categoria_compañia_descriptiva) {
                    $("<div>").text(startDate + ' - ' + endDate + " | Visitas (" + itemData.contacto.toString() + "/" + itemData.frecuencia.toString() + ")").addClass("dx-scheduler-appointment-content-date").appendTo($details);
                    $("<div>").text("Categoría " + itemData.categoria_compañia_descriptiva).appendTo($details);
                }
                
                return $details;
            },
            width: "100%",
            height: "100%",
            dataSource: dataRecord,
            resources: [
                {
                    field: 'cliente_id',
                    dataSource: dataClient,
                    label: "Cliente"
                }
            ],
            appointmentTooltipTemplate: function (data, container) {
                var markup = tooltipAgenda(data);
                return markup;
            }
        };

        var scheduler = $("#scheduler").dxScheduler(optionsScheduler).dxScheduler('instance');
    });
}

function combineDateAndTime(date, time) {
    timeString = time.getHours() + ':' + time.getMinutes() + ':00';

    var year = date.getFullYear();
    var month = date.getMonth() + 1; // Jan is 0, dec is 11
    var day = date.getDate();
    var dateString = '' + year + '-' + month + '-' + day;
    var combined = new Date(dateString + ' ' + timeString);

    return combined;
};

function getHour(date, hour) {
    var d = new Date(date);
    d.setHours(hour);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d;
}

function getClientById(id) {
    for(var i = 0; i < dataClient.length; i++) {
        if (dataClient[i].id === id)
            return dataClient[i];
    }
    return null;
}

function getStatusById(id) {
    for(var i = 0; i < dataStatus.length; i++) {
        if (dataStatus[i].id === id)
            return dataStatus[i];
    }
    return null;
}

function getStatusByCodigo(codigo) {
    for(var i = 0; i < dataStatus.length; i++) {
        if (dataStatus[i].codigo === codigo)
            return dataStatus[i];
    }
    return null;
}

function getRepresentanteById(id) {
    for(var i = 0; i < dataRepresentante.length; i++) {
        if (dataRepresentante[i].representante_id === id)
            return dataRepresentante[i];
    }
    return null;
}

function tooltipAgenda(data) {
    var startDate = formaterHour(data.startDate);
    var endDate = formaterHour(data.endDate);

    var tooltipTitle = '';
    var tooltipDate = '';
    var restoreOrDeleteButton = '';

    if(data.estado_codigo === 'vigente') {
        tooltipTitle = data.text;
        tooltipDate = startDate + ' - ' + endDate;
        restoreOrDeleteButton = "<div class='visit-tooltip-buttons'>" +
                                    "<div class='visitDelete'></div>" +
                                "</div>";
    }
    else {
        tooltipTitle = "<div style='float:left'>" + data.text + "</div>" +
                "<div style='float:right'>Cancelada</div>" +
                "<div style='clear: both;'></div>";

        tooltipDate = "<div style='float:left'>" + startDate + ' - ' + endDate + "</div>" +
                "<div style='float:right'>Motivo: " + getStatusById(data.estado_id).descriptiva + "</div>" +
                "<div style='clear: both;'></div>";

        
    }

    return $("<div class='visit-tooltip'>" +
                "<div class='visit-tooltip-title'>" + tooltipTitle + "</div>" +
                "<div class='visit-tooltip-date'>" + tooltipDate + "</div>" +
            "</div>");
}

function getHourAndMinutes(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var timeString = "" + hours;
    timeString += ((minutes < 10) ? ":0" : ":") + minutes;
    return timeString;
}

function formatTooltipDate(startDate, endDate) {
    var dateTimeFormat = "MMMM d h:mm tt";
    if ($("#scheduler").dxScheduler("instance").option("currentView") === "month") {
        var startDateString = formaterHour(startDate, dateTimeFormat) + " - ",
            endDateString = startDate.getDate() === endDate.getDate() ? formaterHour(endDate) : formaterHour(endDate, dateTimeFormat);
        return startDateString + endDateString;
    }
    return formaterHour(startDate) + " - " + formaterHour(endDate);
}