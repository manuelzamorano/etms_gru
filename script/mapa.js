
var resultData, dataCiclo, mapGo, currentLatitud, currentLongitud, dateFechaVisita, currentId;
var dataMap = new Array();

$.when(promiseCultureLoad).done(function () {
    loadDataCicloActual();
    resultData = $.Deferred();    
    mapGo = $("#map").dxMap({
        zoom: 10,
        width: '100%',
        height: 400,
        controls: true
    }).dxMap("instance");

    dateFechaVisita = $("#dateFechaVisita").dxCalendar({
        value: new Date()
        , min: dataCiclo[0].ciclo_fecha_inicio
        , max: dataCiclo[0].ciclo_fecha_termino
        , onValueChanged: function(e){
            loadDataMapaAgenda(e.value);
        }
    });

    //Agrega ubicacion actual al mapa
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            currentLatitud = position.coords.latitude;
            currentLongitud = position.coords.longitude;
            loadDataDataMap();
            addNearMarkers(mapGo, dataMap);
        });
    }
    detenerLoadingPanel();
});

function loadDataDataMap() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/mapa/getDataCurrentPosition",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        dataMap = data[0];
        resultData.resolve();
    });
    resultData.resolve();
};

function loadDataCicloActual() {
    $.ajax({
        dataType: "json",
        'async': false,
        url: "../rest/resource/getDataCicloActual",
        error: function () {
            console.log("No se pudo cargar la data de DataMap");
        }
    }).done(function (data) {
        dataCiclo = data[0];
    });
};

function loadDataMapaAgenda(fecha) {
    $.ajax({
        dataType: "json",
        'async': false,
        data: {fecha:fecha},
        url: "../rest/mapa/getDataMapaAgenda",
        error: function(xhr, status) {
            if (typeof this.statusCode[xhr.status] != 'undefined') {
                return false;
            }
            console.log('ajax.error');
        },
        statusCode: {
            404: function(response) {
                console.log('status_code: 404');
            },
            500: function(response) {
                console.log('status_code: 500');
            }
        }
    }).done(function (data) {
        mapGo.option('markers', []);
        if(data[0].length > 0 ){
            addNearMarkers(mapGo, data[0]);
        }else{
            swal('No se encontraron registros!', 'Asigne locales en la agenda', 'warning').then(function () {});
        }
    });
};

function addNearMarkers(mapGo, dataMap) {
    for (var i = 0; i < dataMap.length; i++) {
        var markerLongitud = dataMap[i].longitud;
        var markerLatitud = dataMap[i].latitud;
        var pageLink, icon;
        switch(dataMap[i].tipo_cliente) {
            case 1:
                pageLink = "formularioVisitaInstitucion";
                break;
            case 2:
                pageLink = "formularioVisitaProfesional";
                break;
            default:
                pageLink = "";
        }
        switch (dataMap[i].categoria) {
            case 'A':
                icon = '../img/Icon/map_marker_green.png';
                break;
            case 'B':
                icon = '../img/Icon/map_marker_orange.png';
                break;
            case 'C':
                icon = '../img/Icon/map_marker_red.png';
                break;
            case 'D':
                icon = '../img/Icon/map_marker_yellow.png';
                break;
            case 'S/C':
                icon = '../img/Icon/map_marker_gray.png';
                break;
            default:
                icon = "";
        }
        
        mapGo.addMarker({
            location: [dataMap[i].latitud, dataMap[i].longitud],
            tooltip: "<div><b><p style=\"color: black;\">" + dataMap[i].nombre +
                "<br> " + dataMap[i].direccion + "</p></b></div>" +
                //"<span class='rute' onclick='agregarRuta(\"" + markerLatitud + "\",\"" + markerLongitud + "\")'>Ver Ruta</span>" +
                "<b><a class='rute' onclick='agregarRuta(\"" + markerLatitud + "\",\"" + markerLongitud + "\")'>Ver Ruta</a></b>" +
                "<b><a style='float: right' href='/" + pageLink + "?id=" + dataMap[i].cliente_id + "'>Realizar visita</a></b>" +
                "<div style='clear: both; height: 1px'></div>"
            ,
            iconSrc: icon
        });
    }
}

function agregarRuta(latitud, longitud) {
    mapGo.option('routes', [
        {
            weight: 6,
            color: "blue",
            opacity: 0.5,
            mode: "",
            locations: [
                [currentLatitud, currentLongitud],
                [latitud, longitud]
            ]
        }
    ]);
}