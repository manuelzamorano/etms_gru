module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: ['public/js/inc/*.js'],
                dest: 'public/js/app.js',
            },
        },
        uglify: {
            my_target: {
                files: {
                    'public/js/app.min.js': ['public/js/app.js']
                }    
            },
            dynamic_mappings: {
                // Grunt will search for "**/*.js" under "lib/" when the "uglify" task
                // runs and build the appropriate src-dest file mappings then, so you
                // don't need to update the Gruntfile when files are added or removed.
                files: [
                    {
                        expand: true,     // Enable dynamic expansion.
                        cwd: 'script/',      // Src matches are relative to this path.
                        src: ['**/*.js'], // Actual pattern(s) to match.
                        dest: 'public/js/views/',   // Destination path prefix.
                        ext: '.min.js',   // Dest filepaths will have this extension.
                        extDot: 'first'   // Extensions in filenames begin after the first dot
                    },
                ],
            }
        },
        less: {
            development: {
                options: {
                    paths: ["css"]
                },
                files: {
                    "public/css/inc/app.css": "public/less/app.less",
                },
                cleancss: true
            }
        },
        csssplit: {
            your_target: {
                src: ['public/css/inc/app.css'],
                dest: 'public/css/inc/app.css',
                options: {
                    maxSelectors: 4000,
                    suffix: '_'
                }
            },
        },
        cssmin: {
            options: {
                keepSpecialComments: 0
            },
            target: {
                files: {
                    'public/css/app_1.min.css': ['public/css/inc/app_1.css'],
                    'public/css/app_2.min.css': ['public/css/inc/app_2.css'],
                }
            }
        },
        copy: {
            main: {
                expand: true,
                cwd: '',
                src: [
                    "**/*",
                    '!**/node_modules/**',
                    '!**/.idea/**',
                    '!**/*.text**',
                    '!**/*.txt**',
                    '!**/.DS_Store**',
                    '!**/public/less/**',
                    '!**/public/js/inc/**',
                    '!**/public/js/app.js**',
                    '!**/package.json**',
                    '!**/gruntfile.js**',
                    '!**/public/css/inc/**'
                ],
                dest: 'dist/'
            },
        },
        clean: ['**/.idea', '**/.DS_Store', 'dist'],
        watch: {
            less: {
                files: ['public/less/**/*.less'], // which files to watch
                tasks: ['less', 'csssplit', 'cssmin']
            },
            js: {
                files: ['public/js/inc/**/*.js'], // which files to watch
                tasks: ['concat', 'uglify:my_target']
            },
            js_script: {
                files: ['script/**/*.js'], // which files to watch
                tasks: ['concat', 'uglify:dynamic_mappings']
            }
        }
    });

    // Load the plugin that provides the "less" task.
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-csssplit');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('dist', ['clean', 'copy']);

};