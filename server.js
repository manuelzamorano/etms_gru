///////////////////////////////////////////////////////////////////
//////////////////////////  VARIABLES  ////////////////////////////
///////////////////////////////////////////////////////////////////
var moment = require('moment-timezone');
moment.tz.setDefault("America/Santiago");
var express = require('express');
var requireDir = require('require-dir');
var promise = require('promise');
var expressSession = require('express-session');
var msSQLStore = require('connect-mssql')(expressSession);
var fs = require('fs');
var controllers = requireDir('./controllers');
var session;
var configDB = {
    user: 'sa',
    password: 'GoPharmaSolutions01',
    server: '10.172.5.221\\GRU',
    database: 'ETMS', 
    options: {
        useUTC: false
    }
}
var app = express();
var multer = require('multer');
var storage = multer.memoryStorage();
var upload = multer({ 
    storage: storage,
    limits: { fileSize: 4194304 } // 4 megas
})

////////////////////////////////////////////////////////////////////////
////////////////////////// CONFIGURACIONES /////////////////////////////
////////////////////////////////////////////////////////////////////////
app.set("view engine", "pug");
app.use(express.static("public"));
app.set('trust proxy', 1);
app.use(expressSession({
    store: new msSQLStore(configDB),
    resave: true,
    saveUninitialized: false,
    secret: 'go_session',
    name : 'goSessionId'
}));
app.get('/upload', function(req, res) {
    res.render('upload');
});
/////////////////////////////////////////////////////////////////////////
//////////////////////////  MODULOS GENERALES ///////////////////////////
/////////////////////////////////////////////////////////////////////////
app.set('views', './views') 
app.get('/', function(req, res){
    session = req.session;

    if(session.cuenta_id) {
        switch(session.rol_codigo) {
            case 'administrador':
                res.redirect('/inicioAdministrador');
                break;
            case 'supervisor':
                res.redirect('/inicioSupervisor');
                break;
            case 'representante':
                res.redirect('/inicio');
                break;
            case 'director':
                res.redirect('/inicioDirector');
                break;
        }
    }
    else{
        res.redirect('/login');
    }
});
app.get('/login', function(req, res){
    res.render('login');
});
app.post('/login', upload.array(),function(req, res){
    session = req.session;
    var usuario = req.body.user;
    var contraseña = req.body.password;
    if(usuario === '' || contraseña === ''){
        res.render('login');
        return;
    }

    var parameters =
    {
        "usuario": usuario,
        "contraseña": contraseña
    }
    var responseMessage;
    controllers.login.getCuentaAccesoForLogin(configDB, parameters) 
        .then(data => {
            if(data[0].length > 0) // No viene data
            {
                var result = data[0][0]; // Primera fila de primer select
                if(result.cuenta_activa && !result.cuenta_bloqueo_acceso && result.cuenta_valida)
                {
                    session.cuenta_id = result.cuenta_id;
                    session.nombre_completo = result.nombre_completo;
                    session.nombre = result.nombre;
                    session.apellido_paterno = result.apellido_paterno;
                    session.rol_codigo = result.rol_codigo;
                    session.rol_descriptiva = result.rol_descriptiva;
                    switch(session.rol_codigo){
                        case 'representante':
                            res.redirect('/inicio');
                            break;
                        case 'supervisor':
                            res.redirect('/inicioSupervisor');
                            break;
                        case 'director':
                            res.redirect('/inicioDirector');
                            break;
                        case 'administrador':
                            res.redirect('/inicioAdministrador');
                            break;
                    }
                }
                else{
                    responseMessage = 'La contraseña es incorrecta. Vuelve a intentarlo.';
                    res.render('login', {message: responseMessage});
                }
            }
            else {
                responseMessage = 'La contraseña es incorrecta. Vuelve a intentarlo.';
                res.render('login', {message: responseMessage});
            }
        })
        .catch(err => {
            res.render('login', {message: responseMessage});
        })
});
app.get('/reset', function(req, res){
    res.render('reset');
});
app.get('/rest/datosUsuario', function(req, res){
    session = req.session;
    res.json(session);
});

////////////////////////////////////////////////////////////////////////
//////////////////////////  MODULOS POR ROL ////////////////////////////
////////////////////////////////////////////////////////////////////////

//REPRESENTANTE
    app.get('/inicio', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {
                cuenta_id:session.cuenta_id
            }
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) {
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) {
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('inicio', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else{
            res.redirect('/login');
        }
    });

    app.get('/agenda', function(req, res){
        session = req.session;
        
        if(session.cuenta_id) {
            var parameters = {
                cuenta_id:session.cuenta_id
            }

            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }

                    res.render('agenda', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.json(err);
                })
        }
        else{
            res.redirect('/login');
        }
    });

    app.get('/mapa', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('mapa', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/kardexInstitucion', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('kardexInstitucion', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/kardexProfesional', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('kardexProfesional', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/kardexHospitalario', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('kardexHospitalario', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/formularioVisitaInstitucion', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('formularioVisitaInstitucion', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/formularioVisitaProfesional', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('formularioVisitaProfesional', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/formularioVisitaHospitalario', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('formularioVisitaHospitalario', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/visitasInstitucion', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('visitasInstitucion', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/visitasProfesional', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('visitasProfesional', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/visitasHospitalario', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('visitasHospitalario', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/cobertura', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('cobertura', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/incidencia', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('incidencia', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

//SUPERVISOR
    app.get('/inicioSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('inicioSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/agendaSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {
                cuenta_id:session.cuenta_id
            }
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('agendaSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else{
            res.redirect('/login');
        }
    });

    app.get('/visitasInstitucionSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('visitasInstitucionSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/visitasProfesionalSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('visitasProfesionalSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/visitasHospitalarioSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('visitasHospitalarioSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/kardexInstitucionSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('kardexInstitucionSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/kardexProfesionalSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('kardexProfesionalSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/kardexHospitalarioSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('kardexHospitalarioSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/incidenciaSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('incidenciaSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/coberturaSupervisor', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('coberturaSupervisor', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        }
    });

    app.get('/incidenciaImagen', function(req, res){
        res.render('incidenciaImagen');
    });

//ADMINISTRADOR

    app.get('/inicioAdministrador', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('inicioAdministrador', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crearUsuario', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crearUsuario', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/importarVisita', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('importarVisita', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudProfesional', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudProfesional', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudProfesionalEditar', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudProfesionalEditar', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudProfesionalNuevo', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudProfesionalNuevo', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudInstitucion', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudInstitucion', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudInstitucionEditar', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudInstitucionEditar', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudInstitucionNuevo', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudInstitucionNuevo', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudHospitalario', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudHospitalario', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudHospitalarioEditar', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudHospitalarioEditar', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudHospitalarioNuevo', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudHospitalarioNuevo', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudKardex', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudKardex', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudDescuento', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudDescuento', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudVisita', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudVisita', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

    app.get('/crudIncidencia', function(req, res){
        session = req.session;
        if(session.cuenta_id) {
            var parameters = {cuenta_id:session.cuenta_id}
            controllers.layout.getMenu(configDB, parameters)
                .then(data => {
                    var menu = {
                        padre: data[0].filter(function(a) { 
                            return !a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                        , hijo: data[0].filter(function(a) { 
                            return a.estudio_padre_codigo;
                        }).sort(function(a, b) {            
                            return a.orden - b.orden;
                        })
                    }
                    res.render('crudIncidencia', { 
                        menu: menu,
                        path: req.path
                    });
                })
                .catch(err => {
                    res.render('error');
                })
        }
        else {
            res.redirect('/login');
        } 
    });

////////////////////////////////////////////////////////////////////////
//////////////////////////  WEB SERVICES ///////////////////////////////
////////////////////////////////////////////////////////////////////////

//CONTROLLER AGENDA
    app.get('/rest/agenda/getDataScheduler', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.agenda.getDataScheduler(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/agenda/getDataCliente', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.agenda.getDataCliente(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/agenda/updateDataScheduler', function(req, res){
        session = req.session;
        var parameters = {
                id: req.query.id
                , fecha_inicio: new Date(req.query.startDate)
                , fecha_fin: new Date(req.query.endDate)
            }
        controllers.agenda.updateDataScheduler(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/agenda/insertDataScheduler', function(req, res){
        session = req.session;
        var parameters = {
                cliente_id: req.query.clienteId
                , fecha: new Date(req.query.fecha)
                , cuenta_acceso_id:session.cuenta_id }
        controllers.agenda.insertDataScheduler(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/agenda/updateStateScheduler', function(req, res){
        session = req.session;
        var parameters = {
                id: req.query.id
                , estado_id: req.query.estado_id
            }
        controllers.agenda.updateStateScheduler(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/agenda/deleteDataScheduler', function(req, res){
        session = req.session;
        var parameters = {
                id: req.query.id
            }
        controllers.agenda.deleteDataScheduler(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/agenda/getDataEstadoAgenda', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.agenda.getDataEstadoAgenda(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/agenda/getCantidadAgenda', function(req, res){
        session = req.session;
        var parameters = {
                cuenta_acceso_id: session.cuenta_id
                , fecha: new Date(req.query.fecha)
            }
        controllers.agenda.getCantidadAgenda(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/agenda/insertarClonacionAgenda', function(req, res){
        session = req.session;
        var parameters = {
                cuenta_acceso_id: session.cuenta_id
                , fechaOrigen: new Date(req.query.fechaOrigen)
                , fechaDestino: new Date(req.query.fechaDestino)
            }
        controllers.agenda.insertarClonacionAgenda(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER AGENDA SUPERVISOR
    app.get('/rest/agendaSupervisor/getDataScheduler', function(req, res){
        session = req.session;
        var parameters = {representante_id:req.query.representante_id}
        controllers.agendaSupervisor.getDataScheduler(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER COBERTURA
    app.get('/rest/cobertura/getDataCobertura', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id    : session.cuenta_id
        }
        controllers.cobertura.getDataCobertura(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER COBERTURA SUPERVISOR
    app.get('/rest/coberturaSupervisor/getDataCoberturaSupervisor', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id    : session.cuenta_id
        }
        if(!req.query.ciclo_id ==''){
            parameters.ciclo_id = parseInt(req.query.ciclo_id);
        }
        controllers.coberturaSupervisor.getDataCoberturaSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER FORMULARIO VISITA INSTITUCION
    app.get('/rest/formularioVisitaInstitucion/getDataVisita', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , cliente_id: parseInt(req.query.cliente_id)
        }
        controllers.formularioVisitaInstitucion.getDataVisita(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaInstitucion/getDataEncuesta', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , cliente_id: req.query.cliente_id
        }
        controllers.formularioVisitaInstitucion.getDataEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaInstitucion/insertAlternativaEncuesta', function(req, res){1
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , alternativa_id: req.query.alternativa_id
            , visita_id: req.query.visita_id
        }
        controllers.formularioVisitaInstitucion.insertAlternativaEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaInstitucion/insertVisita', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cliente_id:                         parseInt(req.query.cliente_id)
            , cuenta_acceso_id:                 session.cuenta_id
            , ciclo_id:                         parseInt(req.query.ciclo_id)
            , tipo_visita_id:                   parseInt(req.query.tipo_visita_id)
            , es_acompañada:                    JSON.parse(req.query.es_acompañada)
            , fecha:                            new Date(req.query.fecha)
            , informacion:                      req.query.informacion
            , longitud:                         req.query.longitud
            , latitud:                          req.query.latitud
        };
        controllers.formularioVisitaInstitucion.insertVisita(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaInstitucion/insertVisitaProducto', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , visita_id:parseInt(req.query.visita_id)
            , producto_id:parseInt(req.query.producto_id)
            , real:parseInt(req.query.real)
            , real_promocional:parseInt(req.query.real_promocional)
        }
        controllers.formularioVisitaInstitucion.insertVisitaProducto(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaInstitucion/insertVisitaEncuesta', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , visita_id:parseInt(req.query.visita_id)
            , alternativa_id:parseInt(req.query.alternativa_id)
        }
        controllers.formularioVisitaInstitucion.insertVisitaEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER FORMULARIO VISITA HOSPITALARIO
    app.get('/rest/formularioVisitaHospitalario/getDataVisita', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , cliente_id: parseInt(req.query.cliente_id)
        }
        controllers.formularioVisitaHospitalario.getDataVisita(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaHospitalario/getDataEncuesta', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , cliente_id: req.query.cliente_id
        }
        controllers.formularioVisitaHospitalario.getDataEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaHospitalario/insertAlternativaEncuesta', function(req, res){1
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , alternativa_id: req.query.alternativa_id
            , visita_id: req.query.visita_id
        }
        controllers.formularioVisitaHospitalario.insertAlternativaEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaHospitalario/insertVisita', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cliente_id:                         parseInt(req.query.cliente_id)
            , cuenta_acceso_id:                 session.cuenta_id
            , ciclo_id:                         parseInt(req.query.ciclo_id)
            , tipo_visita_id:                   parseInt(req.query.tipo_visita_id)
            , es_acompañada:                    JSON.parse(req.query.es_acompañada)
            , fecha:                            new Date(req.query.fecha)
            , informacion:                      req.query.informacion
            , longitud:                         req.query.longitud
            , latitud:                          req.query.latitud
        };
        controllers.formularioVisitaHospitalario.insertVisita(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaHospitalario/insertVisitaProducto', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , visita_id:parseInt(req.query.visita_id)
            , producto_id:parseInt(req.query.producto_id)
            , real:parseInt(req.query.real)
            , real_promocional:parseInt(req.query.real_promocional)
        }
        controllers.formularioVisitaHospitalario.insertVisitaProducto(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaHospitalario/insertVisitaEncuesta', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , visita_id:parseInt(req.query.visita_id)
            , alternativa_id:parseInt(req.query.alternativa_id)
        }
        controllers.formularioVisitaHospitalario.insertVisitaEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER FORMULARIO VISITA PROFESIONAL
    app.get('/rest/formularioVisitaProfesional/getDataVisita', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , cliente_id: parseInt(req.query.cliente_id)
        }
        controllers.formularioVisitaProfesional.getDataVisita(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaProfesional/getDataEncuesta', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , cliente_id: req.query.cliente_id
        }
        controllers.formularioVisitaProfesional.getDataEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaProfesional/insertAlternativaEncuesta', function(req, res){1
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , alternativa_id: req.query.alternativa_id
            , visita_id: req.query.visita_id
        }
        controllers.formularioVisitaProfesional.insertAlternativaEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaProfesional/insertVisita', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cliente_id:                         parseInt(req.query.cliente_id)
            , cuenta_acceso_id:                 session.cuenta_id
            , ciclo_id:                         parseInt(req.query.ciclo_id)
            , tipo_visita_id:                   parseInt(req.query.tipo_visita_id)
            , es_acompañada:                    JSON.parse(req.query.es_acompañada)
            , fecha:                            new Date(req.query.fecha)
            , informacion:                      req.query.informacion
            , longitud:                         req.query.longitud
            , latitud:                          req.query.latitud
        };
        controllers.formularioVisitaProfesional.insertVisita(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaProfesional/insertVisitaProducto', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , visita_id:parseInt(req.query.visita_id)
            , producto_id:parseInt(req.query.producto_id)
            , real:parseInt(req.query.real)
            , real_promocional:parseInt(req.query.real_promocional)
        }
        controllers.formularioVisitaProfesional.insertVisitaProducto(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/formularioVisitaProfesional/insertVisitaEncuesta', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , visita_id:parseInt(req.query.visita_id)
            , alternativa_id:parseInt(req.query.alternativa_id)
        }
        controllers.formularioVisitaProfesional.insertVisitaEncuesta(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER INCIDENCIA
    app.get('/rest/incidencia/getDataCadena', function(req, res){
        session = req.session;
        var menu = {};
        controllers.incidencia.getDataCadena(configDB) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/incidencia/getDataLocalDireccion', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id    : session.cuenta_id
            , cadena_id          : req.query.cadena_id
        }
        controllers.incidencia.getDataLocalDireccion(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.post('/rest/incidencia/insertarDataIncidencia', upload.single('imagen'), function(req, res){
        session = req.session;
        var parameters = {
            cuenta_acceso_id                : session.cuenta_id
            , institucion_id                : parseInt(req.body.institucion_id)
            , producto_id                   : parseInt(req.body.producto_id)
            , comentario                    : req.body.comentario
            , quiebre_stock                 : parseBoolean(req.body.quiebre_stock)
            , stock_bajo_sugerido           : parseBoolean(req.body.stock_bajo_sugerido)
            , no_funciona_programa_paciente : parseBoolean(req.body.no_funciona_programa_paciente)
            , planograma                    : parseBoolean(req.body.planograma)
            , accion_competencia            : parseBoolean(req.body.accion_competencia)
            , capacitacion_realizada        : parseBoolean(req.body.capacitacion_realizada)
            , informacion_especial          : parseBoolean(req.body.informacion_especial)
            , exhibicion                    : parseBoolean(req.body.exhibicion)
        };
        if (typeof req.file != 'undefined'){
            parameters.imagen = req.file.buffer
        } else {
            console.log('No subio imagen')
        }
        controllers.incidencia.insertarDataIncidencia(configDB, parameters)
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/incidencia/getDataIncidenciaProducto', function(req, res){
        session = req.session;
        var menu = {};
        controllers.incidencia.getDataIncidenciaProducto(configDB) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER INCIDENCIA SUPERVISOR
    app.get('/rest/incidenciaSupervisor/getDataIncidencia', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id    : session.cuenta_id
        }
        controllers.incidenciaSupervisor.getDataIncidencia(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/incidenciaImagen', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            incidencia_id : parseInt(req.query.incidencia_id)
        }
        controllers.incidenciaImagen.getDataImagen(configDB, parameters) // Returns a Promise
            .then(data => {
                res.json(data);
            })
            .catch(err => {
                res.status(500).json({ error: err.toString(), number: err.number });
            });
    });
    
//CONTROLLER INICIO
    app.get('/rest/inicio/getDataInicio', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
        }
        controllers.inicio.getDataInicioRepresentante(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    }); 

    app.get('/rest/inicio/getDataInstitucion', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , tipo_cliente:'institucion'
        }
        controllers.inicio.getDataInicio(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    }); 

    app.get('/rest/inicio/getDataProfesional', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , tipo_cliente:'profesional'
        }
        controllers.inicio.getDataInicio(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER INICIO SUPERVISOR
    app.get('/rest/inicioSupervisor/getDataInicioSupervisor', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , representante_id: null
            , ciclo_id: null
        }
        controllers.inicioSupervisor.getDataInicioSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/inicioSupervisor/getDataSelectBoxSupervisor', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
        }
        if(!req.query.ciclo_id ==''){
            parameters.ciclo_id = parseInt(req.query.ciclo_id);
        }
        if(!req.query.representante_id ==''){
            parameters.representante_id = parseInt(req.query.representante_id);
        }
        controllers.inicioSupervisor.getDataInicioSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER KARDEX INSTITUCION
    app.get('/rest/kardex/getDataKardexInstitucion', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'institucion'
        }
        controllers.kardex.getDataKardex(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER KARDEX INSTITUCION SUPERVISOR
    app.get('/rest/kardex/getDataKardexInstitucionSupervisor', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'institucion'
        }
        controllers.kardexSupervisor.getDataKardexSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER KARDEX PROFESIONAL
    app.get('/rest/kardex/getDataKardexProfesional', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'profesional'
        }
        controllers.kardex.getDataKardex(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER KARDEX PROFESIONAL SUPERVISOR
    app.get('/rest/kardex/getDataKardexProfesionalSupervisor', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'profesional'
        }
        controllers.kardexSupervisor.getDataKardexSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER KARDEX HOSPITALARIO
    app.get('/rest/kardex/getDataKardexHospitalario', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'hospitalario'
        }
        controllers.kardex.getDataKardex(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER KARDEX HOSPITALARIO SUPERVISOR
    app.get('/rest/kardex/getDataKardexHospitalarioSupervisor', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'hospitalario'
        }
        controllers.kardexSupervisor.getDataKardexSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER LAYOUT
    app.get('/rest/getDataSelectBoxCiclo', function(req, res){
        session = req.session;
        var menu = {};
        controllers.inicioSupervisor.getDataSelectBoxCiclo(configDB) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/getDataSelectBoxRepresentante', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
        }
        controllers.inicioSupervisor.getDataSelectBoxRepresentante(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/getDataCadena', function(req, res){
        session = req.session;
        var menu = {};
        controllers.reporteInstitucion.getDataCadena(configDB) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/getDataProducto', function(req, res){
        session = req.session;
        var menu = {};
        controllers.layout.getDataProducto(configDB) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER MAPA
    app.get('/rest/mapa/getDataCurrentPosition', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {cuenta_acceso_id:session.cuenta_id, lat:req.query.lat, lng:req.query.lng}
        controllers.mapa.getDataCurrentPosition(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/mapa/getDataMapaAgenda', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
            , fecha:            new Date(req.query.fecha)
        };
        controllers.mapa.getDataClienteAgenda(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        }); 
    });

//CONTROLLER VISITAS
    app.get('/rest/visitas/getVisitasInstitucion', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , tipo_cliente: 'institucion'
        }
        controllers.visitas.getVisitas(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/visitas/getVisitasProfesional', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , tipo_cliente: 'profesional'
        }
        controllers.visitas.getVisitas(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/visitas/getVisitasHospitalario', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id:session.cuenta_id
            , tipo_cliente: 'hospitalario'
        }
        controllers.visitas.getVisitas(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER VISITAS SUPERVISOR

    app.get('/rest/visitasSupervisor/getDataVisitasInstitucion', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'institucion'
        }
        if(!req.query.ciclo_id ==''){
            parameters.ciclo_id = parseInt(req.query.ciclo_id);
        }
        controllers.visitasSupervisor.getVisitasSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/visitasSupervisor/getDataVisitasProfesional', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'profesional'
        }
        if(!req.query.ciclo_id ==''){
            parameters.ciclo_id = parseInt(req.query.ciclo_id);
        }
        controllers.visitasSupervisor.getVisitasSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/visitasSupervisor/getDataVisitasHospitalario', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {
            cuenta_acceso_id: session.cuenta_id
            , tipo_cliente: 'hospitalario'
        }
        if(!req.query.ciclo_id ==''){
            parameters.ciclo_id = parseInt(req.query.ciclo_id);
        }
        controllers.visitasSupervisor.getVisitasSupervisor(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//RESOURCES
    app.get('/rest/resource/getRepresentante', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.layout.getDataRepresentante(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/resource/getCiclo', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.layout.getDataCiclo(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/resource/getDataCicloActual', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.layout.getDataCicloActual(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/resource/getDataFechaCiclo', function(req, res){
        session = req.session;
        var menu = {};
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.layout.getDataFechaCiclo(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

////////////////////////////////////////////////////////////////////////
//////////////////////  CONTROLLER MANTENEDORES ////////////////////////
////////////////////////////////////////////////////////////////////////
    
//CONTROLLER CRUD DATA

    app.get('/rest/crudData/obtenerCrudDataBrickSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataBrickSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataCadenaSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataCadenaSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataEspecialidadSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataEspecialidadSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataGeneroSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataGeneroSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataHorarioSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataHorarioSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataNacionalidadSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataNacionalidadSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataCategoriaAdopcionSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataCategoriaAdopcionSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataCategoriaCompaniaSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataCategoriaCompaniaSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataCategoriaPotencialSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataCategoriaPotencialSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataTipoInstitucionSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataTipoInstitucionSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudData/obtenerCrudDataTipoProfesionalSelectBox', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudData.obtenerCrudDataTipoProfesionalSelectBox(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });


//CONTROLLER CRUD PROFESIONAL
    app.get('/rest/crudProfesional/obtenerDataProfesional', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudProfesional.obtenerDataProfesional(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudProfesional/obtenerDataProfesionalPorID', function(req, res){
        session = req.session;
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
            , profesional_id:   parseInt(req.query.profesional_id)
        };
        controllers.crudProfesional.obtenerDataProfesionalPorID(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudProfesional/actualizarDataProfesional', function(req, res){
        session = req.session;
        
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
        };

        if (typeof req.query.tipo_profesional_id != 'undefined'&& req.query.tipo_profesional_id != "" ) 
            { parameters.tipo_profesional_id = parseInt(req.query.tipo_profesional_id); }

        if (typeof req.query.calle != 'undefined' && req.query.calle != "" ) 
            { parameters.calle = req.query.calle; }

        if (typeof req.query.brick_id != 'undefined'&& req.query.brick_id != "" ) 
            { parameters.brick_id = parseInt(req.query.brick_id); }

        if (typeof req.query.categoria_adopcion_id != 'undefined'&& req.query.categoria_adopcion_id != "" ) 
            { parameters.categoria_adopcion_id = parseInt(req.query.categoria_adopcion_id); }

        if (typeof req.query.categoria_compania_id != 'undefined'&& req.query.categoria_compania_id != "" ) 
            { parameters.categoria_compania_id = parseInt(req.query.categoria_compania_id); }

        if (typeof req.query.categoria_estrategica != 'undefined'&& req.query.categoria_estrategica != "" ) 
            { parameters.categoria_estrategica = req.query.categoria_estrategica; }

        if (typeof req.query.categoria_potencial_id != 'undefined'&& req.query.categoria_potencial_id != "" ) 
            { parameters.categoria_potencial_id = parseInt(req.query.categoria_potencial_id); }

        if (typeof req.query.comuna != 'undefined'&& req.query.comuna != "" ) 
            { parameters.comuna = req.query.comuna; }

        if (typeof req.query.correo != 'undefined'&& req.query.correo != "" ) 
            { parameters.correo = req.query.correo; }

        if (typeof req.query.especialidad_2 != 'undefined'&& req.query.especialidad_2 != "" ) 
            { parameters.especialidad_2 = req.query.especialidad_2; }

        if (typeof req.query.especialidad_id != 'undefined'&& req.query.especialidad_id != "" ) 
            { parameters.especialidad_id = parseInt(req.query.especialidad_id); }

        if (typeof req.query.genero_id != 'undefined'&& req.query.genero_id != "" ) 
            { parameters.genero_id = parseInt(req.query.genero_id); }

        if (typeof req.query.id != 'undefined'&& req.query.id != "" ) 
            { parameters.profesional_id = parseInt(req.query.id); }        

        if (typeof req.query.nacionalidad_id != 'undefined'&& req.query.nacionalidad_id != "" ) 
            { parameters.nacionalidad_id = parseInt(req.query.nacionalidad_id); }

        if (typeof req.query.nombre != 'undefined'&& req.query.nombre != "" ) 
            { parameters.nombre = req.query.nombre; }

        if (typeof req.query.telefono != 'undefined'&& req.query.telefono != "" ) 
            { parameters.telefono = req.query.telefono; }

        if (typeof req.query.tipo_profesional_id != 'undefined'&& req.query.tipo_profesional_id != "" ) 
            { parameters.tipo_profesional_id = parseInt(req.query.tipo_profesional_id); }

        if (typeof req.query.lunes_horario_id != 'undefined'&& req.query.lunes_horario_id != "" ) 
            { parameters.lunes_horario_id = parseInt(req.query.lunes_horario_id); }

        if (typeof req.query.martes_horario_id != 'undefined'&& req.query.martes_horario_id != "" ) 
            { parameters.martes_horario_id = parseInt(req.query.martes_horario_id); }

        if (typeof req.query.miercoles_horario_id != 'undefined'&& req.query.miercoles_horario_id != "" ) 
            { parameters.miercoles_horario_id = parseInt(req.query.miercoles_horario_id); }

        if (typeof req.query.jueves_horario_id != 'undefined'&& req.query.jueves_horario_id != "" ) 
            { parameters.jueves_horario_id = parseInt(req.query.jueves_horario_id); }

        if (typeof req.query.viernes_horario_id != 'undefined'&& req.query.viernes_horario_id != "" ) 
            { parameters.viernes_horario_id = parseInt(req.query.viernes_horario_id); }

        controllers.crudProfesional.actualizarDataProfesional(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudProfesional/insertarDataProfesional', function(req, res){
        session = req.session;
        
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
        };

        if (typeof req.query.tipo_profesional_id != 'undefined'&& req.query.tipo_profesional_id != "" ) 
            { parameters.tipo_profesional_id = parseInt(req.query.tipo_profesional_id); }

        if (typeof req.query.calle != 'undefined' && req.query.calle != "" ) 
            { parameters.calle = req.query.calle; }

        if (typeof req.query.brick_id != 'undefined'&& req.query.brick_id != "" ) 
            { parameters.brick_id = parseInt(req.query.brick_id); }

        if (typeof req.query.categoria_adopcion_id != 'undefined'&& req.query.categoria_adopcion_id != "" ) 
            { parameters.categoria_adopcion_id = parseInt(req.query.categoria_adopcion_id); }

        if (typeof req.query.categoria_compania_id != 'undefined'&& req.query.categoria_compania_id != "" ) 
            { parameters.categoria_compania_id = parseInt(req.query.categoria_compania_id); }

        if (typeof req.query.categoria_estrategica != 'undefined'&& req.query.categoria_estrategica != "" ) 
            { parameters.categoria_estrategica = req.query.categoria_estrategica; }

        if (typeof req.query.categoria_potencial_id != 'undefined'&& req.query.categoria_potencial_id != "" ) 
            { parameters.categoria_potencial_id = parseInt(req.query.categoria_potencial_id); }

        if (typeof req.query.codigo != 'undefined'&& req.query.codigo != "" ) 
            { parameters.codigo = req.query.codigo; }

        if (typeof req.query.comuna != 'undefined'&& req.query.comuna != "" ) 
            { parameters.comuna = req.query.comuna; }

        if (typeof req.query.correo != 'undefined'&& req.query.correo != "" ) 
            { parameters.correo = req.query.correo; }

        if (typeof req.query.especialidad_2 != 'undefined'&& req.query.especialidad_2 != "" ) 
            { parameters.especialidad_2 = req.query.especialidad_2; }

        if (typeof req.query.especialidad_id != 'undefined'&& req.query.especialidad_id != "" ) 
            { parameters.especialidad_id = parseInt(req.query.especialidad_id); }

        if (typeof req.query.genero_id != 'undefined'&& req.query.genero_id != "" ) 
            { parameters.genero_id = parseInt(req.query.genero_id); }      

        if (typeof req.query.nacionalidad_id != 'undefined'&& req.query.nacionalidad_id != "" ) 
            { parameters.nacionalidad_id = parseInt(req.query.nacionalidad_id); }

        if (typeof req.query.nombre != 'undefined'&& req.query.nombre != "" ) 
            { parameters.nombre = req.query.nombre; }

        if (typeof req.query.telefono != 'undefined'&& req.query.telefono != "" ) 
            { parameters.telefono = req.query.telefono; }

        if (typeof req.query.tipo_profesional_id != 'undefined'&& req.query.tipo_profesional_id != "" ) 
            { parameters.tipo_profesional_id = parseInt(req.query.tipo_profesional_id); }

        if (typeof req.query.lunes_horario_id != 'undefined'&& req.query.lunes_horario_id != "" ) 
            { parameters.lunes_horario_id = parseInt(req.query.lunes_horario_id); }

        if (typeof req.query.martes_horario_id != 'undefined'&& req.query.martes_horario_id != "" ) 
            { parameters.martes_horario_id = parseInt(req.query.martes_horario_id); }

        if (typeof req.query.miercoles_horario_id != 'undefined'&& req.query.miercoles_horario_id != "" ) 
            { parameters.miercoles_horario_id = parseInt(req.query.miercoles_horario_id); }

        if (typeof req.query.jueves_horario_id != 'undefined'&& req.query.jueves_horario_id != "" ) 
            { parameters.jueves_horario_id = parseInt(req.query.jueves_horario_id); }

        if (typeof req.query.viernes_horario_id != 'undefined'&& req.query.viernes_horario_id != "" ) 
            { parameters.viernes_horario_id = parseInt(req.query.viernes_horario_id); }

        controllers.crudProfesional.insertarDataProfesional(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER CRUD INSTITUCION
    app.get('/rest/crudInstitucion/obtenerDataInstitucion', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudInstitucion.obtenerDataInstitucion(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudInstitucion/obtenerDataInstitucionPorID', function(req, res){
        session = req.session;
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
            , institucion_id:   parseInt(req.query.institucion_id)
        };
        controllers.crudInstitucion.obtenerDataInstitucionPorID(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudInstitucion/actualizarDataInstitucion', function(req, res){
        session = req.session;
        
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
        };

        if (typeof req.query.tipo_institucion_id != 'undefined'&& req.query.tipo_institucion_id != "" ) 
            { parameters.tipo_institucion_id = parseInt(req.query.tipo_institucion_id); }

        if (typeof req.query.calle != 'undefined' && req.query.calle != "" ) 
            { parameters.calle = req.query.calle; }

        if (typeof req.query.brick_id != 'undefined'&& req.query.brick_id != "" ) 
            { parameters.brick_id = parseInt(req.query.brick_id); }

        if (typeof req.query.cadena_id != 'undefined'&& req.query.cadena_id != "" ) 
            { parameters.cadena_id = parseInt(req.query.cadena_id); }

        if (typeof req.query.categoria_adopcion_id != 'undefined'&& req.query.categoria_adopcion_id != "" ) 
            { parameters.categoria_adopcion_id = parseInt(req.query.categoria_adopcion_id); }

        if (typeof req.query.categoria_compania_id != 'undefined'&& req.query.categoria_compania_id != "" ) 
            { parameters.categoria_compania_id = parseInt(req.query.categoria_compania_id); }

        if (typeof req.query.categoria_estrategica != 'undefined'&& req.query.categoria_estrategica != "" ) 
            { parameters.categoria_estrategica = req.query.categoria_estrategica; }

        if (typeof req.query.categoria_potencial_id != 'undefined'&& req.query.categoria_potencial_id != "" ) 
            { parameters.categoria_potencial_id = parseInt(req.query.categoria_potencial_id); }

        if (typeof req.query.comuna != 'undefined'&& req.query.comuna != "" ) 
            { parameters.comuna = req.query.comuna; }

        if (typeof req.query.id != 'undefined'&& req.query.id != "" ) 
            { parameters.institucion_id = parseInt(req.query.id); }

        if (typeof req.query.nombre != 'undefined'&& req.query.nombre != "" ) 
            { parameters.nombre = req.query.nombre; }

        if (typeof req.query.telefono != 'undefined'&& req.query.telefono != "" ) 
            { parameters.telefono = req.query.telefono; }
        
        if (typeof req.query.local != 'undefined'&& req.query.local != "" ) 
            { parameters.local = req.query.local; }

        if (typeof req.query.lunes_horario_id != 'undefined'&& req.query.lunes_horario_id != "" ) 
            { parameters.lunes_horario_id = parseInt(req.query.lunes_horario_id); }

        if (typeof req.query.martes_horario_id != 'undefined'&& req.query.martes_horario_id != "" ) 
            { parameters.martes_horario_id = parseInt(req.query.martes_horario_id); }

        if (typeof req.query.miercoles_horario_id != 'undefined'&& req.query.miercoles_horario_id != "" ) 
            { parameters.miercoles_horario_id = parseInt(req.query.miercoles_horario_id); }

        if (typeof req.query.jueves_horario_id != 'undefined'&& req.query.jueves_horario_id != "" ) 
            { parameters.jueves_horario_id = parseInt(req.query.jueves_horario_id); }

        if (typeof req.query.viernes_horario_id != 'undefined'&& req.query.viernes_horario_id != "" ) 
            { parameters.viernes_horario_id = parseInt(req.query.viernes_horario_id); }

        controllers.crudInstitucion.actualizarDataInstitucion(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudInstitucion/insertarDataInstitucion', function(req, res){
        session = req.session;
        
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
        };

        if (typeof req.query.codigo != 'undefined'&& req.query.codigo != "" ) 
            { parameters.codigo = req.query.codigo; }

            if (typeof req.query.tipo_institucion_id != 'undefined'&& req.query.tipo_institucion_id != "" ) 
            { parameters.tipo_institucion_id = parseInt(req.query.tipo_institucion_id); }

        if (typeof req.query.calle != 'undefined' && req.query.calle != "" ) 
            { parameters.calle = req.query.calle; }

        if (typeof req.query.brick_id != 'undefined'&& req.query.brick_id != "" ) 
            { parameters.brick_id = parseInt(req.query.brick_id); }

        if (typeof req.query.cadena_id != 'undefined'&& req.query.cadena_id != "" ) 
            { parameters.cadena_id = parseInt(req.query.cadena_id); }

        if (typeof req.query.categoria_adopcion_id != 'undefined'&& req.query.categoria_adopcion_id != "" ) 
            { parameters.categoria_adopcion_id = parseInt(req.query.categoria_adopcion_id); }

        if (typeof req.query.categoria_compania_id != 'undefined'&& req.query.categoria_compania_id != "" ) 
            { parameters.categoria_compania_id = parseInt(req.query.categoria_compania_id); }

        if (typeof req.query.categoria_estrategica != 'undefined'&& req.query.categoria_estrategica != "" ) 
            { parameters.categoria_estrategica = req.query.categoria_estrategica; }

        if (typeof req.query.categoria_potencial_id != 'undefined'&& req.query.categoria_potencial_id != "" ) 
            { parameters.categoria_potencial_id = parseInt(req.query.categoria_potencial_id); }

        if (typeof req.query.comuna != 'undefined'&& req.query.comuna != "" ) 
            { parameters.comuna = req.query.comuna; }

        if (typeof req.query.id != 'undefined'&& req.query.id != "" ) 
            { parameters.institucion_id = parseInt(req.query.id); }

        if (typeof req.query.nombre != 'undefined'&& req.query.nombre != "" ) 
            { parameters.nombre = req.query.nombre; }

        if (typeof req.query.telefono != 'undefined'&& req.query.telefono != "" ) 
            { parameters.telefono = req.query.telefono; }
        
        if (typeof req.query.local != 'undefined'&& req.query.local != "" ) 
            { parameters.local = req.query.local; }

        if (typeof req.query.lunes_horario_id != 'undefined'&& req.query.lunes_horario_id != "" ) 
            { parameters.lunes_horario_id = parseInt(req.query.lunes_horario_id); }

        if (typeof req.query.martes_horario_id != 'undefined'&& req.query.martes_horario_id != "" ) 
            { parameters.martes_horario_id = parseInt(req.query.martes_horario_id); }

        if (typeof req.query.miercoles_horario_id != 'undefined'&& req.query.miercoles_horario_id != "" ) 
            { parameters.miercoles_horario_id = parseInt(req.query.miercoles_horario_id); }

        if (typeof req.query.jueves_horario_id != 'undefined'&& req.query.jueves_horario_id != "" ) 
            { parameters.jueves_horario_id = parseInt(req.query.jueves_horario_id); }

        if (typeof req.query.viernes_horario_id != 'undefined'&& req.query.viernes_horario_id != "" ) 
            { parameters.viernes_horario_id = parseInt(req.query.viernes_horario_id); }

        controllers.crudInstitucion.insertarDataInstitucion(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER CRUD HOSPITALARIO
    app.get('/rest/crudHospitalario/obtenerDataHospitalario', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudHospitalario.obtenerDataHospitalario(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudHospitalario/obtenerDataHospitalarioPorID', function(req, res){
        session = req.session;
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
            , hospitalario_id:   parseInt(req.query.hospitalario_id)
        };
        controllers.crudHospitalario.obtenerDataHospitalarioPorID(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudHospitalario/actualizarDataHospitalario', function(req, res){
        session = req.session;
        
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
        };

        if (typeof req.query.calle != 'undefined' && req.query.calle != "" ) 
            { parameters.calle = req.query.calle; }

        if (typeof req.query.brick_id != 'undefined'&& req.query.brick_id != "" ) 
            { parameters.brick_id = parseInt(req.query.brick_id); }

        if (typeof req.query.categoria_adopcion_id != 'undefined'&& req.query.categoria_adopcion_id != "" ) 
            { parameters.categoria_adopcion_id = parseInt(req.query.categoria_adopcion_id); }

        if (typeof req.query.categoria_compania_id != 'undefined'&& req.query.categoria_compania_id != "" ) 
            { parameters.categoria_compania_id = parseInt(req.query.categoria_compania_id); }

        if (typeof req.query.categoria_potencial_id != 'undefined'&& req.query.categoria_potencial_id != "" ) 
            { parameters.categoria_potencial_id = parseInt(req.query.categoria_potencial_id); }

        if (typeof req.query.comuna != 'undefined'&& req.query.comuna != "" ) 
            { parameters.comuna = req.query.comuna; }

        if (typeof req.query.contacto != 'undefined'&& req.query.contacto != "" ) 
            { parameters.contacto = req.query.contacto; }

        if (typeof req.query.correo != 'undefined'&& req.query.correo != "" ) 
            { parameters.correo = req.query.correo; }

        if (typeof req.query.correo2 != 'undefined'&& req.query.correo2 != "" ) 
            { parameters.correo2 = req.query.correo2; }

        if (typeof req.query.id != 'undefined'&& req.query.id != "" ) 
            { parameters.hospitalario_id = parseInt(req.query.id); }        

        if (typeof req.query.nombre != 'undefined'&& req.query.nombre != "" ) 
            { parameters.nombre = req.query.nombre; }

        if (typeof req.query.telefono != 'undefined'&& req.query.telefono != "" ) 
            { parameters.telefono = req.query.telefono; }

        if (typeof req.query.lunes_horario_id != 'undefined'&& req.query.lunes_horario_id != "" ) 
            { parameters.lunes_horario_id = parseInt(req.query.lunes_horario_id); }

        if (typeof req.query.martes_horario_id != 'undefined'&& req.query.martes_horario_id != "" ) 
            { parameters.martes_horario_id = parseInt(req.query.martes_horario_id); }

        if (typeof req.query.miercoles_horario_id != 'undefined'&& req.query.miercoles_horario_id != "" ) 
            { parameters.miercoles_horario_id = parseInt(req.query.miercoles_horario_id); }

        if (typeof req.query.jueves_horario_id != 'undefined'&& req.query.jueves_horario_id != "" ) 
            { parameters.jueves_horario_id = parseInt(req.query.jueves_horario_id); }

        if (typeof req.query.viernes_horario_id != 'undefined'&& req.query.viernes_horario_id != "" ) 
            { parameters.viernes_horario_id = parseInt(req.query.viernes_horario_id); }

        controllers.crudHospitalario.actualizarDataHospitalario(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudHospitalario/insertarDataHospitalario', function(req, res){
        session = req.session;
        
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
        };

        if (typeof req.query.calle != 'undefined' && req.query.calle != "" ) 
            { parameters.calle = req.query.calle; }

        if (typeof req.query.brick_id != 'undefined'&& req.query.brick_id != "" ) 
            { parameters.brick_id = parseInt(req.query.brick_id); }

        if (typeof req.query.categoria_adopcion_id != 'undefined'&& req.query.categoria_adopcion_id != "" ) 
            { parameters.categoria_adopcion_id = parseInt(req.query.categoria_adopcion_id); }

        if (typeof req.query.categoria_compania_id != 'undefined'&& req.query.categoria_compania_id != "" ) 
            { parameters.categoria_compania_id = parseInt(req.query.categoria_compania_id); }

        if (typeof req.query.categoria_potencial_id != 'undefined'&& req.query.categoria_potencial_id != "" ) 
            { parameters.categoria_potencial_id = parseInt(req.query.categoria_potencial_id); }

        if (typeof req.query.codigo != 'undefined'&& req.query.codigo != "" ) 
            { parameters.codigo = req.query.codigo; }

        if (typeof req.query.comuna != 'undefined'&& req.query.comuna != "" ) 
            { parameters.comuna = req.query.comuna; }

        if (typeof req.query.contacto != 'undefined'&& req.query.contacto != "" ) 
            { parameters.contacto = req.query.contacto; }

        if (typeof req.query.correo != 'undefined'&& req.query.correo != "" ) 
            { parameters.correo = req.query.correo; }

        if (typeof req.query.correo2 != 'undefined'&& req.query.correo2 != "" ) 
            { parameters.correo2 = req.query.correo2; }

        if (typeof req.query.id != 'undefined'&& req.query.id != "" ) 
            { parameters.hospitalario_id = parseInt(req.query.id); }        

        if (typeof req.query.nombre != 'undefined'&& req.query.nombre != "" ) 
            { parameters.nombre = req.query.nombre; }

        if (typeof req.query.telefono != 'undefined'&& req.query.telefono != "" ) 
            { parameters.telefono = req.query.telefono; }

        if (typeof req.query.lunes_horario_id != 'undefined'&& req.query.lunes_horario_id != "" ) 
            { parameters.lunes_horario_id = parseInt(req.query.lunes_horario_id); }

        if (typeof req.query.martes_horario_id != 'undefined'&& req.query.martes_horario_id != "" ) 
            { parameters.martes_horario_id = parseInt(req.query.martes_horario_id); }

        if (typeof req.query.miercoles_horario_id != 'undefined'&& req.query.miercoles_horario_id != "" ) 
            { parameters.miercoles_horario_id = parseInt(req.query.miercoles_horario_id); }

        if (typeof req.query.jueves_horario_id != 'undefined'&& req.query.jueves_horario_id != "" ) 
            { parameters.jueves_horario_id = parseInt(req.query.jueves_horario_id); }

        if (typeof req.query.viernes_horario_id != 'undefined'&& req.query.viernes_horario_id != "" ) 
            { parameters.viernes_horario_id = parseInt(req.query.viernes_horario_id); }

        controllers.crudHospitalario.insertarDataHospitalario(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

//CONTROLLER CRUD PROFESIONAL
    app.get('/rest/crudKardex/obtenerDataKardex', function(req, res){
        session = req.session;
        var parameters = {cuenta_acceso_id:session.cuenta_id}
        controllers.crudKardex.obtenerDataKardex(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

    app.get('/rest/crudKardex/obtenerDataKardexPorID', function(req, res){
        session = req.session;
        var parameters = {
            cuenta_acceso_id:   session.cuenta_id
            , representante_cliente_id:   parseInt(req.query.representante_cliente_id)
        };
        controllers.crudKardex.obtenerDataKardexPorID(configDB, parameters) 
        .then(data => { 
            res.json(data);
        })
        .catch(err => {
            res.status(500).json({ error: err.toString(), number: err.number });
        });
    });

////////////////////////////////////////////////////////////////////////
//////////////////////  CONFIGURACIONES FINALES ////////////////////////
////////////////////////////////////////////////////////////////////////

app.get('/logout',function(req,res) {
    req.session.destroy(function(err) {
        if(err) { console.log(err); } else { res.redirect('/'); }
    });
});

// PUERTO ASIGNADO
app.listen(1355, function () {
    console.log('Express activo en el puerto 1355');
});

// FUNCIONES ADICIONALES
function parseBoolean(string) {
    var bool;
    bool = (function() {
        switch (false) {
        case string.toLowerCase() !== 'true':
            return true;
        case string.toLowerCase() !== 'false':
            return false;
        }
    })();
    if (typeof bool === "boolean") {
        return bool;
    }
    return void 0;
};