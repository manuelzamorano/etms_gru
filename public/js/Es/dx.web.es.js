/*! 
* DevExtreme Web
* Version: 16.1.6
* Build date: Sep 2, 2016
*
* Copyright (c) 2012 - 2016 Developer Express Inc. ALL RIGHTS RESERVED
* EULA: https://www.devexpress.com/Support/EULAs/DevExtreme.xml
*/

(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(function(require, exports, module) {
            factory(require("../message"))
        })
    }
    else {
        factory(DevExpress.localization.message)
    }
}(this, function(message) {
    message.load({es: {
        "Yes": "Sí",
        "No": "No",
        "Cancel": "Cancelar",
        "Clear": "Limpiar",
        "Done": "Listo",
        "Loading": "Cargando...",
        "Select": "Seleccionar...",
        "Search": "Buscar",
        "Back": "Atrás",
        "OK": "Aceptar",
        "dxCollectionWidget-noDataText": "No hay datos para mostrar",
        "validation-required": "Requerido",
        "validation-required-formatted": "{0} es requerido",
        "validation-numeric": "El valor debe ser un número",
        "validation-numeric-formatted": "{0} debe ser un número",
        "validation-range": "El valor está fuera del rango",
        "validation-range-formatted": "{0} está fuera del rango",
        "validation-stringLength": "El largo del valor no es correcto",
        "validation-stringLength-formatted": "El largo de {0} no es correcto",
        "validation-custom": "El valor es inválido",
        "validation-custom-formatted": "{0} es inválido",
        "validation-compare": "Los valores no coinciden",
        "validation-compare-formatted": "{0} no coinciden",
        "validation-pattern": "El valor no coincide con el patrón",
        "validation-pattern-formatted": "{0} no coincide con el patrón",
        "validation-email": "El email es inválido",
        "validation-email-formatted": "{0} es inválido",
        "validation-mask": "El valor es inválido"
        }})
}));
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(function(require, exports, module) {
            factory(require("../message"))
        })
    }
    else {
        factory(DevExpress.localization.message)
    }
}(this, function(message) {
    message.load({es: {
        "dxLookup-searchPlaceholder": "Número de carácteres mínimo: {0}",
        "dxList-pullingDownText": "Tire hacia abajo para actualizar...",
        "dxList-pulledDownText": "Suelte para actualizar...",
        "dxList-refreshingText": "Actualizando...",
        "dxList-pageLoadingText": "Cargando...",
        "dxList-nextButtonText": "Más",
        "dxList-selectAll": "Seleccionar todo",
        "dxListEditDecorator-delete": "Borrar",
        "dxListEditDecorator-more": "Más",
        "dxScrollView-pullingDownText": "Tire hacia abajo para actualizar...",
        "dxScrollView-pulledDownText": "Suelte para actualizar...",
        "dxScrollView-refreshingText": "Actualizando...",
        "dxScrollView-reachBottomText": "Cargando...",
        "dxDateBox-simulatedDataPickerTitleTime": "Seleccione hora",
        "dxDateBox-simulatedDataPickerTitleDate": "Seleccione fecha",
        "dxDateBox-simulatedDataPickerTitleDateTime": "Seleccione fecha y hora",
        "dxDateBox-validation-datetime": "El valor debe ser una fecha o hora",
        "dxFileUploader-selectFile": "Seleccione Archivo",
        "dxFileUploader-dropFile": "o Elimine el archivo aquí",
        "dxFileUploader-bytes": "bytes",
        "dxFileUploader-kb": "kb",
        "dxFileUploader-Mb": "Mb",
        "dxFileUploader-Gb": "Gb",
        "dxFileUploader-upload": "Subir",
        "dxFileUploader-uploaded": "Subido",
        "dxFileUploader-readyToUpload": "Listo para subir",
        "dxFileUploader-uploadFailedMessage": "Subida fallida",
        "dxRangeSlider-ariaFrom": "De",
        "dxRangeSlider-ariaTill": "Hasta",
        "dxSwitch-onText": "ENCENDIDO",
        "dxSwitch-offText": "APAGADO",
        "dxForm-optionalMark": "opcional",
        "dxForm-requiredMessage": "{0} es requerido",
        "dxNumberBox-invalidValueMessage": "Value must be a number"
        }})
}));
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(function(require, exports, module) {
            factory(require("../message"))
        })
    }
    else {
        factory(DevExpress.localization.message)
    }
}(this, function(message) {
    message.load({es: {
        "dxDataGrid-columnChooserTitle": "Column Chooser",
        "dxDataGrid-columnChooserEmptyText": "Drag a column here to hide it",
        "dxDataGrid-groupContinuesMessage": "Continues on the next page",
        "dxDataGrid-groupContinuedMessage": "Continued from the previous page",
        "dxDataGrid-groupHeaderText": "",
        "dxDataGrid-ungroupHeaderText": "Ungroup",
        "dxDataGrid-ungroupAllText": "Ungroup All",
        "dxDataGrid-editingEditRow": "Edit",
        "dxDataGrid-editingSaveRowChanges": "Save",
        "dxDataGrid-editingCancelRowChanges": "Cancel",
        "dxDataGrid-editingDeleteRow": "Delete",
        "dxDataGrid-editingUndeleteRow": "Undelete",
        "dxDataGrid-editingConfirmDeleteMessage": "Are you sure you want to delete this record?",
        "dxDataGrid-validationCancelChanges": "Cancel changes",
        "dxDataGrid-groupPanelEmptyText": "Arrastre aquí una columna para agrupar por ella.",
        "dxDataGrid-noDataText": "Sin datos",
        "dxDataGrid-searchPanelPlaceholder": "Buscar...",
        "dxDataGrid-filterRowShowAllText": "(Todos)",
        "dxDataGrid-filterRowResetOperationText": "Reset",
        "dxDataGrid-filterRowOperationEquals": "Igual",
        "dxDataGrid-filterRowOperationNotEquals": "Distinto",
        "dxDataGrid-filterRowOperationLess": "Menor que",
        "dxDataGrid-filterRowOperationLessOrEquals": "Menor o igual que",
        "dxDataGrid-filterRowOperationGreater": "Mayor que",
        "dxDataGrid-filterRowOperationGreaterOrEquals": "Mayor o Igual que",
        "dxDataGrid-filterRowOperationStartsWith": "Comienza con",
        "dxDataGrid-filterRowOperationContains": "Contiene",
        "dxDataGrid-filterRowOperationNotContains": "No contiene",
        "dxDataGrid-filterRowOperationEndsWith": "Termina con",
        "dxDataGrid-filterRowOperationBetween": "Entre",
        "dxDataGrid-filterRowOperationBetweenStartText": "Comienzo",
        "dxDataGrid-filterRowOperationBetweenEndText": "Fin",
        "dxDataGrid-applyFilterText": "Aplicar filtro",
        "dxDataGrid-trueText": "Verdadero",
        "dxDataGrid-falseText": "Falso",
        "dxDataGrid-sortingAscendingText": "Orden Ascendente",
        "dxDataGrid-sortingDescendingText": "Orden Descendente",
        "dxDataGrid-sortingClearText": "Clear Sorting",
        "dxDataGrid-editingSaveAllChanges": "Guardar Cambios",
        "dxDataGrid-editingCancelAllChanges": "Descartar Cambios",
        "dxDataGrid-editingAddRow": "Agregar fila",
        "dxDataGrid-summaryMin": "Min: {0}",
        "dxDataGrid-summaryMinOtherColumn": "Min of {1} is {0}",
        "dxDataGrid-summaryMax": "Max: {0}",
        "dxDataGrid-summaryMaxOtherColumn": "Max of {1} is {0}",
        "dxDataGrid-summaryAvg": "Prom: {0}",
        "dxDataGrid-summaryAvgOtherColumn": "Avg of {1} is {0}",
        "dxDataGrid-summarySum": "Suma: {0}",
        "dxDataGrid-summarySumOtherColumn": "Sum of {1} is {0}",
        "dxDataGrid-summaryCount": "Count: {0}",
        "dxDataGrid-columnFixingFix": "Fix",
        "dxDataGrid-columnFixingUnfix": "Unfix",
        "dxDataGrid-columnFixingLeftPosition": "To the left",
        "dxDataGrid-columnFixingRightPosition": "To the right",
        "dxDataGrid-exportTo": "Exportar",
        "dxDataGrid-exportToExcel": "Exportar a archivo Excel",
        "dxDataGrid-excelFormat": "Archivo Excel",
        "dxDataGrid-selectedRows": "Filas seleccionadas",
        "dxDataGrid-exportSelectedRows": "Exportar filas seleccionadas",
        "dxDataGrid-exportAll": "Exportar toda la data.",
        "dxDataGrid-headerFilterEmptyValue": "(Blanks)",
        "dxDataGrid-headerFilterOK": "Aceptar",
        "dxDataGrid-headerFilterCancel": "Cancelar",
        "dxDataGrid-ariaColumn": "Columna",
        "dxDataGrid-ariaValue": "Valor",
        "dxDataGrid-ariaFilterCell": "Filter cell",
        "dxDataGrid-ariaCollapse": "Collapse",
        "dxDataGrid-ariaExpand": "Expand",
        "dxDataGrid-ariaDataGrid": "Data grid",
        "dxDataGrid-ariaSearchInGrid": "Buscar en la grilla",
        "dxDataGrid-ariaSelectAll": "Seleccionar todo",
        "dxDataGrid-ariaSelectRow": "Seleccionar fila",
        "dxPager-infoText": "Página {0} de {1} ({2} registros)",
        "dxPager-pagesCountText": "de",
        "dxPivotGrid-grandTotal": "Grand Total",
        "dxPivotGrid-total": "{0} Total",
        "dxPivotGrid-fieldChooserTitle": "Field Chooser",
        "dxPivotGrid-showFieldChooser": "Show Field Chooser",
        "dxPivotGrid-expandAll": "Expandir todo",
        "dxPivotGrid-collapseAll": "Colapsar todo",
        "dxPivotGrid-sortColumnBySummary": "Sort \"{0}\" by This Column",
        "dxPivotGrid-sortRowBySummary": "Sort \"{0}\" by This Row",
        "dxPivotGrid-removeAllSorting": "Remove All Sorting",
        "dxPivotGrid-rowFields": "Row Fields",
        "dxPivotGrid-columnFields": "Column Fields",
        "dxPivotGrid-dataFields": "Data Fields",
        "dxPivotGrid-filterFields": "Filter Fields",
        "dxPivotGrid-allFields": "All Fields",
        "dxPivotGrid-columnFieldArea": "Drop Column Fields Here",
        "dxPivotGrid-dataFieldArea": "Drop Data Fields Here",
        "dxPivotGrid-rowFieldArea": "Drop Row Fields Here",
        "dxPivotGrid-filterFieldArea": "Drop Filter Fields Here",
        "dxScheduler-editorLabelTitle": "Asunto",
        "dxScheduler-editorLabelStartDate": "Fecha de inicio",
        "dxScheduler-editorLabelEndDate": "Fecha final",
        "dxScheduler-editorLabelDescription": "Descripción",
        "dxScheduler-editorLabelRecurrence": "Repetir",
        "dxScheduler-openAppointment": "Abrir cita",
        "dxScheduler-recurrenceNever": "Nunca",
        "dxScheduler-recurrenceDaily": "Diariamente",
        "dxScheduler-recurrenceWeekly": "Semanalmente",
        "dxScheduler-recurrenceMonthly": "Mensualmente",
        "dxScheduler-recurrenceYearly": "Anualmente",
        "dxScheduler-recurrenceEvery": "Siempre",
        "dxScheduler-recurrenceEnd": "Fin de la repetición",
        "dxScheduler-recurrenceAfter": "Después",
        "dxScheduler-recurrenceOn": "On",
        "dxScheduler-recurrenceRepeatDaily": "day(s)",
        "dxScheduler-recurrenceRepeatWeekly": "week(s)",
        "dxScheduler-recurrenceRepeatMonthly": "month(s)",
        "dxScheduler-recurrenceRepeatYearly": "year(s)",
        "dxScheduler-switcherDay": "Dia",
        "dxScheduler-switcherWeek": "Semana",
        "dxScheduler-switcherWorkWeek": "Semana Laboral",
        "dxScheduler-switcherMonth": "Mes",
        "dxScheduler-switcherAgenda": "Agenda",
        "dxScheduler-switcherTimelineDay": "Timeline Day",
        "dxScheduler-switcherTimelineWeek": "Timeline Week",
        "dxScheduler-switcherTimelineWorkWeek": "Timeline Work Week",
        "dxScheduler-switcherTimelineMonth": "Timeline Month",
        "dxScheduler-recurrenceRepeatOnDate": "on date",
        "dxScheduler-recurrenceRepeatCount": "occurrence(s)",
        "dxScheduler-allDay": "Todo el día",
        "dxScheduler-confirmRecurrenceEditMessage": "Do you want to edit only this appointment or the whole series?",
        "dxScheduler-confirmRecurrenceDeleteMessage": "Do you want to delete only this appointment or the whole series?",
        "dxScheduler-confirmRecurrenceEditSeries": "Edit series",
        "dxScheduler-confirmRecurrenceDeleteSeries": "Delete series",
        "dxScheduler-confirmRecurrenceEditOccurrence": "Edit appointment",
        "dxScheduler-confirmRecurrenceDeleteOccurrence": "Delete appointment",
        "dxScheduler-noTimezoneTitle": "No timezone",
        "dxCalendar-todayButtonText": "Hoy",
        "dxCalendar-ariaWidgetName": "Calendar",
        "dxColorView-ariaRed": "Red",
        "dxColorView-ariaGreen": "Green",
        "dxColorView-ariaBlue": "Blue",
        "dxColorView-ariaAlpha": "Transparency",
        "dxColorView-ariaHex": "Color code",
        "vizExport-printingButtonText": "Imprimir",
        "vizExport-titleMenuText": "Exportación/Impresión",
        "vizExport-exportButtonText": "{0} archivo"
        }})
}));
