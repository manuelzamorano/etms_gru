var sql = require('mssql');
var promise = require('promise');

exports.getCuentaAccesoForLogin = function getCuentaAccesoForLogin(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_usuario', sql.VarChar(50), parameters.usuario)
            .input('cuenta_contraseña', sql.VarChar(15), parameters.contraseña)
            .execute('[goph].PROC_Cuenta_acceso_SELECT_FOR_Login').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};