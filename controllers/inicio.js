var sql = require('mssql');
var promise = require('promise');
var sql2 = require('mssql');
var promise2 = require('promise');

exports.getDataInicio = function getDataInicio(configDB, parameters){
    return new promise2((resolve, reject) => {
        sql2.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('tipo_cliente', sql.VarChar(20), parameters.tipo_cliente)
            .execute('cliente.PROC_Inicio_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

//ACTUAL
exports.getDataInicioRepresentante = function getDataInicioRepresentante(configDB, parameters){
    return new promise2((resolve, reject) => {
        sql2.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Inicio_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};