var sql = require('mssql');
var promise = require('promise');

exports.getDataVisita = function getDataVisita(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('cliente_id', sql.Int, parameters.cliente_id)
            .execute('cliente.PROC_Visita_SELECT_For_Formulario_profesional_BY_id').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataEncuesta = function getDataEncuesta(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('cliente_id', sql.Int, parameters.cliente_id)
            .execute('encuesta.PROC_Pregunta_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertAlternativaEncuesta = function insertAlternativaEncuesta(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('pregunta_id', sql.Int, parameters.pregunta_id)
            .execute('encuesta.PROC_alternativa_SELECT_BY_Pregunta_id').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertVisita = function insertVisita(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cliente_id'         , sql.Int,              parameters.cliente_id)
            .input('cuenta_acceso_id'   , sql.Int,              parameters.cuenta_acceso_id)
            .input('ciclo_id'           , sql.Int,              parameters.ciclo_id)
            .input('tipo_visita_id'     , sql.Int,              parameters.tipo_visita_id)
            .input('fecha'              , sql.Date,             parameters.fecha)
            .input('informacion'        , sql.VarChar(1000),    parameters.informacion)
            .input('es_acompañada'      , sql.Bit,              parameters.es_acompañada)
            .input('latitud'            , sql.VarChar(200),     parameters.latitud)
            .input('longitud'           , sql.VarChar(200),     parameters.longitud)
            .execute('cliente.PROC_Visita_INSERT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertVisitaProducto = function insertVisitaProducto(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('visita_id', sql.Int, parameters.visita_id)
            .input('producto_id', sql.Int, parameters.producto_id)
            .input('real', sql.Int, parameters.real)
            .input('real_promocional', sql.Bit, parameters.real_promocional)
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Visita_producto_INSERT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertVisitaEncuesta = function insertVisitaEncuesta(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('visita_id', sql.Int, parameters.visita_id)
            .input('alternativa_id', sql.Int, parameters.alternativa_id)
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('encuesta.PROC_Visita_encuesta_INSERT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};