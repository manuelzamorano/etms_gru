
var sql = require('mssql');
var promise = require('promise');

exports.getDataEdicion = function getDataEdicion(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .execute('edicion.PROC_data_Editar_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataCliente = function getDataCliente(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('tipo_cliente', sql.VarChar(20), parameters.tipo_cliente)
            .execute('edicion.PROC_datos_cliente_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataFormEdicion = function getDataFormEdicion(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('cliente_id', sql.Int, parameters.cliente_id)
            .execute('edicion.PROC_Cliente_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};