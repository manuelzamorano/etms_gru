var sql = require('mssql');
var promise = require('promise');

exports.getDataCurrentPosition = function getDataCurrentPosition(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Cliente_Mapa_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataClienteAgenda = function getDataClienteAgenda(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('fecha', sql.Date, parameters.fecha)
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Cliente_Agenda_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};