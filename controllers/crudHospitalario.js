var sql = require('mssql');
var promise = require('promise');

exports.obtenerDataHospitalario = function obtenerDataHospitalario(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Hospitalario_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerDataHospitalarioPorID = function obtenerDataHospitalarioPorID(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('id', sql.Int, parameters.hospitalario_id)
            .execute('crud.PROC_Hospitalario_SELECT_BY_ID').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.actualizarDataHospitalario = function actualizarDataHospitalario(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('hospitalario_id', sql.Int, parameters.hospitalario_id)
            .input('brick_id', sql.Int, parameters.brick_id)
            .input('calle', sql.VarChar(400), parameters.calle)
            .input('categoria_adopcion_id', sql.Int, parameters.categoria_adopcion_id)
            .input('categoria_compania_id', sql.Int, parameters.categoria_compania_id)
            .input('categoria_potencial_id', sql.Int, parameters.categoria_potencial_id)
            .input('comuna', sql.VarChar(100), parameters.comuna)
            .input('contacto', sql.VarChar(100), parameters.contacto)
            .input('correo', sql.VarChar(100), parameters.correo)
            .input('correo2', sql.VarChar(100), parameters.correo2)
            .input('nombre', sql.VarChar(300), parameters.nombre)
            .input('telefono', sql.VarChar(25), parameters.telefono)
            .input('lunes_horario_id', sql.Int, parameters.lunes_horario_id)
            .input('martes_horario_id', sql.Int, parameters.martes_horario_id)
            .input('miercoles_horario_id', sql.Int, parameters.miercoles_horario_id)
            .input('jueves_horario_id', sql.Int, parameters.jueves_horario_id)
            .input('viernes_horario_id', sql.Int, parameters.viernes_horario_id)
            .execute('crud.PROC_Hospitalario_UPDATE').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertarDataHospitalario = function insertarDataHospitalario(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('brick_id', sql.Int, parameters.brick_id)
            .input('calle', sql.VarChar(400), parameters.calle)
            .input('categoria_adopcion_id', sql.Int, parameters.categoria_adopcion_id)
            .input('categoria_compania_id', sql.Int, parameters.categoria_compania_id)
            .input('categoria_potencial_id', sql.Int, parameters.categoria_potencial_id)
            .input('codigo', sql.VarChar(15), parameters.codigo)
            .input('comuna', sql.VarChar(100), parameters.comuna)
            .input('contacto', sql.VarChar(100), parameters.contacto)
            .input('correo', sql.VarChar(100), parameters.correo)
            .input('correo2', sql.VarChar(100), parameters.correo2)
            .input('nombre', sql.VarChar(300), parameters.nombre)
            .input('telefono', sql.VarChar(25), parameters.telefono)
            .input('lunes_horario_id', sql.Int, parameters.lunes_horario_id)
            .input('martes_horario_id', sql.Int, parameters.martes_horario_id)
            .input('miercoles_horario_id', sql.Int, parameters.miercoles_horario_id)
            .input('jueves_horario_id', sql.Int, parameters.jueves_horario_id)
            .input('viernes_horario_id', sql.Int, parameters.viernes_horario_id)
            .execute('crud.PROC_Hospitalario_INSERT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};