
var sql = require('mssql');
var promise = require('promise');

exports.getDataImagen = function getDataImagen(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('incidencia_id'   , sql.Int, parameters.incidencia_id)
            .execute('cliente.PROC_Incidencia_imagen_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};
