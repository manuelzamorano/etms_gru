var sql = require('mssql');
var promise = require('promise');

exports.getMenu = function getMenu(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_id)
            .execute('goph.PROC_Estudio_SELECT_BY_Cuenta_acceso_id').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataProducto = function getDataProducto(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .execute('cliente.PROC_Producto_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataRepresentante = function getDataRepresentante(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Representante_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataCiclo = function getDataCiclo(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .execute('cliente.PROC_Ciclo_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataCicloActual = function getDataCicloActual(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .execute('cliente.PROC_Ciclo_actual_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataFechaCiclo = function getDataFechaCiclo(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .execute('goph.PROC_Configuracion_fecha_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};