var sql = require('mssql');
var promise = require('promise');

exports.getDataCoberturaSupervisor = function getDataCoberturaSupervisor(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('cuenta_acceso_id'   , sql.Int, parameters.cuenta_acceso_id)
            .input('ciclo_id'          , sql.Int, parameters.ciclo_id)
            .execute('cliente.PROC_Cobertura_supervisor_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};