
var sql = require('mssql');
var promise = require('promise');

exports.getDataInicioSupervisor = function getDataInicioSupervisor(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id'   , sql.Int, parameters.cuenta_acceso_id)
            .input('representante_id'   , sql.Int, parameters.representante_id)
            .input('ciclo_id'           , sql.Int, parameters.ciclo_id)
            // .input('tipo_cliente'        , sql.VarChar(20), parameters.tipo_cliente)
            .execute('cliente.PROC_Inicio_Supervisor_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};