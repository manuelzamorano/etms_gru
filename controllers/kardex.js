var sql = require('mssql');
var promise = require('promise');

exports.getDataKardex = function getDataKardex(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('tipo_cliente', sql.VarChar(20), parameters.tipo_cliente)
            .execute('cliente.PROC_Kardex_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};
