var sql = require('mssql');
var promise = require('promise');

exports.getDataScheduler = function getDataScheduler(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Agenda_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataCliente = function getDataCliente(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Cliente_SELECT_by_Representante').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataEstadoAgenda = function getDataEstadoAgenda(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Estado_agenda_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.updateDataScheduler = function updateDataScheduler(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('id', sql.Int, parameters.id)
            .input('fecha_inicio', sql.DateTime, parameters.fecha_inicio)
            .input('fecha_fin', sql.DateTime, parameters.fecha_fin)
            .execute('cliente.PROC_Agenda_UPDATE').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertDataScheduler = function insertDataScheduler(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cliente_id', sql.Int, parameters.cliente_id)
            .input('fecha', sql.DateTime, parameters.fecha)
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .execute('cliente.PROC_Agenda_INSERT_FOR_Agenda').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.updateStateScheduler = function updateStateScheduler(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('id', sql.Int, parameters.id)
            .input('estado_id', sql.Int, parameters.estado_id)
            .execute('cliente.PROC_Agenda_UPDATE_Estado_id').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.deleteDataScheduler = function deleteDataScheduler(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('id', sql.Int, parameters.id)
            .execute('cliente.PROC_Agenda_DELETE').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getCantidadAgenda = function getCantidadAgenda(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('fecha', sql.DateTime, parameters.fecha)
            .execute('cliente.[PROC_Agenda_contador_evento_SELECT]').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertarClonacionAgenda = function insertarClonacionAgenda(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('fechaOrigen', sql.DateTime, parameters.fechaOrigen)
            .input('fechaDestino', sql.DateTime, parameters.fechaDestino)
            .execute('cliente.[PROC_Agenda_clonar_evento_SELECT]').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};