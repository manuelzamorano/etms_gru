var sql = require('mssql');
var promise = require('promise');

exports.obtenerDataProfesional = function obtenerDataProfesional(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Profesional_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerDataProfesionalPorID = function obtenerDataProfesionalPorID(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('id', sql.Int, parameters.profesional_id)
            .execute('crud.PROC_Profesional_SELECT_BY_ID').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.actualizarDataProfesional = function actualizarDataProfesional(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('profesional_id', sql.Int, parameters.profesional_id)
            .input('tipo_profesional_id', sql.Int, parameters.tipo_profesional_id)
            .input('brick_id', sql.Int, parameters.brick_id)
            .input('calle', sql.VarChar(400), parameters.calle)
            .input('categoria_adopcion_id', sql.Int, parameters.categoria_adopcion_id)
            .input('categoria_compania_id', sql.Int, parameters.categoria_compania_id)
            .input('categoria_estrategica', sql.VarChar(50), parameters.categoria_estrategica)
            .input('categoria_potencial_id', sql.Int, parameters.categoria_potencial_id)
            .input('comuna', sql.VarChar(100), parameters.comuna)
            .input('correo', sql.VarChar(100), parameters.correo)
            .input('especialidad_2', sql.VarChar(100), parameters.especialidad_2)
            .input('especialidad_id', sql.Int, parameters.especialidad_id)
            .input('genero_id', sql.Int, parameters.genero_id)
            .input('nacionalidad_id', sql.Int, parameters.nacionalidad_id)
            .input('nombre', sql.VarChar(300), parameters.nombre)
            .input('telefono', sql.VarChar(25), parameters.telefono)
            .input('lunes_horario_id', sql.Int, parameters.lunes_horario_id)
            .input('martes_horario_id', sql.Int, parameters.martes_horario_id)
            .input('miercoles_horario_id', sql.Int, parameters.miercoles_horario_id)
            .input('jueves_horario_id', sql.Int, parameters.jueves_horario_id)
            .input('viernes_horario_id', sql.Int, parameters.viernes_horario_id)
            .execute('crud.PROC_Profesional_UPDATE').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertarDataProfesional = function insertarDataProfesional(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('tipo_profesional_id', sql.Int, parameters.tipo_profesional_id)
            .input('brick_id', sql.Int, parameters.brick_id)
            .input('calle', sql.VarChar(400), parameters.calle)
            .input('categoria_adopcion_id', sql.Int, parameters.categoria_adopcion_id)
            .input('categoria_compania_id', sql.Int, parameters.categoria_compania_id)
            .input('categoria_estrategica', sql.VarChar(50), parameters.categoria_estrategica)
            .input('categoria_potencial_id', sql.Int, parameters.categoria_potencial_id)
            .input('comuna', sql.VarChar(100), parameters.comuna)
            .input('correo', sql.VarChar(100), parameters.correo)
            .input('codigo', sql.VarChar(50), parameters.codigo)
            .input('especialidad_2', sql.VarChar(100), parameters.especialidad_2)
            .input('especialidad_id', sql.Int, parameters.especialidad_id)
            .input('genero_id', sql.Int, parameters.genero_id)
            .input('nacionalidad_id', sql.Int, parameters.nacionalidad_id)
            .input('nombre', sql.VarChar(300), parameters.nombre)
            .input('telefono', sql.VarChar(25), parameters.telefono)
            .input('lunes_horario_id', sql.Int, parameters.lunes_horario_id)
            .input('martes_horario_id', sql.Int, parameters.martes_horario_id)
            .input('miercoles_horario_id', sql.Int, parameters.miercoles_horario_id)
            .input('jueves_horario_id', sql.Int, parameters.jueves_horario_id)
            .input('viernes_horario_id', sql.Int, parameters.viernes_horario_id)
            .execute('crud.PROC_Profesional_INSERT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};