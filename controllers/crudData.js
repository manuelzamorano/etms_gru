var sql = require('mssql');
var promise = require('promise');

exports.obtenerCrudDataBrickSelectBox = function obtenerCrudDataBrickSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Brick_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataCadenaSelectBox = function obtenerCrudDataCadenaSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Cadena_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataEspecialidadSelectBox = function obtenerCrudDataEspecialidadSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Especialidad_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataGeneroSelectBox = function obtenerCrudDataGeneroSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Genero_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataHorarioSelectBox = function obtenerCrudDataHorarioSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Horario_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataNacionalidadSelectBox = function obtenerCrudDataNacionalidadSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Nacionalidad_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataCategoriaAdopcionSelectBox = function obtenerCrudDataCategoriaAdopcionSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Categoria_adopcion_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataCategoriaCompaniaSelectBox = function obtenerCrudDataCategoriaCompaniaSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Categoria_compañia_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataCategoriaPotencialSelectBox = function obtenerCrudDataCategoriaPotencialSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Categoria_potencial_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataTipoInstitucionSelectBox = function obtenerCrudDataTipoInstitucionSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Tipo_institucion_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerCrudDataTipoProfesionalSelectBox = function obtenerCrudDataTipoProfesionalSelectBox(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Tipo_profesional_SELECT_selectBox').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};