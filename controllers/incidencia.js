
var sql = require('mssql');
var promise = require('promise');

exports.getDataCadena = function getDataCadena(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .execute('cliente.PROC_Cadena_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataLocalDireccion = function getDataLocalDireccion(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id'   , sql.Int, parameters.cuenta_acceso_id)
            .input('cadena_id'          , sql.Int, parameters.cadena_id)
            .execute('cliente.PROC_Local_direccion_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.getDataIncidenciaProducto = function getDataIncidenciaProducto(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .execute('cliente.PROC_Incidencia_producto_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

var fs = require('fs');

exports.insertarDataIncidencia = function insertarDataIncidencia(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            new sql.Request()
            .input('cuenta_acceso_id', sql.Int, parameters.cuenta_acceso_id)
            .input('institucion_id', sql.Int, parameters.institucion_id)
            .input('producto_id', sql.Int, parameters.producto_id)
            .input('comentario', sql.VarChar(1000), parameters.comentario)
            .input('quiebre_stock', sql.Bit, parameters.quiebre_stock)
            .input('stock_bajo_sugerido', sql.Bit, parameters.stock_bajo_sugerido)
            .input('no_funciona_programa_paciente', sql.Bit, parameters.no_funciona_programa_paciente)
            .input('exhibicion', sql.Bit, parameters.exhibicion)
            .input('planograma', sql.Bit, parameters.planograma)
            .input('accion_competencia', sql.Bit, parameters.accion_competencia)
            .input('capacitacion_realizada', sql.Bit, parameters.capacitacion_realizada)
            .input('informacion_especial', sql.Bit, parameters.informacion_especial)
            .input('imagen', sql.VarBinary(sql.MAX), parameters.imagen)
            .execute('cliente.PROC_Incidencia_INSERT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};