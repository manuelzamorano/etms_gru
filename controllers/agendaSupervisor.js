var sql = require('mssql');
var promise = require('promise');

exports.getDataScheduler = function getDataScheduler(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('representante_id', sql.Int, parameters.representante_id)
            .execute('cliente.PROC_Agenda_SELECT_BY_Representante_id').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};