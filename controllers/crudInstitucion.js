var sql = require('mssql');
var promise = require('promise');

exports.obtenerDataInstitucion = function obtenerDataInstitucion(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .execute('crud.PROC_Institucion_SELECT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.obtenerDataInstitucionPorID = function obtenerDataInstitucionPorID(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('id', sql.Int, parameters.institucion_id)
            .execute('crud.PROC_Institucion_SELECT_BY_ID').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.actualizarDataInstitucion = function actualizarDataInstitucion(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('institucion_id', sql.Int, parameters.institucion_id)
            .input('tipo_Institucion_id', sql.Int, parameters.tipo_institucion_id)
            .input('brick_id', sql.Int, parameters.brick_id)
            .input('calle', sql.VarChar(400), parameters.calle)
            .input('cadena_id', sql.Int, parameters.cadena_id)
            .input('categoria_adopcion_id', sql.Int, parameters.categoria_adopcion_id)
            .input('categoria_compania_id', sql.Int, parameters.categoria_compania_id)
            .input('categoria_estrategica', sql.VarChar(50), parameters.categoria_estrategica)
            .input('categoria_potencial_id', sql.Int, parameters.categoria_potencial_id)
            .input('comuna', sql.VarChar(100), parameters.comuna)
            .input('nombre', sql.VarChar(300), parameters.nombre)
            .input('telefono', sql.VarChar(25), parameters.telefono)
            .input('local', sql.VarChar(10), parameters.local)
            .input('lunes_horario_id', sql.Int, parameters.lunes_horario_id)
            .input('martes_horario_id', sql.Int, parameters.martes_horario_id)
            .input('miercoles_horario_id', sql.Int, parameters.miercoles_horario_id)
            .input('jueves_horario_id', sql.Int, parameters.jueves_horario_id)
            .input('viernes_horario_id', sql.Int, parameters.viernes_horario_id)
            .execute('crud.PROC_Institucion_UPDATE').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};

exports.insertarDataInstitucion = function insertarDataInstitucion(configDB, parameters){
    return new promise((resolve, reject) => {
        sql.connect(configDB).then(function() {
            // Stored Procedure 
            new sql.Request()
            .input('codigo', sql.VarChar(15), parameters.codigo)
            .input('tipo_Institucion_id', sql.Int, parameters.tipo_institucion_id)
            .input('brick_id', sql.Int, parameters.brick_id)
            .input('calle', sql.VarChar(400), parameters.calle)
            .input('cadena_id', sql.Int, parameters.cadena_id)
            .input('categoria_adopcion_id', sql.Int, parameters.categoria_adopcion_id)
            .input('categoria_compania_id', sql.Int, parameters.categoria_compania_id)
            .input('categoria_estrategica', sql.VarChar(50), parameters.categoria_estrategica)
            .input('categoria_potencial_id', sql.Int, parameters.categoria_potencial_id)
            .input('comuna', sql.VarChar(100), parameters.comuna)
            .input('nombre', sql.VarChar(300), parameters.nombre)
            .input('telefono', sql.VarChar(25), parameters.telefono)
            .input('local', sql.VarChar(10), parameters.local)
            .input('lunes_horario_id', sql.Int, parameters.lunes_horario_id)
            .input('martes_horario_id', sql.Int, parameters.martes_horario_id)
            .input('miercoles_horario_id', sql.Int, parameters.miercoles_horario_id)
            .input('jueves_horario_id', sql.Int, parameters.jueves_horario_id)
            .input('viernes_horario_id', sql.Int, parameters.viernes_horario_id)
            .execute('crud.PROC_Institucion_INSERT').then(function(recordsets) {
                sql.close();
                return resolve(recordsets);
            }).catch(function(err) {
                sql.close();
                return reject(err);
            });
        }).catch(function(err) {
            sql.close();
            return reject(err);
        });
    })
};